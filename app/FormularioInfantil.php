<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FormularioInfantil extends Model
{
    protected $table = 'cp_caso';
    protected $guarded = [];
    public $timestamps = false;
}
