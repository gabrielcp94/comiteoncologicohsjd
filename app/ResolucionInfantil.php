<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ResolucionInfantil extends Model
{
    protected $table = 'cp_resolucion';
    protected $guarded = [];

    public function estado()
    {
		return $this->belongsTo('App\parametros', 'tx_estado');
    }

    public function integrantes()
    {
		return $this->belongsTo('App\IntegrantesInfantil', 'id_integrantes');
    }
}
