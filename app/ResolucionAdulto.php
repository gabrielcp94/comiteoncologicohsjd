<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ResolucionAdulto extends Model
{
    protected $table = 'comite';
    protected $guarded = [];
}