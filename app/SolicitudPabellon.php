<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SolicitudPabellon extends Model
{
    protected $connection = 'mysql3';
    protected $table = 'cb_solicitud_pabellon';

    public function protocolo()
    {
		return $this->hasOne('App\Protocolo', 'id_solicitud_pabellon');
    }
}
