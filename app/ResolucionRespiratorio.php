<?php

namespace App;

use Auth;
use Illuminate\Database\Eloquent\Model;

class ResolucionRespiratorio extends Model
{
    protected $table = 'com_resolucion_respiratorio';
    protected $guarded = [];

    public static function boot()
    {
        parent::boot();

        static::creating(function($resolucion)
        {
            $resolucion->created_by = Auth::id();
        });
          
        static::created(function($resolucion)
        {
            $resolucion->created_by = Auth::id();
        });
    }
}
