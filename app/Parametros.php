<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Parametros extends Model
{
    protected $table = 'parametros';

    protected $appends = ['tx_nombre_minuscula'];

    public function getTxNombreMinusculaAttribute()
    {
        return ucwords(mb_strtolower("{$this->tx_descripcion}"));
    }
}
