<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cie10 extends Model
{
    protected $table = 'cie10';

    protected $appends = ['tx_nombre_minuscula'];

    public function getTxNombreMinusculaAttribute()
    {
        return ucwords(mb_strtolower("{$this->dec10}"));
    }
}
