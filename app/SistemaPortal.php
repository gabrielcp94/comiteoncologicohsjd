<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SistemaPortal extends Model
{
    protected $connection = 'mysql2';
    protected $table = 'gen_sistemas';
}
