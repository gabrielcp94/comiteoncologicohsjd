<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class IntegrantesAdulto extends Model
{
    protected $table = 'integrantes_fecha';
    protected $guarded = [];
    public $timestamps = false;
}