<?php

namespace App;

use Auth;
use Illuminate\Database\Eloquent\Model;

class Comite extends Model
{
    protected $table = 'com_comite';
    protected $guarded = [];

    public static function boot()
    {
        parent::boot();

        static::creating(function($comite)
        {
            $comite->created_by = Auth::id();
            $comite->id_especialidad = Auth::user()->tx_especialidad;
        });
          
        static::created(function($comite)
        {
            $comite->created_by = Auth::id();
            $comite->id_especialidad = Auth::user()->tx_especialidad;
        });
    }

    public function paciente()
    {
		    return $this->belongsTo('App\Paciente', 'id_paciente');
    }

    public function tipoComite()
    {
		    return $this->belongsTo('App\TipoComite', 'id_tipo_comite');
    }

    public function medico()
    {
	  	  return $this->belongsTo('App\User', 'created_by');
    }

    public function establecimiento()
    {
		    return $this->belongsTo('App\Establecimiento', 'id_hospital_origen');
    }

    public function especialidad()
    {
		    return $this->belongsTo('App\Especialidad', 'id_especialidad');
    }

    public function comorbilidades()
    {
        return $this->belongsToMany('App\Comorbilidad', 'com_comite_comorbilidad', 'id_comite', 'id_comorbilidad');
    }

    public function cie10()
    {
        return $this->belongsTo('App\Cie10', 'id_cie10');
    }

    public function integrantes()
    {
        return $this->belongsToMany('App\User', 'com_comite_integrante', 'id_comite', 'id_medico');
    }

    public function tratamientos()
    {
        return $this->belongsToMany('App\Tratamiento', 'com_comite_tratamiento', 'id_comite', 'id_tratamiento');
    }

    public function formulario()
    {
        switch ($this->id_tipo_comite) {
            case 1:
                return $this->hasOne('App\FormularioRespiratorio', 'id_comite');
                break;
            
            default:
                # code...
                break;
        }
    }

    public function resolucion()
    {
        switch ($this->id_tipo_comite) {
            case 1:
                return $this->hasOne('App\ResolucionRespiratorio', 'id_comite');
                break;
            
            default:
                # code...
                break;
        }
    }
}
