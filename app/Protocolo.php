<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Protocolo extends Model
{
    protected $connection = 'mysql3';
    protected $table = 'cb_protocolo';
}
