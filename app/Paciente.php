<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Paciente extends Model
{
    protected $connection = 'mysql2';
    protected $table = 'gen_paciente';
    public $guarded = [];

    protected $appends = ['tx_nombre_completo', 'tx_rut', 'edad'];

    public function getTxNombreCompletoAttribute()
    {
        return ucwords(mb_strtolower("{$this->tx_nombre} {$this->tx_apellido_paterno} {$this->tx_apellido_materno}"));
    }

    public function getTxRutAttribute()
    {
        return "{$this->nr_run}-{$this->tx_digito_verificador}";
    }

    public function getEdadAttribute(){
        if($this->fc_nacimiento != null){
            $f1 = Carbon::createFromFormat('Y-m-d', $this->fc_nacimiento);
            $f2 = Carbon::now();
            $anos = $f2->diff($f1)->format('%y años');
            $dias = $f2->diff($f1)->format('%d días');
            $meses = $f2->diff($f1)->format('%m meses');
            $anos = $f2->diff($f1)->format('%y años');

            if($f2->diff($f1)->format('%d') == '1')
                $dias = $f2->diff($f1)->format('%d día');
            if($f2->diff($f1)->format('%m') == '1')
                $meses = $f2->diff($f1)->format('%m mes');
            if($f2->diff($f1)->format('%y') == '1')
                $anos = $f2->diff($f1)->format('%y año');

            if ($f2->diff($f1)->y == 0) {
                if ($f2->diff($f1)->format('%m') == '0'){
                    return $dias;
                } 
                return "{$meses} y {$dias}";
            }
            
            return "{$anos}, {$meses} y {$dias}";
        }
        return false;
    }
}