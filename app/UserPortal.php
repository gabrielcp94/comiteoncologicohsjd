<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserPortal extends Model
{
    protected $connection = 'mysql2';
    protected $table = 'users';
    protected $guarded = [];

    public static function boot()
    {
        parent::boot();

        static::creating(function($user)
        {
            $password = substr($user->rut, 0, 4);
            $user->password = $password;
            $user->password_old = $password;
        });
          
        static::created(function($user)
        {
            $password = substr($user->rut, 0, 4);
            $user->password = $password;
            $user->password_old = $password;
        });
    }

    public function setPasswordAttribute($value){
        $this->attributes['password'] = bcrypt(intval($value));
    }

    public function setPasswordOldAttribute($value){
        $this->attributes['password_old'] = bcrypt(intval($value));
    }

    public function sistemas()
    {
        return $this->belongsToMany('App\SistemaPortal', 'gen_user_sistemas', 'id_user', 'id_sistema');
    }
}
