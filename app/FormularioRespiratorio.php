<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FormularioRespiratorio extends Model
{
    protected $table = 'com_formulario_respiratorio';
    protected $guarded = [];
    public $timestamps = false;
}
