<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserGrupo extends Model
{
    protected $table = 'sec_users_groups';
    protected $guarded = [];
    public $timestamps = false;
}
