<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TipoComite extends Model
{
    protected $table = 'com_tipo_comite';

    protected $appends = ['tx_nombre_mayuscula'];

    public function getTxNombreMayusculaAttribute()
    {
        return ucwords(mb_strtolower("{$this->tx_nombre}"));
    }
}
