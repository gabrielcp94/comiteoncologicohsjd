<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class IntegrantesInfantil extends Model
{
    protected $table = 'integrantes_fecha_infantil';
    protected $guarded = [];
    public $timestamps = false;
}