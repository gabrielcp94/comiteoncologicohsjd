<?php

namespace App;

use Auth;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ComiteFormularioAdulto extends Model
{
    use SoftDeletes;
    protected $table = 'formulario';
    protected $guarded = [];

    public static function boot()
    {
        parent::boot();

        static::creating(function($comite)
        {
            $comite->tx_medico_tratante = Auth::user()->name;
        });
          
        static::created(function($comite)
        {
            $comite->tx_medico_tratante = Auth::user()->name;
        });
    }

    protected $appends = ['tx_nombre_completo', 'tx_medico', 'tx_telefono', 'edad'];

    public function getTxMedicoAttribute()
    {
        return ucwords(mb_strtolower("{$this->tx_medico_tratante}"));
    }

    public function getTxNombreCompletoAttribute()
    {
        return ucwords(mb_strtolower("{$this->tx_nombre} {$this->tx_apellido_paterno} {$this->tx_apellido_materno}"));
    }

    public function getTxTelefonoAttribute()
    {
        return "{$this->nr_telefono}";
    }

    public function setTxApellidoMaternoAttribute($value){
        if (is_null($value)) {
            $this->attributes['tx_apellido_materno'] = '';
        }else {
            $this->attributes['tx_apellido_materno'] = $value;
        }
    }

    public function setNrTelefonoAttribute($value){
        if (is_null($value)) {
            $this->attributes['nr_telefono'] = '0';
        }else {
            $this->attributes['nr_telefono'] = $value;
        }
    }

    public function getEdadAttribute(){
        if($this->fc_nacimiento != null){
            $f1 = Carbon::createFromFormat('Y-m-d', $this->fc_nacimiento);
            $f2 = Carbon::now();
            $anos = $f2->diff($f1)->format('%y años');
            $dias = $f2->diff($f1)->format('%d días');
            $meses = $f2->diff($f1)->format('%m meses');
            $anos = $f2->diff($f1)->format('%y años');

            if($f2->diff($f1)->format('%d') == '1')
                $dias = $f2->diff($f1)->format('%d día');
            if($f2->diff($f1)->format('%m') == '1')
                $meses = $f2->diff($f1)->format('%m mes');
            if($f2->diff($f1)->format('%y') == '1')
                $anos = $f2->diff($f1)->format('%y año');

            if ($f2->diff($f1)->y == 0) {
                if ($f2->diff($f1)->format('%m') == '0'){
                    return $dias;
                } 
                return "{$meses} y {$dias}";
            }
            
            return "{$anos}, {$meses} y {$dias}";
        }
        return false;
    }
    
    public function resolucion()
    {
		return $this->belongsTo('App\ResolucionAdulto', 'id');
    }
}
