<?php

namespace App;

use Auth;
use Illuminate\Database\Eloquent\Model;

class ComiteInfantil extends Model
{
    protected $table = 'cp_presentacion';
    protected $guarded = [];

    public static function boot()
    {
        parent::boot();

        static::creating(function($comite)
        {
            $comite->id_usuario = Auth::user()->login;
            $comite->fc_registro = date('Y-m-d');
            $comite->nr_telefono = 0;
        });
          
        static::created(function($comite)
        {
            $comite->created_by = Auth::id();
            $comite->id_especialidad = Auth::user()->tx_especialidad;
        });
    }

    public function paciente()
    {
		    return $this->belongsTo('App\Paciente', 'id_paciente_general');
    }

    public function medico()
    {
	  	    return $this->belongsTo('App\User', 'id_usuario', 'login');
    }

    public function formulario()
    {
            return $this->hasOne('App\FormularioInfantil', 'nr_comite', 'nr_comite');
    }

    public function resolucion()
    {
		    return $this->hasOne('App\ResolucionInfantil', 'nr_comite', 'nr_comite');
    }
}
