<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use App\Rules\isValidProfile;
use App\User;
use App\UserPortal;
use Illuminate\Support\Facades\Auth;
use Session;
use Symfony\Component\HttpFoundation\Request;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function username()
    {
        return 'login';
    }

    public function loginPortal(Request $request)
    {
        $request->validate([
            'rUser' => ['required', 'string', new isValidProfile],
            'tUser' => 'required|string',
        ]);

        $usr = UserPortal::where('rut', $request->rUser)->firstOrFail();
        if ($usr->token == $request->tUser) {
            $user = User::where('login', $request->rUser)->firstOrFail();

            Auth::loginUsingId($user->id);
            Session::put('token', $request->tUser);
            Session::put('id_user_portal', $usr->id);
            return $this->sendLoginResponse( $request);
        } else {
            return $this->sendFailedLoginResponse($request);
        }
    }
}
