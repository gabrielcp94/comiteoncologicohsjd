<?php

namespace App\Http\Controllers;

use App\ResolucionAdulto;
use App\ComiteFormularioAdulto;
use App\Parametros;
use App\IntegrantesAdulto;
use App\SolicitudPabellon;
use Illuminate\Http\Request;
use App\Http\Controllers\Helpers\InformesBiopsia;

class ResolucionAdultoController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    } 
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request, InformesBiopsia $informesBiopsia)
    {
        $comite = ComiteFormularioAdulto::find($request->id);
        $tratamientos = Parametros::where('tx_tipo', 'Tratamiento Propuesto')->get();
        $integrantes = IntegrantesAdulto::orderBy('fc_lista', 'ASC')->get();
        $url = "http://10.4.237.27/epicrisis-hsjd/api/epicrisis/get-all?token=5b7dbd9cf2d88&rut=$comite->tx_rut";
        $data = file_get_contents($url);
        $search  = array('\n\r', '\n', '\r');
        $data = str_replace($search,'',$data);
        $objson = json_decode($data, true);
        $arr_epicrises = $objson['epicrises'];
        $rutDV = explode("-",$comite->tx_rut);
        list('informes' => $informes, 'cuenta_biopsia' => $cuenta_biopsia,
        'resultado_consulta' => $resultado_consulta) = $informesBiopsia->fetch($rutDV[0], $rutDV[1]);
        $solicitudesPabellon = SolicitudPabellon::with('protocolo')->whereHas('protocolo')->where('gl_rut', $comite->tx_rut)->get();
        return view('comite.adulto.resolucion', compact('comite', 'tratamientos', 'integrantes', 'arr_epicrises', 'informes', 'solicitudesPabellon'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $comite = ComiteFormularioAdulto::where('id', $request->id)->update(['tx_historia_clinica' => $request->tx_historia_clinica]);
        if (isset($request->tratamientos)) {
            $tx_tratamiento_propuesto = Parametros::whereIn('id', $request->tratamientos)->get()->pluck('tx_descripcion')->implode(';');
            $request->request->add(['tx_tratamiento_propuesto' => $tx_tratamiento_propuesto]);
        }else{
            $tx_tratamiento_propuesto = null;
        }
        $id_integrantes_fecha = IntegrantesAdulto::find($request->id_integrantes_fecha);
        $request->request->add(['id_integrantes_fecha' => $id_integrantes_fecha->tx_integrantes]);
        $request->request->add(['tx_motivo' => '']);
        $resolucion = ResolucionAdulto::updateOrCreate(['id'=> $request->id], $request->except('_token', 'tratamientos', 'tx_historia_clinica'));
        if($resolucion){
            return redirect(session('url'))->with('message', "Se ha guardado exitosamente");
        }else{
            return redirect('/comiteAdulto?resuelto=0')->with('error', "No se ha guardado");
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ResolucionAdulto  $resolucionAdulto
     * @return \Illuminate\Http\Response
     */
    public function show(ResolucionAdulto $resolucionAdulto)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ResolucionAdulto  $resolucionAdulto
     * @return \Illuminate\Http\Response
     */
    public function edit(ResolucionAdulto $resolucionAdulto)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ResolucionAdulto  $resolucionAdulto
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ResolucionAdulto $resolucionAdulto)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ResolucionAdulto  $resolucionAdulto
     * @return \Illuminate\Http\Response
     */
    public function destroy(ResolucionAdulto $resolucionAdulto)
    {
        //
    }
}
