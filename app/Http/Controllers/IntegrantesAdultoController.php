<?php

namespace App\Http\Controllers;

use App\IntegrantesAdulto;
use App\User;
use Illuminate\Http\Request;

class IntegrantesAdultoController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $integrantes = IntegrantesAdulto::orderBy('fc_lista', 'DESC')
            ->when($request->has('medico') && !is_null($request->medico), function ($collection) use ($request) {
                return $collection->whereRaw("tx_integrantes LIKE ?", ['%'.$request->medico.'%']);
            })
            ->paginate(10);
        $medicos = User::get();
        return view('integrantes.adulto.index', compact('integrantes', 'medicos'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $medicos = User::get();
        return view('integrantes.adulto.register', compact('medicos'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if(isset($request->medicos)){
            $tx_integrantes = User::whereIn('id', $request->medicos)->get()->pluck('name')->implode(', ');
            $request->request->add(['tx_integrantes' => $tx_integrantes]);
        }
        $request->request->add(['tx_estado' => '']);
        if($request->bo_clonar == 1){
            $integrantes = IntegrantesAdulto::create($request->except('_token', 'medicos', 'bo_clonar', 'id'));
        }else{
            $integrantes = IntegrantesAdulto::updateOrCreate(['id' => $request->id], $request->except('_token', 'medicos', 'bo_clonar'));
        }
        if($integrantes){
            return redirect('/integrantesAdulto')->with('message', "Se ha guardado exitosamente");
        }else{
            return redirect('/integrantesAdulto')->with('error', "No se ha guardado");
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\IntegrantesAdulto  $integrantesAdulto
     * @return \Illuminate\Http\Response
     */
    public function show(IntegrantesAdulto $integrantesAdulto)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\IntegrantesAdulto  $integrantesAdulto
     * @return \Illuminate\Http\Response
     */
    public function edit(IntegrantesAdulto $integrantesAdulto)
    {
        $integrantes = $integrantesAdulto;
        return view('integrantes.adulto.register', compact('integrantes'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\IntegrantesAdulto  $integrantesAdulto
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, IntegrantesAdulto $integrantesAdulto)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\IntegrantesAdulto  $integrantesAdulto
     * @return \Illuminate\Http\Response
     */
    public function destroy(IntegrantesAdulto $integrantesAdulto)
    {
        //
    }

    public function clonarIntegrantesAdulto(Request $request)
    {
        $integrantes = IntegrantesAdulto::find($request->id);
        $bo_clonar = 1;
        return view('integrantes.adulto.register', compact('integrantes', 'bo_clonar'));
    }
}
