<?php

namespace App\Http\Controllers;

use App\IntegrantesInfantil;
use App\User;
use Illuminate\Http\Request;

class IntegrantesInfantilController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $integrantes = IntegrantesInfantil::orderBy('fc_lista', 'DESC')
            ->when($request->has('medico') && !is_null($request->medico), function ($collection) use ($request) {
                return $collection->whereRaw("tx_integrantes LIKE ?", ['%'.$request->medico.'%']);
            })
            ->paginate(10);
        $medicos = User::get();
        return view('integrantes.infantil.index', compact('integrantes', 'medicos'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $medicos = User::get();
        return view('integrantes.infantil.register', compact('medicos'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if(isset($request->medicos)){
            $tx_integrantes = User::whereIn('id', $request->medicos)->get()->pluck('name')->implode(', ');
            $request->request->add(['tx_integrantes' => $tx_integrantes]);
        }
        $request->request->add(['tx_estado' => 1]);
        // dd($request->all());
        if($request->bo_clonar == 1){
            $integrantes = IntegrantesInfantil::create($request->except('_token', 'medicos', 'bo_clonar', 'id'));
        }else{
            $integrantes = IntegrantesInfantil::updateOrCreate(['id' => $request->id], $request->except('_token', 'medicos', 'bo_clonar'));
        }
        if($integrantes){
            return redirect('/integrantesInfantil')->with('message', "Se ha guardado exitosamente");
        }else{
            return redirect('/integrantesInfantil')->with('error', "No se ha guardado");
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\IntegrantesInfantil  $integrantesInfantil
     * @return \Illuminate\Http\Response
     */
    public function show(IntegrantesInfantil $integrantesInfantil)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\IntegrantesInfantil  $integrantesInfantil
     * @return \Illuminate\Http\Response
     */
    public function edit(IntegrantesInfantil $integrantesInfantil)
    {
        $integrantes = $integrantesInfantil;
        return view('integrantes.infantil.register', compact('integrantes'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\IntegrantesInfantil  $integrantesInfantil
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, IntegrantesInfantil $integrantesInfantil)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\IntegrantesInfantil  $integrantesInfantil
     * @return \Illuminate\Http\Response
     */
    public function destroy(IntegrantesInfantil $integrantesInfantil)
    {
        //
    }

    public function clonarIntegrantesInfantil(Request $request)
    {
        $integrantes = IntegrantesInfantil::find($request->id);
        $bo_clonar = 1;
        return view('integrantes.infantil.register', compact('integrantes', 'bo_clonar'));
    }
}
