<?php

namespace App\Http\Controllers;

use Auth;
use DB;
use Illuminate\Http\Request;
use App\User;
use App\ComiteFormularioAdulto;
use App\Parametros;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth')->except('portal');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(Request $request)
    {
        $especialidad = Parametros::find(Auth::user()->tx_especialidad);
        $estadisticas = ComiteFormularioAdulto::with('resolucion')
                ->where('tx_especialidad', $especialidad->tx_descripcion)
                ->get();

        $estadistica['porResolver'] = $estadisticas->filter(function ($value, $key){
                $i = false;
                if((!isset($value->resolucion) || $value->resolucion->tx_estado != 'RESUELTO') && $value->tx_medico_tratante == Auth::user()->name){
                    $i = true;
                }
                return $i;
            })->count();

        $estadistica['porResolverMes'] = $estadisticas->filter(function ($value, $key){
                $i = false;
                if((!isset($value->resolucion) || $value->resolucion->tx_estado != 'RESUELTO') && $value->tx_medico_tratante == Auth::user()->name){
                    if(date("Y-m", strtotime($value->fc_ingreso)) == date("Y-m")){
                        $i = true;
                    }
                }
                return $i;
            })->count();

        $estadistica['porResolverAno'] = $estadisticas->filter(function ($value, $key){
                $i = false;
                if((!isset($value->resolucion) || $value->resolucion->tx_estado != 'RESUELTO') && $value->tx_medico_tratante == Auth::user()->name){
                    if(date("Y", strtotime($value->fc_ingreso)) == date("Y")){
                        $i = true;
                    }
                }
                return $i;
            })->count();

        $estadistica['resuelto'] = $estadisticas->filter(function ($value, $key){
                $i = false;
                if(isset($value->resolucion) && $value->resolucion->tx_estado == 'RESUELTO' && $value->tx_medico_tratante == Auth::user()->name){
                    $i = true;
                }
                return $i;
            })->count();
        
        $estadistica['resueltoMes'] = $estadisticas->filter(function ($value, $key){
                $i = false;
                if(isset($value->resolucion) && $value->resolucion->tx_estado == 'RESUELTO' && $value->tx_medico_tratante == Auth::user()->name){
                    if(date("Y-m", strtotime($value->fc_ingreso)) == date("Y-m")){
                        $i = true;
                    }
                }
                return $i;
            })->count();

        $estadistica['resueltoAno'] = $estadisticas->filter(function ($value, $key){
                $i = false;
                if(isset($value->resolucion) && $value->resolucion->tx_estado == 'RESUELTO' && $value->tx_medico_tratante == Auth::user()->name){
                    if(date("Y", strtotime($value->fc_ingreso)) == date("Y")){
                        $i = true;
                    }
                }
                return $i;
            })->count();
        
        $estadistica['porResolverEspecialidad'] = $estadisticas->filter(function ($value, $key){
                $i = false;
                if((!isset($value->resolucion) || $value->resolucion->tx_estado != 'RESUELTO')){
                    $i = true;
                }
                return $i;
            })->count();

        $estadistica['porResolverEspecialidadMes'] = $estadisticas->filter(function ($value, $key){
                $i = false;
                if((!isset($value->resolucion) || $value->resolucion->tx_estado != 'RESUELTO')){
                    if(date("Y-m", strtotime($value->fc_ingreso)) == date("Y-m")){
                        $i = true;
                    }
                }
                return $i;
            })->count();

        $estadistica['porResolverEspecialidadAno'] = $estadisticas->filter(function ($value, $key){
                $i = false;
                if((!isset($value->resolucion) || $value->resolucion->tx_estado != 'RESUELTO')){
                    if(date("Y", strtotime($value->fc_ingreso)) == date("Y")){
                        $i = true;
                    }
                }
                return $i;
            })->count();

        $estadisticasTotal = ComiteFormularioAdulto::with('resolucion')
                ->doesnthave('resolucion')
                ->orWhereHas('resolucion',
                                function ($query) use ($request) {
                                    $query->where('tx_estado', '!=', 'RESUELTO');
                                }
                            )
                ->get();

        $estadistica['porResolverTotal'] = $estadisticasTotal->count();

        $estadistica['porResolverTotalMes'] = $estadisticasTotal->filter(function ($value, $key){
            $i = false;
            if((!isset($value->resolucion) || $value->resolucion->tx_estado != 'RESUELTO')){
                if(date("Y-m", strtotime($value->fc_ingreso)) == date("Y-m")){
                    $i = true;
                }
            }
            return $i;
        })->count();

        $estadistica['porResolverTotalAno'] = $estadisticasTotal->filter(function ($value, $key){
            $i = false;
            if((!isset($value->resolucion) || $value->resolucion->tx_estado != 'RESUELTO')){
                if(date("Y", strtotime($value->fc_ingreso)) == date("Y")){
                    $i = true;
                }
            }
            return $i;
        })->count();

        $torta = [
            ['Language', 'Rating']
        ];

        $estadisticasResueltasTotal = ComiteFormularioAdulto::
            whereHas('resolucion', function ($query) use ($request) {
                $query->where('tx_estado', 'RESUELTO');
            })->groupBy('tx_especialidad')->select('tx_especialidad', DB::raw('count(*) as num'))->get();

        foreach ($estadisticasResueltasTotal as $key => $especialidad) {
            array_push($torta, [$especialidad->tx_especialidad, $especialidad->num]);
        }

        $torta2 = [
            ['Language', 'Rating']
        ];

        $estadisticasPorResolverTotal = ComiteFormularioAdulto::doesnthave('resolucion')
            ->orWhereHas('resolucion', function ($query) use ($request) {
                $query->where('tx_estado', '!=', 'RESUELTO');
            })->groupBy('tx_especialidad')->select('tx_especialidad', DB::raw('count(*) as num'))->get();

        foreach ($estadisticasPorResolverTotal as $key => $especialidad) {
            array_push($torta2, [$especialidad->tx_especialidad, $especialidad->num]);
        }

        return view('home', compact('estadistica', 'torta', 'torta2'));
    }

    public function portal(Request $request)
    {
        Auth::logout();
        return redirect('/loginPortal?rUser='.$request->rut.'&tUser='.$request->token.'');
    }
}
