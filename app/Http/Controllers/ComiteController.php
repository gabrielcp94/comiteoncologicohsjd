<?php

namespace App\Http\Controllers;

use App\Comite;
use App\User;
use App\Paciente;
use App\TipoComite;
use App\Establecimiento;
use App\Especialidad;
use App\Comorbilidad;
use App\Cie10;
use App\FormularioRespiratorio;
use Illuminate\Http\Request;
use App\Exports\ComiteExport;
use Maatwebsite\Excel\Facades\Excel;

class ComiteController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $comites = Comite::with('paciente')
            ->when($request->has('medicos') && !is_null($request->medicos), function ($collection) use ($request){
                $collection->whereIn('created_by', $request->medicos);
            })
            ->when($request->has('especialidades') && !is_null($request->especialidades), function ($collection) use ($request){
                $collection->whereIn('id_especialidad', $request->especialidades);
            })
            ->when($request->has('fc_inicio') && !is_null($request->fc_inicio), function ($collection) use ($request){
                return $collection->where('fc_presentacion', '>=', date("Y-m-d 00:00:00", strtotime($request->fc_inicio)))
                        ->where('fc_presentacion', '<=', date("Y-m-d 00:00:00", strtotime($request->fc_termino)));
            })
            ->when($request->resuelto == 0, function ($collection) use ($request){
                $collection->whereIn('bo_resuelto', [0, 73]);
            })
            ->when($request->resuelto == 1, function ($collection) use ($request){
                $collection->where('bo_resuelto', 72);
            })
            ->orderBy('id', 'DESC')
            ->paginate(10);

        $tipoComite = TipoComite::find($request->tipo_comite);
        $especialidades = Especialidad::get();
        $medicos = User::get();
        session(['url' => url()->full()]);
        return view('comite.index', compact('tipoComite', 'comites', 'especialidades', 'medicos'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $tipoComite = TipoComite::find($request->tipo_comite);
        $establecimientos = Establecimiento::get();
        $comorbilidades = Comorbilidad::get();
        return view('comite.register', compact('tipoComite', 'establecimientos', 'comorbilidades'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $pacienteRequest = [
            'nr_ficha' => $request->nr_ficha,
            'tx_apellido_paterno' => $request->tx_apellido_paterno,
            'tx_apellido_materno' => $request->tx_apellido_materno,
            'tx_nombre' => $request->tx_nombre,
            'fc_nacimiento' => $request->fc_nacimiento,
            'tx_telefono' => $request->tx_telefono,
            'tx_mail' => $request->tx_mail,
            'id_sexo' => $request->id_sexo,
            'id_prevision' => $request->id_prevision,
            'id_clasificacion_fonasa' => $request->id_clasificacion_fonasa,
        ];
        $rutDV = explode("-",$request->tx_rut);
        $paciente = Paciente::updateOrCreate([
            'nr_run' => $rutDV[0],
            'tx_digito_verificador' => $rutDV[1],
            'id_tipo_dentificacion_paciente' => 1,
        ], $pacienteRequest);
        
        $comiteRequest = [
            'fc_presentacion' => $request->fc_presentacion,
            'id_paciente' => $paciente->id,
            'id_tipo_comite' => $request->id_tipo_comite,
            'id_hospital_origen' => $request->id_hospital_origen,
            'id_cie10' => $request->id_cie10,
        ];
        if(isset($request->archivo)){
            $tx_archivo = $request->file('archivo')->store('public');
            $tx_archivo = substr($tx_archivo, 7);
            $comiteRequest['tx_archivo'] = $tx_archivo;
        }
        if($request->bo_clonar){
            $comite = Comite::create($comiteRequest);
        }else{
            $comite = Comite::updateOrCreate([
                'id' => $request->id,
            ], $comiteRequest);
        }

        $formularioRequest = $request->except('_token', 'id', 'id_sexo', 'id_prevision', 'id_clasificacion_fonasa', 'id_tipo_comite', 'bo_clonar', 'id_hospital_origen', 'fc_presentacion', 'tx_rut', 'tx_nombre', 'tx_apellido_paterno', 'tx_apellido_materno', 'fc_nacimiento', 'tx_mail', 'tx_telefono', 'nr_ficha', 'tx_paciente_nuevo', 'comorbilidades', 'archivo', 'id_cie10');
        switch ($request->id_tipo_comite) {
            case 1:
                $formulario = FormularioRespiratorio::updateOrCreate([
                    'id_comite' => $comite->id,
                ], $formularioRequest);
                break;
            case 2:
                # code...
                break;
            default:
                # code...
                break;
        }
        $comite->comorbilidades()->sync($request->comorbilidades);
        if($comite){
            return redirect('/comite?resuelto=0&tipo_comite='.$request->id_tipo_comite)->with('message', "Se ha guardado exitosamente");
        }else{
            return redirect('/comite?resuelto=0&tipo_comite='.$request->id_tipo_comite)->with('error', "No se ha guardado");
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Comite  $comite
     * @return \Illuminate\Http\Response
     */
    public function show(Comite $comite)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Comite  $comite
     * @return \Illuminate\Http\Response
     */
    public function edit(Comite $comite)
    {
        $comite = Comite::with('comorbilidades', 'cie10')->find($comite->id);
        $formulario = $comite->formulario;
        $tipoComite = TipoComite::find($comite->id_tipo_comite);
        $establecimientos = Establecimiento::get();
        $comorbilidades = Comorbilidad::get();
        return view('comite.register', compact('comite', 'formulario', 'tipoComite', 'establecimientos', 'comorbilidades'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Comite  $comite
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Comite $comite)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Comite  $comite
     * @return \Illuminate\Http\Response
     */
    public function destroy(Comite $comite)
    {
        //
    }

    public function pdfComite(Request $request)
    {
        $comite = Comite::with('paciente')->find($request->id);
     
        $pdf = \PDF::loadView('comite.pdf', compact('comite'));
     
        return $pdf->stream('archivo.pdf');
    }

    public function clonar(Request $request)
    {
        $comite = Comite::with('comorbilidades', 'cie10')->find($request->id);
        $tipoComite = TipoComite::find($comite->id_tipo_comite);
        $formulario = $comite->formulario;
        $establecimientos = Establecimiento::get();
        $comorbilidades = Comorbilidad::get();
        $bo_clonar = 1;
        return view('comite.register', compact('comite', 'tipoComite', 'formulario', 'establecimientos', 'comorbilidades', 'bo_clonar'));
    }

    public function excelComite(Request $request)
    {
        $comites = Comite::with('paciente')
            ->when($request->has('medicos') && !is_null($request->medicos), function ($collection) use ($request){
                $collection->whereIn('created_by', $request->medicos);
            })
            ->when($request->has('especialidades') && !is_null($request->especialidades), function ($collection) use ($request){
                $collection->whereIn('id_especialidad', $request->especialidades);
            })
            ->when($request->has('fc_inicio') && !is_null($request->fc_inicio), function ($collection) use ($request){
                return $collection->where('fc_presentacion', '>=', date("Y-m-d 00:00:00", strtotime($request->fc_inicio)))
                        ->where('fc_presentacion', '<=', date("Y-m-d 00:00:00", strtotime($request->fc_termino)));
            })
            ->when($request->resuelto == 0, function ($collection) use ($request){
                $collection->whereIn('bo_resuelto', [0, 73]);
            })
            ->when($request->resuelto == 1, function ($collection) use ($request){
                $collection->where('bo_resuelto', 72);
            })
            ->orderBy('id', 'DESC')
            ->get();
        return Excel::download(new ComiteExport($comites), 'comiteRespiratorio'.$request->fc_inicio.'&'.$request->fc_termino.'.xlsx');
    }
}
