<?php

namespace App\Http\Controllers;

use App\ComiteFormularioAdulto;
use App\User;
use App\Parametros;
use App\Cie10;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Session;
use App\Exports\ComiteAdultoExport;
use Maatwebsite\Excel\Facades\Excel;

class ComiteAdultoController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if (isset($request->especialidades)) {
            $especialidades = Parametros::whereIn('id', $request->especialidades)->pluck('tx_descripcion')->toArray();
        }else{
            $especialidades = null;
        }
        if (isset($request->medicos)) {
            $medicos = User::whereIn('id', $request->medicos)->pluck('name')->toArray();
        }else{
            $medicos = null;
        }
        $comites = ComiteFormularioAdulto::with('resolucion')
                ->when($request->has('medicos') && !is_null($request->medicos), function ($collection) use ($medicos){
                    $collection->whereIn('tx_medico_tratante', $medicos);
                })
                ->when($request->has('especialidades') && !is_null($request->especialidades), function ($collection) use ($especialidades){
                    $collection->whereIn('tx_especialidad', $especialidades);
                })
                ->when($request->has('fc_inicio') && !is_null($request->fc_inicio), function ($collection) use ($request){
                    return $collection->where('fc_ingreso', '>=', date("Y-m-d 00:00:00", strtotime($request->fc_inicio)))
                            ->where('fc_ingreso', '<=', date("Y-m-d 00:00:00", strtotime($request->fc_termino)));
                })
                ->when($request->resuelto == 0, function ($collection) use ($request){
                    $collection->where(function ($q) use ($request){
                        $q->doesnthave('resolucion')
                            ->orWhereHas('resolucion',
                                function ($query) use ($request) {
                                    $query->where('tx_estado', '!=', 'RESUELTO');
                                }
                            );
                    });
                })
                ->when($request->resuelto == 1, function ($collection) use ($request){
                    $collection->whereHas('resolucion', function ($query) use ($request) {
                        $query->where('tx_estado', 'RESUELTO');
                    });
                })
                ->orderBy('id', 'DESC')
                ->paginate(10);
        $especialidades = Parametros::where('tx_tipo', 'Especialidad')->get();
        $medicos = User::get();
        session(['url' => url()->full()]);
        return view('comite.adulto.index', compact('comites', 'especialidades', 'medicos'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $establecimientos = Parametros::where('tx_tipo', 'Establecimiento')->get();
        $today = date('Y-m-d');
        return view('comite.adulto.register', compact('establecimientos', 'today'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $cie10 = Cie10::find($request->id_cie10);
        $request->request->add(['tx_Cie10' => $cie10->dec10]);

        if(isset($request->archivo)){
            $tx_archivo = $request->file('archivo')->store('public');
            $tx_archivo = substr($tx_archivo, 7);
            $request->request->add(['tx_archivo' => $tx_archivo]);
        }
        if (!isset($request->tx_paciente_nuevo)) {
            $request->request->add(['tx_paciente_nuevo' => null]);
        }
        $edad = carbon::now()->diff($request->fc_nacimiento);
        $nr_edad = $edad->y.' años '.$edad->m.' meses '.$edad->d.' días';
        $request->request->add(['nr_edad' => $nr_edad]);
        $tx_comorbilidad = '';
        if(isset($request->cardiopatia)){
            $tx_comorbilidad = $tx_comorbilidad.$request->cardiopatia.';';
        }
        if(isset($request->diabetes)){
            $tx_comorbilidad = $tx_comorbilidad.$request->diabetes.';';
        }
        if(isset($request->hipertension)){
            $tx_comorbilidad = $tx_comorbilidad.$request->hipertension.';';
        }
        if(isset($request->dhc)){
            $tx_comorbilidad = $tx_comorbilidad.$request->dhc.';';
        }
        if(isset($request->enfermedad_pulmonar)){
            $tx_comorbilidad = $tx_comorbilidad.$request->enfermedad_pulmonar.';';
        }
        $request->request->add(['tx_comorbilidad' => $tx_comorbilidad]);
        if($request->bo_clonar == 0){
            $comite = ComiteFormularioAdulto::updateOrCreate(['id'=> $request->id], $request->except('_token', 'tx_mail', 'fc_presentacion', 'bo_clonar', 'cardiopatia', 'diabetes', 'hipertension', 'dhc', 'enfermedad_pulmonar', 'archivo', 'id_cie10'));
        }else{
            $comite = ComiteFormularioAdulto::create($request->except('_token', 'tx_mail', 'fc_presentacion', 'bo_clonar', 'cardiopatia', 'diabetes', 'hipertension', 'dhc', 'enfermedad_pulmonar', 'archivo', 'id_cie10'));
        }
        if($comite){
            return redirect('/comiteAdulto?resuelto=0')->with('message', "Se ha guardado exitosamente");
        }else{
            return redirect('/comiteAdulto?resuelto=0')->with('error', "No se ha guardado");
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ComiteFormularioAdulto  $comiteFormularioAdulto
     * @return \Illuminate\Http\Response
     */
    public function show(ComiteFormularioAdulto $comiteFormularioAdulto)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ComiteFormularioAdulto  $comiteFormularioAdulto
     * @return \Illuminate\Http\Response
     */
    public function edit(ComiteFormularioAdulto $comiteAdulto)
    {
        $comite = $comiteAdulto;
        $comite->cie10 = Cie10::where('dec10', $comite->tx_Cie10)->first();
        $establecimientos = Parametros::where('tx_tipo', 'Establecimiento')->get();
        return view('comite.adulto.register', compact('comite', 'cie10', 'establecimientos'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ComiteFormularioAdulto  $comiteFormularioAdulto
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ComiteFormularioAdulto $comiteFormularioAdulto)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ComiteFormularioAdulto  $comiteFormularioAdulto
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $comiteFormularioAdulto = ComiteFormularioAdulto::find($id);

        if($comiteFormularioAdulto->delete()){
            return redirect()->back()->with('message', "El comite a sido eliminado correctamente");
        }else{
            return redirect()->back()->with('error', "El comite no a sido eliminado, intente nuevamente");
        }
    }

    public function pdfComiteAdulto(Request $request)
    {
        $comite = ComiteFormularioAdulto::with('resolucion')->find($request->id);
     
        $pdf = \PDF::loadView('comite.adulto.pdf', compact('comite'));
     
        return $pdf->stream('archivo.pdf');
    }

    public function clonarAdulto(Request $request)
    {
        $comite = ComiteFormularioAdulto::find($request->id);
        $comite->cie10 = Cie10::where('dec10', $comite->tx_Cie10)->first();
        $establecimientos = Parametros::where('tx_tipo', 'Establecimiento')->get();
        $bo_clonar = 1;
        return view('comite.adulto.register', compact('comite', 'establecimientos', 'bo_clonar'));
    }

    public function excelAdulto(Request $request)
    {
        if (isset($request->especialidades)) {
            $especialidades = Parametros::whereIn('id', $request->especialidades)->pluck('tx_descripcion')->toArray();
        }else{
            $especialidades = null;
        }
        if (isset($request->medicos)) {
            $medicos = User::whereIn('id', $request->medicos)->pluck('name')->toArray();
        }else{
            $medicos = null;
        }
        $comites = ComiteFormularioAdulto::with('resolucion')
                ->when($request->has('medicos') && !is_null($request->medicos), function ($collection) use ($medicos){
                    $collection->whereIn('tx_medico_tratante', $medicos);
                })
                ->when($request->has('especialidades') && !is_null($request->especialidades), function ($collection) use ($especialidades){
                    $collection->whereIn('tx_especialidad', $especialidades);
                })
                ->when($request->has('fc_inicio') && !is_null($request->fc_inicio), function ($collection) use ($request){
                    return $collection->where('fc_ingreso', '>=', date("Y-m-d 00:00:00", strtotime($request->fc_inicio)))
                            ->where('fc_ingreso', '<=', date("Y-m-d 00:00:00", strtotime($request->fc_termino)));
                })
                ->when($request->resuelto == 0, function ($collection) use ($request){
                    $collection->where(function ($q) use ($request){
                        $q->doesnthave('resolucion')
                            ->orWhereHas('resolucion',
                                function ($query) use ($request) {
                                    $query->where('tx_estado', '!=', 'RESUELTO');
                                }
                            );
                    });
                })
                ->when($request->resuelto == 1, function ($collection) use ($request){
                    $collection->whereHas('resolucion', function ($query) use ($request) {
                        $query->where('tx_estado', 'RESUELTO');
                    });
                })
                ->orderBy('id')
                ->get();
        return Excel::download(new ComiteAdultoExport($comites), 'comiteAdulto'.$request->fc_inicio.'&'.$request->fc_termino.'.xlsx');
    }

    public function estadisticasAdulto(Request $request)
    {
        $comites = ComiteFormularioAdulto::with('resolucion')
                ->when($request->has('fc_inicio') && !is_null($request->fc_inicio), function ($collection) use ($request){
                    return $collection->where('fc_ingreso', '>=', date("Y-m-d 00:00:00", strtotime($request->fc_inicio)))
                            ->where('fc_ingreso', '<=', date("Y-m-d 00:00:00", strtotime($request->fc_termino)));
                })
                // ->when($resuelto == 1, function ($collection) use ($request){
                //     $collection->where(function ($q) use ($request){
                //         $q->doesnthave('resolucion')
                //             ->orWhereHas('resolucion',
                //                 function ($query) use ($request) {
                //                     $query->where('tx_estado', '!=', 'RESUELTO');
                //                 }
                //             );
                //     });
                // })
                // ->when($resuelto == 0, function ($collection) use ($request){
                //     $collection->whereHas('resolucion', function ($query) use ($request) {
                //         $query->where('tx_estado', 'RESUELTO');
                //     });
                // })
                // ->groupBy('tx_Cie10')
                // ->selectRaw('count(*) as total, tx_Cie10')
                // ->orderBy('total', 'DESC')
                ->get();
        //         foreach ($comites as $key => $comite) {
        //             dump($comite);
        //         }
        $comitesPorCie10 = $comites->groupBy('tx_Cie10');
        $rangoResuelto[1] = 0;
        $rangoResuelto[2] = 0;
        $rangoResuelto[3] = 0;
        $rangoResuelto[4] = 0;
        $rangoPorResolver[1] = 0;
        $rangoPorResolver[2] = 0;
        $rangoPorResolver[3] = 0;
        $rangoPorResolver[4] = 0;
        foreach ($comitesPorCie10 as $key => $comitePorCie10) {
            $resuelto = 0;
            $porResolver = 0;
            foreach ($comitePorCie10 as $key2 => $value) {
                if(isset($value->resolucion) && $value->resolucion->tx_estado == 'RESUELTO'){
                    $resuelto++;
                    $años = explode(' ', $value->edad);
                    $años = intval($años[0]);
                    if($años < 18){
                        $rangoResuelto[1]++;
                    }elseif($años >= 18 && $años <= 40){
                        $rangoResuelto[2]++;
                    }elseif($años > 40 && $años <= 60){
                        $rangoResuelto[3]++;
                    }else{
                        $rangoResuelto[4]++;
                    }
                }else{
                    $porResolver++;
                    $años = explode(' ', $value->edad);
                    $años = intval($años[0]);
                    if($años < 18){
                        $rangoPorResolver[1]++;
                    }elseif($años >= 18 && $años <= 40){
                        $rangoPorResolver[2]++;
                    }elseif($años > 40 && $años <= 60){
                        $rangoPorResolver[3]++;
                    }else{
                        $rangoPorResolver[4]++;
                    }
                }
            }
            $comitePorCie10->nombre = $key;
            $comitePorCie10->resuelto = $resuelto;
            $comitePorCie10->porResolver = $porResolver;
            $comitePorCie10->total = $resuelto + $porResolver;
        }
        return view('comite.adulto.estadisticas', compact('comites', 'comitesPorCie10', 'rangoResuelto', 'rangoPorResolver'));
    }
}