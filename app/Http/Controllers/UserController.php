<?php

namespace App\Http\Controllers;

use Auth;
use App\User;
use App\UserPortal;
use App\UserGrupo;
use App\Parametros;
use App\Grupo;
use Illuminate\Http\Request;

class UserController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->authorizeResource(User::class, 'user');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $tx_rut = str_replace(".","",request()->tx_rut);
        $users = User::with('especialidad', 'establecimiento')
                ->when($request->has('tx_rut') && !is_null($request->tx_rut), function ($collection) use ($request, $tx_rut) {
                    return $collection->whereRaw("login LIKE ?", ['%'.$tx_rut.'%']);
                })
                ->when($request->has('tx_nombre') && !is_null($request->tx_nombre), function ($collection) use ($request) {
                    return $collection->whereRaw("name LIKE ?", ['%'.$request->tx_nombre.'%']);
                })
                ->orderBy('id')
                ->paginate(10);
        return view('user.index', compact('users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->authorize('admin', User::class);
        $especialidades = Parametros::where('tx_tipo', 'Especialidad')->get();
        $establecimientos = Parametros::where('tx_tipo', 'Establecimiento')->get();
        $grupos = Grupo::get();
        return view('user.register', compact('especialidades', 'establecimientos', 'grupos'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $userPortal = UserPortal::with('sistemas')->where('rut', $request->login)->first();
        if(isset($userPortal)){
            $userPortal->sistemas()->attach(2);
        }else{
            $userRequest = [
                'rut' => $request->login,
                'nombre' => $request->name,
            ];
            $userPortal = UserPortal::create($userRequest);
            $userPortal->sistemas()->sync([2]);
        }
        if (!isset($request->active)) {
            $request->request->add(['active' => 'N']);
        }
        $user = User::updateOrCreate(['login'=> $request->login], $request->except('_token', 'miUser', 'group_id'));
        if (!isset($request->miUser)) {
            $userGrupo = UserGrupo::updateOrCreate([
                'login'=> $request->login
            ],[
                'login'=> $request->login,
                'group_id'=> $request->group_id
            ]);        
        }
        if (!$user) {
            return redirect()->back()->with('error', 'Error al guardar');
        }
        if (!isset($request->miUser)){
            return redirect('/user')->with('message', 'El usuario '.$user->name.' a sido guardado exitosamente');
        }
        return redirect('/home')->with('message', 'El usuario '.$user->name.' a sido guardado exitosamente');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user)
    {
        // $user = User::with('especialidad', 'establecimiento', 'grupo')->find($user->id);
        $especialidades = Parametros::where('tx_tipo', 'Especialidad')->get();
        $establecimientos = Parametros::where('tx_tipo', 'Establecimiento')->get();
        $grupos = Grupo::get();
        return view('user.register', compact('user', 'especialidades', 'establecimientos', 'grupos'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $user)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        $user->grupo()->delete();
        if($user->forceDelete()){
            return redirect('/user')->with('message', "El usuario a sido eliminado correctamente");
        }else{
            return redirect('/user')->with('error', "El usuario no a sido eliminado, intente nuevamente");
        }
    }

    public function resetPass($id) // Reiniciar Contraseña
    {
        $user = User::find($id);

        // PARA REINICIAR TODAS LAS CONTRASEÑAS BCRYPT (AGREGAR password A LA TABLA cb_usuario)
        // $users = User::get();
        // foreach ($users as $key => $user) {
        //     $password = substr($user->login, 0, 4);
        //     $user->password = $password;
        //     $user->save();
        // }

        $password = substr($user->login, 0, 4);
        $user->password = $password;
        $user->save();

        return redirect('/user')->with('message', "La contraseña del usuario ".$user->name."  a sido reiniciada");
    }

    public function miUser() // Modificar mi usuario
    {
        $user = User::find(Auth::user()->id);
        $especialidades = Parametros::where('tx_tipo', 'Especialidad')->get();
        $establecimientos = Parametros::where('tx_tipo', 'Establecimiento')->get();
        $grupos = Grupo::get();
        $miUser = true;
        return view('user.register', compact('user', 'especialidades', 'establecimientos', 'grupos', 'miUser'));
    }
}
