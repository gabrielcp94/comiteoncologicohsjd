<?php

namespace App\Http\Controllers;

use App\Comite;
use App\User;
use App\TipoComite;
use App\Tratamiento;
use App\SolicitudPabellon;
use App\ResolucionRespiratorio;
use Illuminate\Http\Request;
use Carbon\Carbon;
use App\Http\Controllers\Helpers\InformesBiopsia;

class ResolucionController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request, InformesBiopsia $informesBiopsia)
    {
        $comite = Comite::with('tipoComite')->find($request->id);
        $rut = $comite->paciente->tx_rut;
        $medicos = User::orderBy('name')->get();
        $integrantes = Comite::with('integrantes')->whereHas('integrantes')->where('id_tipo_comite', $request->tipo_comite)->latest()->first();
        if(isset($integrantes->integrantes)){
            $integrantes = $integrantes->integrantes->pluck('id')->toArray();
        }else{
            $integrantes = array();
        }
        $tratamientos = Tratamiento::get();
        $edad = carbon::now()->diff($comite->paciente->fc_nacimiento);
        $url = "http://10.4.237.27/epicrisis-hsjd/api/epicrisis/get-all?token=5b7dbd9cf2d88&rut=$rut";
        $data = file_get_contents($url);
        $search  = array('\n\r', '\n', '\r');
        $data = str_replace($search,'',$data);
        $objson = json_decode($data, true);
        $arr_epicrises = $objson['epicrises'];
        $rutDV = explode("-",$rut);
        list('informes' => $informes, 'cuenta_biopsia' => $cuenta_biopsia,
        'resultado_consulta' => $resultado_consulta) = $informesBiopsia->fetch($rutDV[0], $rutDV[1]);
        $solicitudesPabellon = SolicitudPabellon::with('protocolo')->whereHas('protocolo')->where('gl_rut', $rut)->get();
        return view('comite.resolucion', compact('request', 'comite', 'medicos', 'integrantes', 'tratamientos', 'edad', 'arr_epicrises', 'informes', 'solicitudesPabellon'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        switch ($request->id_tipo_comite) {
            case 1:
                $resolucion = ResolucionRespiratorio::updateOrCreate([
                    'id_comite' => $request->id_comite,
                ], $request->except('_token', 'tratamientos', 'bo_resuelto', 'id_tipo_comite', 'integrantes'));
                break;
            
            default:
                # code...
                break;
        }
        $comite = Comite::where('id', $request->id_comite)->update(['bo_resuelto' => $request->bo_resuelto]);
        $comite = Comite::find($request->id_comite);
        $comite->integrantes()->sync($request->integrantes);
        $comite->tratamientos()->sync($request->tratamientos);
        if($resolucion){
            return redirect(session('url'))->with('message', "Se ha guardado exitosamente");
        }else{
            return redirect('/comite?resuelto=0&tipo_comite='.$request->id_tipo_comite)->with('error', "No se ha guardado");
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id, InformesBiopsia $informesBiopsia)
    {
        $comite = Comite::with('tipoComite', 'tratamientos', 'integrantes')->find($id);
        switch ($comite->id_tipo_comite) {
            case 1:
                $resolucion = ResolucionRespiratorio::where('id_comite', $id)->first();
                break;
            
            default:
                # code...
                break;
        }
        $rut = $comite->paciente->tx_rut;
        $medicos = User::orderBy('name')->get();
        $integrantes = Comite::with('integrantes')->find($id);
        if(isset($integrantes->integrantes)){
            $integrantes = $integrantes->integrantes->pluck('id')->toArray();
        }else{
            $integrantes = array();
        }
        $tratamientos = Tratamiento::get();
        $edad = carbon::now()->diff($comite->paciente->fc_nacimiento);
        $url = "http://10.4.237.27/epicrisis-hsjd/api/epicrisis/get-all?token=5b7dbd9cf2d88&rut=$rut";
        $data = file_get_contents($url);
        $search  = array('\n\r', '\n', '\r');
        $data = str_replace($search,'',$data);
        $objson = json_decode($data, true);
        $arr_epicrises = $objson['epicrises'];
        $rutDV = explode("-",$rut);
        list('informes' => $informes, 'cuenta_biopsia' => $cuenta_biopsia,
        'resultado_consulta' => $resultado_consulta) = $informesBiopsia->fetch($rutDV[0], $rutDV[1]);
        $solicitudesPabellon = SolicitudPabellon::with('protocolo')->whereHas('protocolo')->where('gl_rut', $rut)->get();
        return view('comite.resolucion', compact('comite', 'resolucion', 'medicos', 'integrantes', 'tratamientos', 'edad', 'arr_epicrises', 'informes', 'solicitudesPabellon'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
