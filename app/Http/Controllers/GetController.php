<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Api\FonasaApi;
use Carbon\Carbon;
use App\User;
use App\Paciente;
use App\Cie10;

class GetController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function getDatosRut(Request $request, FonasaApi $fonasaApi)
    {
        $rut = str_replace(".","",request()->rut);
        $rutDV = explode("-", $rut);
        
        if(!isset($rutDV[1])){
            $data['encontrado'] = false;
            return $data;
        }
        $paciente = Paciente::where('id_tipo_dentificacion_paciente', 1)->where('nr_run', $rutDV[0])->first();
        $data = $fonasaApi->fetchNormalized($rutDV[0], $rutDV[1]);
        if(isset($paciente)){
            $data['tx_mail'] = $paciente->tx_mail;
            $data['tx_telefono'] = $paciente->tx_telefono;
            $data['nr_ficha'] = $paciente->nr_ficha;
        }
        if($data['nr_run'] == null){
            $data['encontrado'] = false;
            return $data;
        }
        $fch = explode("/",$data['fecha_nacimiento']);
        $data['fc_nacimiento'] = $fch[2]."-".$fch[1]."-".$fch[0];
        $data['tx_nombre_solo'] = $data['tx_nombre'];
        $data['tx_nombre'] = $data['tx_nombre']." ".$data['tx_apellido_paterno']." ".$data['tx_apellido_materno'];
        $data['tx_rut'] = $data['rut'];
        $user = User::where('login', $rut)->first();
        if(isset($user)){
            $data['user'] = true;
            $data['id'] = $user->id;
        }else{
            $data['user'] = false;
        }
        return $data;
    }

    public function getDiagnostico(Request $request)
    {
        $diagnosticos = Cie10::whereRaw("dec10 LIKE ?", ['%'.$request->search.'%'])->get();
        $data['results'] = array();
        foreach($diagnosticos as $key => $diagnostico){
            $producto = array();
            $producto['id'] = $diagnostico->id;
            $producto['text'] = "{$diagnostico->dec10}"; 
            array_push($data['results'], $producto);
        }
        $data['pagination'] = ["more" => false];
        return $data;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
