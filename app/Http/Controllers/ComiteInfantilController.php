<?php

namespace App\Http\Controllers;

use App\ComiteInfantil;
use App\User;
use App\Paciente;
use App\Parametros;
use App\FormularioInfantil;
use Illuminate\Http\Request;
use App\Exports\ComiteInfantilExport;
use Maatwebsite\Excel\Facades\Excel;

class ComiteInfantilController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    } 
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if (isset($request->medicos)) {
            $medicos = User::whereIn('id', $request->medicos)->pluck('login')->toArray();
        }else{
            $medicos = null;
        }
        $comites = ComiteInfantil::with('paciente', 'medico') 
                ->when($request->has('medicos') && !is_null($request->medicos), function ($collection) use ($medicos){
                    $collection->whereIn('id_usuario', $medicos);
                })           
                ->when($request->has('fc_inicio') && !is_null($request->fc_inicio), function ($collection) use ($request){
                    return $collection->where('fc_presentacion', '>=', date("Y-m-d 00:00:00", strtotime($request->fc_inicio)))
                            ->where('fc_presentacion', '<=', date("Y-m-d 00:00:00", strtotime($request->fc_termino)));
                })
                ->when($request->resuelto == 0, function ($collection) use ($request){
                    $collection->doesnthave('resolucion');
                })
                ->when($request->resuelto == 1, function ($collection) use ($request){
                    $collection->whereHas('resolucion');
                })    
                ->orderBy('nr_comite', 'DESC')
                ->paginate(10);
        $medicos = User::get();
        session(['url' => url()->full()]);
        return view('comite.infantil.index', compact('comites', 'medicos'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $establecimientos = Parametros::where('tx_tipo', 'Establecimiento')->get();
        $today = date('Y-m-d');
        return view('comite.infantil.register', compact('establecimientos', 'today'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $pacienteRequest = [
            'nr_ficha' => $request->nr_ficha,
            'tx_apellido_paterno' => $request->tx_apellido_paterno,
            'tx_apellido_materno' => $request->tx_apellido_materno,
            'tx_nombre' => $request->tx_nombre,
            'fc_nacimiento' => $request->fc_nacimiento,
            'tx_telefono' => $request->tx_telefono,
            'tx_mail' => $request->tx_mail,
            'id_sexo' => $request->id_sexo,
            'id_prevision' => $request->id_prevision,
            'id_clasificacion_fonasa' => $request->id_clasificacion_fonasa,
        ];
        $rutDV = explode("-",$request->tx_rut);
        $paciente = Paciente::updateOrCreate([
            'nr_run' => $rutDV[0],
            'tx_digito_verificador' => $rutDV[1],
            'id_tipo_dentificacion_paciente' => 1,
        ], $pacienteRequest);

        if(isset($request->nr_comite)){
            $nr_comite = $request->nr_comite;
        }else{
            $nr_comite = ComiteInfantil::max('nr_comite') + 1;
        }
        $comiteRequest = [
            'id_paciente_general' => $paciente->id,
            'fc_presentacion' => $request->fc_presentacion,
        ];
        if($request->bo_clonar){
            $comiteRequest['nr_comite'] = $nr_comite;
            $comite = ComiteInfantil::create($comiteRequest);
        }else{
            $comite = ComiteInfantil::updateOrCreate([
                'nr_comite' => $nr_comite,
            ], $comiteRequest);
        }

        if(!isset($request->tx_antecedentes_personales)){
            $request->request->add(['tx_antecedentes_personales' => '']);
        }
        if(!isset($request->tx_antecedentes_familiares)){
            $request->request->add(['tx_antecedentes_familiares' => '']);
        }
        if(!isset($request->tx_historia)){
            $request->request->add(['tx_historia' => '']);
        }
        if(!isset($request->tx_resultado_laboratorio)){
            $request->request->add(['tx_resultado_laboratorio' => '']);
        }
        if(!isset($request->tx_diagnostico_actual)){
            $request->request->add(['tx_diagnostico_actual' => '']);
        }
        if(!isset($request->tx_examen)){
            $request->request->add(['tx_examen' => '']);
        }
        if(!isset($request->tx_plan)){
            $request->request->add(['tx_plan' => '']);
        }    
        $formularioRequest = $request->except('_token', 'id', 'id_sexo', 'id_prevision', 'id_clasificacion_fonasa', 'id_tipo_comite', 'bo_clonar', 'nr_hospital_origen', 'fc_presentacion', 'tx_rut', 'tx_nombre', 'tx_apellido_paterno', 'tx_apellido_materno', 'fc_nacimiento', 'tx_mail', 'tx_telefono', 'nr_ficha', 'tx_paciente_nuevo', 'comorbilidades', 'archivo', 'id_cie10');
        $formularioRequest['nr_comite'] = $comite->nr_comite;
        $formulario = FormularioInfantil::updateOrCreate([
            'nr_comite' => $comite->nr_comite,
        ], $formularioRequest);
        if($comite){
            return redirect('/comiteInfantil?resuelto=0')->with('message', "Se ha guardado exitosamente");
        }else{
            return redirect('/comiteInfantil?resuelto=0')->with('error', "No se ha guardado");
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ComiteInfantil  $comiteInfantil
     * @return \Illuminate\Http\Response
     */
    public function show(ComiteInfantil $comiteInfantil)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ComiteInfantil  $comiteInfantil
     * @return \Illuminate\Http\Response
     */
    public function edit(ComiteInfantil $comiteInfantil)
    {
        $comite = $comiteInfantil;
        $formulario = $comite->formulario;
        $establecimientos = Parametros::where('tx_tipo', 'Establecimiento')->get();
        return view('comite.infantil.register', compact('comite', 'formulario', 'establecimientos'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ComiteInfantil  $comiteInfantil
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ComiteInfantil $comiteInfantil)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ComiteInfantil  $comiteInfantil
     * @return \Illuminate\Http\Response
     */
    public function destroy(ComiteInfantil $comiteInfantil)
    {
        //
    }

    public function pdfComiteInfantil(Request $request)
    {
        $comite = ComiteInfantil::with('paciente', 'medico', 'formulario', 'resolucion')->find($request->id);
     
        $pdf = \PDF::loadView('comite.infantil.pdf', compact('comite'));
     
        return $pdf->stream('archivo.pdf');
    }

    public function clonarInfantil(Request $request)
    {
        $comite = ComiteInfantil::find($request->id);
        // dd($comite);
        $formulario = $comite->formulario;
        $establecimientos = Parametros::where('tx_tipo', 'Establecimiento')->get();
        $bo_clonar = 1;
        return view('comite.infantil.register', compact('comite', 'formulario', 'establecimientos', 'bo_clonar'));
    }

    public function excelInfantil(Request $request)
    {
        if (isset($request->medicos)) {
            $medicos = User::whereIn('id', $request->medicos)->pluck('login')->toArray();
        }else{
            $medicos = null;
        }
        $comites = ComiteInfantil::with('paciente', 'medico', 'formulario') 
                ->when($request->has('medicos') && !is_null($request->medicos), function ($collection) use ($medicos){
                    $collection->whereIn('id_usuario', $medicos);
                })           
                ->when($request->has('fc_inicio') && !is_null($request->fc_inicio), function ($collection) use ($request){
                    return $collection->where('fc_presentacion', '>=', date("Y-m-d 00:00:00", strtotime($request->fc_inicio)))
                            ->where('fc_presentacion', '<=', date("Y-m-d 00:00:00", strtotime($request->fc_termino)));
                })
                ->when($request->resuelto == 0, function ($collection) use ($request){
                    $collection->doesnthave('resolucion');
                })
                ->when($request->resuelto == 1, function ($collection) use ($request){
                    $collection->whereHas('resolucion');
                })    
                ->orderBy('nr_comite', 'DESC')
                ->get();
        return Excel::download(new ComiteInfantilExport($comites), 'comiteInfantil'.$request->fc_inicio.'&'.$request->fc_termino.'.xlsx');
    }
}
