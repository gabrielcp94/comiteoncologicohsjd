<?php

namespace App\Http\Controllers;

use App\ResolucionInfantil;
use App\User;
use App\ComiteInfantil;
use App\IntegrantesInfantil;
use App\SolicitudPabellon;
use Illuminate\Http\Request;
use App\Http\Controllers\Helpers\InformesBiopsia;

class ResolucionInfantilController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request, InformesBiopsia $informesBiopsia)
    {
        $comite = ComiteInfantil::with('paciente', 'formulario')->find($request->id);
        $rut = $comite->paciente->tx_rut;
        $medicos = User::orderBy('name')->get();
        $integrantes = IntegrantesInfantil::orderBy('fc_lista', 'ASC')->get();
        $url = "http://10.4.237.27/epicrisis-hsjd/api/epicrisis/get-all?token=5b7dbd9cf2d88&rut=$rut";
        $data = file_get_contents($url);
        $search  = array('\n\r', '\n', '\r');
        $data = str_replace($search,'',$data);
        $objson = json_decode($data, true);
        $arr_epicrises = $objson['epicrises'];
        $rutDV = explode("-",$rut);
        list('informes' => $informes, 'cuenta_biopsia' => $cuenta_biopsia,
        'resultado_consulta' => $resultado_consulta) = $informesBiopsia->fetch($rutDV[0], $rutDV[1]);
        $solicitudesPabellon = SolicitudPabellon::with('protocolo')->whereHas('protocolo')->where('gl_rut', $rut)->get();
        return view('comite.infantil.resolucion', compact('request', 'comite', 'medicos', 'integrantes', 'arr_epicrises', 'informes', 'solicitudesPabellon'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $comite = ComiteInfantil::find($request->id);
        $resolucionRequest = [
            'nr_comite' => $comite->nr_comite,
            'tx_tratamiento' => $request->tx_tratamiento,
            'fc_resolucion' => $comite->fc_presentacion,
            'tx_estado' => $request->tx_estado,
            'id_integrantes' => $request->id_integrantes_fecha,
        ];
        $resolucion = ResolucionInfantil::updateOrCreate(['nr_comite' => $request->id], $resolucionRequest);
        if($resolucion){
            return redirect(session('url'))->with('message', "Se ha guardado exitosamente");
        }else{
            return redirect('/comiteInfantil?resuelto=0')->with('error', "No se ha guardado");
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ResolucionInfantil  $resolucionInfantil
     * @return \Illuminate\Http\Response
     */
    public function show(ResolucionInfantil $resolucionInfantil)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ResolucionInfantil  $resolucionInfantil
     * @return \Illuminate\Http\Response
     */
    public function edit(ResolucionInfantil $resolucionInfantil)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ResolucionInfantil  $resolucionInfantil
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ResolucionInfantil $resolucionInfantil)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ResolucionInfantil  $resolucionInfantil
     * @return \Illuminate\Http\Response
     */
    public function destroy(ResolucionInfantil $resolucionInfantil)
    {
        //
    }
}
