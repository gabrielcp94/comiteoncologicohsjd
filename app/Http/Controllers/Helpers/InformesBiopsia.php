<?php

namespace App\Http\Controllers\Helpers;

use nusoap_client;
use Exception;

class InformesBiopsia
{
    public function fetch(String $rut, String $dv)
    {
        $informes = [];
        $objSOAP = "";
        $url = "http://10.6.1.236/pathient/ws/wsnPath.php?wsdl";
        $type = "";
        $proxyhost = "";
        $proxyport = "";
        $proxyusername = "";
        $proxypassword = "";
        $clientError = "";

        $objSOAP = new nusoap_client($url, $type, $proxyhost, $proxyport, $proxyusername, $proxypassword);
        $clientError = $objSOAP->getError();

        try {
            $resultado = $objSOAP->call('consultaInforme', [ "rut" => $rut, "dv" => $dv ], '', '', false, true);         
                if ($objSOAP->fault) {
                    $resulto = $resultado;
                } else {
                    $error = $objSOAP->getError();
                    if ($error) {
                        $resulto = $error;
                    } else {
                        $resulto = $resultado;
                    }
                }

                $xml = simplexml_load_string(utf8_encode($resultado));

                if(isset($xml->informe)){

                    foreach ($xml->informe as $informe) {
                        $informes[] = $informe;
                    }

                    $cuenta_biopsia = count($xml->xpath('//informe'));
                    $resultado_consulta = $xml->resultado;
                }
        } catch(Exception $e) {
            $resulto = ( $e->getMessage());
        }
        
        return [
            "informes" => $informes,
            "resultado_consulta" => $resultado_consulta,
            "cuenta_biopsia" => $cuenta_biopsia
        ];
    }
}
