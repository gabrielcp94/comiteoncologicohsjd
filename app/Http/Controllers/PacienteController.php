<?php

namespace App\Http\Controllers;

use App\Paciente;
use App\ComiteFormularioAdulto;
use App\ComiteInfantil;
use App\Comite;
use App\Http\Controllers\Helpers\InformesBiopsia;
use Illuminate\Http\Request;
use nusoap_client;

class PacienteController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Paciente  $paciente
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, InformesBiopsia $informesBiopsia)
    {
        $rut = $request->input('rut');
        $rut = str_replace(".","",$rut);
        $rutDV = explode("-",$rut);
        $paciente = Paciente::where('nr_run', $rutDV[0])->where('tx_digito_verificador', $rutDV[1])->first();
        if(!isset($paciente)){
            $paciente = ComiteFormularioAdulto::where('tx_rut', $rut)->orderBy('fc_ingreso', 'DESC')->first();
            $comites = null;
        }else{
            $comites = Comite::where('id_paciente', $paciente->id)->get();
            $comitesInfantil = ComiteInfantil::with('resolucion')->where('id_paciente_general', $paciente->id)->get();
        }
        if(!isset($paciente)){
            return redirect('home')->with('error', "Paciente no registrado");
        }
        $comitesAdulto = ComiteFormularioAdulto::with('resolucion')->where('tx_rut', $rut)->get();
        $url = "http://10.4.237.27/epicrisis-hsjd/api/epicrisis/get-all?token=5b7dbd9cf2d88&rut=$rut";
        $data = file_get_contents($url);
        $search  = array('\n\r', '\n', '\r');
        $data = str_replace($search,'',$data);
        $objson = json_decode($data, true);
        $arr_epicrises = $objson['epicrises'];
        list('informes' => $informes, 'cuenta_biopsia' => $cuenta_biopsia,
            'resultado_consulta' => $resultado_consulta) = $informesBiopsia->fetch($rutDV[0], $rutDV[1]);
            
        return view('paciente.show', compact('paciente', 'comitesAdulto', 'comites', 'comitesInfantil', 'arr_epicrises', 'informes'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Paciente  $paciente
     * @return \Illuminate\Http\Response
     */
    public function edit(Paciente $paciente)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Paciente  $paciente
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Paciente $paciente)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Paciente  $paciente
     * @return \Illuminate\Http\Response
     */
    public function destroy(Paciente $paciente)
    {
        //
    }

    public function getProtocolos(Request $request)
    {
        $options = [
            CURLOPT_RETURNTRANSFER => true,     // return web page
            CURLOPT_HEADER => true,     //return headers in addition to content
            CURLOPT_FOLLOWLOCATION => true,     // follow redirects
            CURLOPT_ENCODING => '',       // handle all encodings
            CURLOPT_AUTOREFERER => true,     // set referer on redirect
            CURLOPT_CONNECTTIMEOUT => 120,      // timeout on connect
            CURLOPT_TIMEOUT => 120,      // timeout on response
            CURLOPT_MAXREDIRS => 10,       // stop after 10 redirects
            CURLINFO_HEADER_OUT => true,
            CURLOPT_SSL_VERIFYPEER => false,     // Disabled SSL Cert checks
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_COOKIE => '',
            CURLOPT_POST => 1,
            CURLOPT_POSTFIELDS => 'gl_usuario=oncologico&gl_clave=25fdfdc3955066fbbb47435f3df26256972abfbe'
        ];

        $ch = curl_init("http://10.6.3.43/pabellon/miprotocolo/pdf/{$request->protocolo_id}");
        curl_setopt_array($ch, $options);
        $rough_content = curl_exec($ch);
        $err = curl_errno($ch);
        $errmsg = curl_error($ch);
        $header = curl_getinfo($ch);
        curl_close($ch);

        $header_content = substr($rough_content, 0, $header['header_size']);
        $body_content = trim(str_replace($header_content, '', $rough_content));
        $pattern = '#Set-Cookie:\\s+(?<cookie>[^=]+=[^;]+)#m';
        preg_match_all($pattern, $header_content, $matches);
        $cookiesOut = implode('; ', $matches['cookie']);

        $header['errno'] = $err;
        $header['errmsg'] = $errmsg;
        $header['headers'] = $header_content;
        $header['content'] = $body_content;
        $header['cookies'] = $cookiesOut;

        header('Content-type: application/pdf');
        echo $header['content'];
    }

    public function getBiopsia(Request $request)
    {
        $array_rut = explode("-",$request->rut);
        $rut_sd = $array_rut[0];
        $rut_dv = $array_rut[1];
            $objSOAP;
            $url = "http://10.6.1.236/pathient/ws/wsnPath.php?wsdl";
            $type = "";
            $proxyhost = "";
            $proxyport = "";
            $proxyusername = "";
            $proxypassword = "";
            $clientError;
            $objSOAP = new nusoap_client($url, $type, $proxyhost, $proxyport, $proxyusername, $proxypassword);
            $clientError = $objSOAP->getError();
            $param = array(
                'rut' => $rut_sd,
                'dv' => $rut_dv
            );
        try{
            $resultado = $objSOAP->call('consultaInforme', $param, '', '', false, true);		
            if ($objSOAP->fault) {
                    $resulto = $resultado;
            } else {
                    $error = $objSOAP->getError();
                    if ($error) {
                        $resulto = $error;
                    } else {
                        $resulto = $resultado;
                    }
            }
        }catch(Exception $e){
            $resulto = ( $e->getMessage());
        }
            $xml = simplexml_load_string(utf8_encode($resultado));
        foreach($xml->informe as $informe){
                $arr_pdf[] = $informe->informepdf;	
            }
        for($i = 0; $i < count($arr_pdf); $i++){
            if($i == $request->fila ){
                $pdf_codificado = $arr_pdf[$i];
            }
        }
            $decoded = base64_decode($pdf_codificado);
            header('Content-Type: application/pdf');
            echo $decoded;
    }
}