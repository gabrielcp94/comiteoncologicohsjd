<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromView;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;

class ComiteExport implements FromView, ShouldAutoSize
{
    use Exportable;

    protected $comite;

    public function view(): View
    {
        return view('comite.excel', [
            'comites' => $this->comite
        ]);
    }

    public function __construct($comite = null)
    {
        $this->comite = $comite;
    }
}
