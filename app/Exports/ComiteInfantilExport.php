<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromView;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;

class ComiteInfantilExport implements FromView, ShouldAutoSize
{
    use Exportable;

    protected $comiteInfantil;

    public function view(): View
    {
        return view('comite.infantil.excel', [
            'comites' => $this->comiteInfantil
        ]);
    }

    public function __construct($comiteInfantil = null)
    {
        $this->comiteInfantil = $comiteInfantil;
    }
}
