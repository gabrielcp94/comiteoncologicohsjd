<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromView;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;

class ComiteAdultoExport implements FromView, ShouldAutoSize
{
    use Exportable;

    protected $comiteAdulto;

    public function view(): View
    {
        return view('comite.adulto.excel', [
            'comites' => $this->comiteAdulto
        ]);
    }

    public function __construct($comiteAdulto = null)
    {
        $this->comiteAdulto = $comiteAdulto;
    }
}
