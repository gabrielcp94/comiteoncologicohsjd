<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use Notifiable;

    protected $table = 'sec_users';
    protected $guarded = [];
    public $timestamps = false;

    public static function boot()
    {
        parent::boot();

        static::creating(function($user)
        {
            $password = substr($user->login, 0, 4);
            $user->password = $password;
            $user->pswd = $password;
        });
          
        static::created(function($user)
        {
            $password = substr($user->login, 0, 4);
            $user->password = $password;
            $user->pswd = $password;
        });
    }

    public function setPswdAttribute($value){
        $this->attributes['pswd'] = sha1(intval($value));
    }

    public function setPasswordAttribute($value){
        $this->attributes['password'] = bcrypt(intval($value));
    }

    protected $appends = ['tx_nombre_minuscula', 'tx_rut'];

    public function getTxNombreMinusculaAttribute()
    {
        return ucwords(mb_strtolower("{$this->name}"));
    }

    public function getTxRutAttribute()
    {
        return "{$this->login}";
    }

    public function especialidad()
    {
		return $this->belongsTo('App\Parametros', 'tx_especialidad');
    }

    public function establecimiento()
    {
		return $this->belongsTo('App\Parametros', 'tx_establecimiento');
    }

    public function grupo()
    {
		return $this->belongsTo('App\UserGrupo', 'login', 'login');
    }
}
