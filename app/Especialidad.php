<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Especialidad extends Model
{
    protected $table = 'com_especialidad';

    protected $appends = ['tx_nombre_minuscula'];

    public function getTxNombreMinusculaAttribute()
    {
        return ucwords(mb_strtolower("{$this->tx_nombre}"));
    }
}
