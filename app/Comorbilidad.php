<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Comorbilidad extends Model
{
    protected $table = 'com_comorbilidad';
}
