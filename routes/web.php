<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect('/home');
});

Auth::routes();

Route::get('/loginPortal', 'Auth\LoginController@loginPortal')->name('loginPortal');

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/portal', 'HomeController@portal');

Route::get('/getDatosRut', 'GetController@getDatosRut');
Route::get('/getDiagnostico', 'GetController@getDiagnostico');

Route::resource('/user', 'UserController');
Route::get('/resetPass/{id}', 'UserController@resetPass');
Route::get('/miUser', 'UserController@miUser');

Route::resource('/paciente', 'PacienteController');
Route::get('getProtocolos', 'PacienteController@getProtocolos');
Route::get('getBiopsia', 'PacienteController@getBiopsia');

Route::resource('/comite', 'ComiteController');
Route::get('/pdfComite', 'ComiteController@pdfComite');
Route::get('/clonar', 'ComiteController@clonar');
Route::get('/excelComite', 'ComiteController@excelComite');

Route::resource('/resolucion', 'ResolucionController');

Route::resource('/comiteAdulto', 'ComiteAdultoController');
Route::get('/pdfComiteAdulto', 'ComiteAdultoController@pdfComiteAdulto');
Route::get('/clonarAdulto', 'ComiteAdultoController@clonarAdulto');
Route::get('/excelAdulto', 'ComiteAdultoController@excelAdulto');
Route::get('/estadisticasAdulto', 'ComiteAdultoController@estadisticasAdulto');

Route::resource('/resolucionAdulto', 'ResolucionAdultoController');

Route::resource('/integrantesAdulto', 'IntegrantesAdultoController');
Route::get('/clonarIntegrantesAdulto', 'IntegrantesAdultoController@clonarIntegrantesAdulto');

Route::resource('/comiteInfantil', 'ComiteInfantilController');
Route::get('/pdfComiteInfantil', 'ComiteInfantilController@pdfComiteInfantil');
Route::get('/clonarInfantil', 'ComiteInfantilController@clonarInfantil');
Route::get('/excelInfantil', 'ComiteInfantilController@excelInfantil');

Route::resource('/resolucionInfantil', 'ResolucionInfantilController');

Route::resource('/integrantesInfantil', 'IntegrantesInfantilController');
Route::get('/clonarIntegrantesInfantil', 'IntegrantesInfantilController@clonarIntegrantesInfantil');

Route::get('storage/{archivo}', function ($archivo) {
    $public_path = public_path();
    $url = $public_path.'/storage/'.$archivo;
    if (Storage::exists($archivo))
    {
        return response()->download($url);
    }
    abort(404);
});