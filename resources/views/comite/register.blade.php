@extends('adminlte::page')

@section('title', 'Ingresar Paciente')

@section('content')
    <div class="card card-info">
        <div class="card-header">
            <h3 class="card-title">Ingresar Paciente {{$tipoComite->tx_nombre_mayuscula}}</h3>
        </div>
        <form role="form" class="form-horizontal" id="form" method="POST" onSubmit="return validate();" action="{{action('ComiteController@store')}}" enctype="multipart/form-data">
			{{ csrf_field() }}
			<input type="hidden" id="id" name="id" value="{{isset($comite) && !isset($bo_clonar) ? $comite->id : ''}}"/>
			<input type="hidden" id="id_sexo" name="id_sexo" />
			<input type="hidden" id="id_prevision" name="id_prevision" />
			<input type="hidden" id="id_clasificacion_fonasa" name="id_clasificacion_fonasa" />
			<input type="hidden" id="id_tipo_comite" name="id_tipo_comite" value="{{$tipoComite->id}}"/>
			<input type="hidden" id="bo_clonar" name="bo_clonar" value="{{isset($bo_clonar) ? '1' : '0'}}"/>
            <div class="card-body">
				<div class="form-group">
					<div class="row">
						<div class="col-sm-3">
							<label for="tx_medico_tratante">Medico Tratante:</label>
							<input class="form-control" value="{{Auth::user()->name}}" readonly>
						</div>
						<div class="col-sm-3">
							<label for="especialidad">Especialidad:</label>
							<input class="form-control" value="{{Auth::user()->especialidad->tx_descripcion}}" readonly>
						</div>
						<div class="col-sm-3">
							<label for="id_hospital_origen">Hosp. Origen de Atención:<span style="color:#FF0000";>*</span></label>
							<select class="select2 select2-hidden-accessible" id="id_hospital_origen" name="id_hospital_origen" data-placeholder="Seleccione establecimiento" style="width: 100%;" data-select2-id="7" tabindex="-1" aria-hidden="true" required>
								<option value=""></option>
								@foreach ($establecimientos as $establecimiento)
									<option value={{$establecimiento->id}} {{Auth::user()->establecimiento->id == $establecimiento->id ? "selected" : ""}}>{{$establecimiento->tx_nombre}}</option>
								@endforeach
							</select>
						</div>
						<div class="col-sm-3">
							<label for="fc_presentacion">Fecha de Presentación:<span style="color:#FF0000";>*</span></label>
							<div class="input-group">
								<div class="input-group-prepend">
									<span class="input-group-text"><i class="far fa-calendar-alt"></i></span>
								</div>
							<input type="date" class="form-control" id="fc_presentacion" name="fc_presentacion" min="{{!isset($comite->fc_presentacion) ? date("Y-m-d") : ''}}" value="{{isset($comite) ? $comite->fc_presentacion : ''}}" required>
							</div>						
						</div>
					</div>
                </div>
				<hr>
				<br>
                <h4>Datos Paciente</h4>
				<div class="form-group">
					<div class="row">
						<div class="col-sm-3">
							<label for="tx_rut">Rut:<span style="color:#FF0000";>*</span></label>
							<input class="form-control" id="tx_rut" name="tx_rut" onblur="getPerson($(this).val());" value="{{isset($comite) ? $comite->paciente->tx_rut : ''}}" required>
						</div>
						<div class="col-sm-3">
							<label for="tx_nombre">Nombre:<span style="color:#FF0000";>*</span></label>
							<input class="form-control" id="tx_nombre" name="tx_nombre" value="{{isset($comite) ? $comite->paciente->tx_nombre : ''}}" required>
						</div>
						<div class="col-sm-3">
							<label for="tx_apellido_paterno">Apellido Paterno:<span style="color:#FF0000";>*</span></label>
							<input class="form-control" id="tx_apellido_paterno" name="tx_apellido_paterno" value="{{isset($comite) ? $comite->paciente->tx_apellido_paterno : ''}}" required>
                        </div>
                        <div class="col-sm-3">
							<label for="tx_apellido_materno">Apellido Materno:</label>
							<input class="form-control" id="tx_apellido_materno" name="tx_apellido_materno" value="{{isset($comite) ? $comite->paciente->tx_apellido_materno : ''}}">
						</div>
					</div>
				</div>
				<div class="form-group">
					<div class="row">
						<div class="col-sm-3">
							<label for="fc_nacimiento">Fecha de Nacimiento:<span style="color:#FF0000";>*</span></label>
							<div class="input-group">
								<div class="input-group-prepend">
									<span class="input-group-text"><i class="far fa-calendar-alt"></i></span>
								</div>
								<input type="date" class="form-control" id="fc_nacimiento" name="fc_nacimiento" disabled value="{{isset($comite) ? $comite->paciente->fc_nacimiento : ''}}" required>
							</div>						
						</div>
						<div class="col-sm-3">
							<label for="tx_mail">Mail:</label>
							<input class="form-control" type="email" id="tx_mail" name="tx_mail" value="{{isset($comite) ? $comite->paciente->tx_mail : ''}}">
						</div>
						<div class="col-sm-2">
							<label for="tx_telefono">Teléfono: </label>
							<input class="form-control" type="number" maxlength="9" oninput="if(this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);" id="tx_telefono" name="tx_telefono" value="{{isset($comite->paciente->tx_telefono) ? $comite->paciente->tx_telefono : ''}}">
						</div>
						<div class="col-sm-2">
							<label for="nr_ficha">Ficha</label>
							<input class="form-control" type="number" maxlength="10" oninput="if(this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);" id="nr_ficha" name="nr_ficha" value="{{isset($comite->paciente->nr_ficha) ? $comite->paciente->nr_ficha : ''}}">
                        </div>
						<div class="col-sm-2">
							<div class="custom-control custom-checkbox">
								<input class="custom-control-input" type="checkbox" id="tx_paciente_nuevo" name="tx_paciente_nuevo" value="SI" {{isset($comite->tx_paciente_nuevo) && ($comite->tx_paciente_nuevo = 'Si') ? 'checked' : ''}}>
								<label for="tx_paciente_nuevo" class="custom-control-label">Paciente Nuevo</label>
							</div>
						</div>
					</div>
				</div>
				@include('comite.formulario.'.$tipoComite->tx_nombre)
			</div>
		  	<div class="card-footer text-right">
				<button type="submit" class="btn btn-success" onclick="guardar()">Guardar</button>
		  	</div>
        </form>
    </div>
@stop

@section('js')
<script>
	$('.select2').select2();
	var url_diagnosticos = @json(url('getDiagnostico'));

	function validate() {
		var resultado_validacion=true;
		$("#editor_error").html("");
		var editor_val = CKEDITOR.instances.tx_plan.document.getBody().getChild(0).getText().trim();
		if(!(editor_val.length>0)){
			alert('Ingrese Plan');
			resultado_validacion=false;
		}       

		return resultado_validacion;
	}

	function guardar(){
		$("#fc_nacimiento").css('display','').attr('disabled', false);
		@if(request()->adulto == 1)
			$("#bo_antecedentes_familiares").css('display','').attr('required', true);
			$("#bo_cancer_previo").css('display','').attr('required', true);
			$("#bo_tabaco").css('display','').attr('required', true);
			$("#bo_oh").css('display','').attr('required', true);
			$("#tx_Cie10").css('display','').attr('required', true);
		@elseif(request()->adulto == 2)
			$("#bo_antecedentes_familiares").css('display','').attr('required', true);
			$("#bo_cancer_previo").css('display','').attr('required', true);
			$("#bo_tabaco").css('display','').attr('required', true);
			$("#bo_oh").css('display','').attr('required', true);
		@endif
	}

	function getPerson(rut){
		@if (!isset($user))
			$.getJSON("{{action('GetController@getDatosRut')}}?rut="+rut,
			function(data){
				if(data.encontrado == true){
					$("#tx_rut").val(data.rut);
					$("#nr_run").val(data.nr_run);
					$("#tx_digito_verificador").val(data.tx_digito_verificador);
					$("#id_sexo").val(data.id_sexo);
					$("#id_prevision").val(data.id_prevision);
					$("#id_clasificacion_fonasa").val(data.id_clasificacion_fonasa);
					$("#tx_nombre").val(data.tx_nombre_solo);
					$("#tx_nombre").css('display','').attr('readonly', true);
					$("#tx_apellido_paterno").val(data.tx_apellido_paterno);
					$("#tx_apellido_paterno").css('display','').attr('readonly', true);
					$("#tx_apellido_materno").val(data.tx_apellido_materno);
					$("#tx_apellido_materno").css('display','').attr('readonly', true);
					$("#fc_nacimiento").val(data.fc_nacimiento);
					$("#fc_nacimiento").css('display','').attr('disabled', true);
					$("#tx_mail").val(data.tx_mail);
					$("#tx_telefono").val(data.tx_telefono);
					$("#nr_ficha").val(data.nr_ficha);
				}else{
					alert("Rut no encontrado");
					$("#tx_rut").val("");
					$("#tx_nombre").val("");
					$("#tx_nombre").css('display','').attr('readonly', false);
					$("#tx_apellido_paterno").val("");
					$("#tx_apellido_paterno").css('display','').attr('readonly', false);
					$("#tx_apellido_materno").val("");
					$("#tx_apellido_materno").css('display','').attr('readonly', false);
				}
			})
		@endif
	};

	$('.select_diagnosticos').select2({
        width: '99%',
        allowClear: true,
        minimumInputLength: 3,
        placeholder: "Seleccione CIE10",
        language: {
            inputTooShort: function() {
                return 'Ingrese 3 o más caracteres para la búsqueda';
            }
        },
        ajax: {
            url: url_diagnosticos,
            dataType: 'json',
            data: function (params) {
                var query = {
                    search: params.term,
                    type: 'public'
                }
                return query;
            }
        }
    })

	@if (isset($comite))
		var comite = @json($comite);
		var comorbilidades = comite.comorbilidades.map(comorbilidades => comorbilidades['id']);
		$("#comorbilidades").val(comorbilidades).trigger('change');

		var id_cie10 = [];
		var newOption1 = new Option('{{$comite->cie10->dec10}}', {{ $comite->cie10->id }}, true, true);
		id_cie10.push(newOption1);
        $('#id_cie10').append(id_cie10).trigger('change');
	@endif
</script>
<script src="{{ asset('/vendors/ckeditor/ckeditor.js') }}"></script>
@stop