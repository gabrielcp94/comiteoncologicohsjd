<!DOCTYPE html>
<html lang="es">
    <head>
        {{-- <link rel="stylesheet" href="{{url('/')}}/css/base.css" />
        <link rel="stylesheet" href="{{url('/')}}/css/bootstrap.min.css" /> --}}
        <style type="text/css">
			table.table-bordered{ font-size: 11px; }
			table.table-bordered td{ padding: 3px;border-color: #909090!important;color: #000000!important;}
			table.table-bordered th{ padding: 3px;border-color: #909090!important;}
			.table tr td{
                font-family:monospace;
				font-size: 11px;
                /* LINE-HEIGHT:1px; */
			}
			.justify{
				text-align: justify;
			}
			.table-responsive{
				page-break-inside: avoid;
			}
            h4{
                text-align: center;
                text-transform: uppercase;
            }
            .texto{
                font-size: 18px;
                font-family:'Courier New', Courier, monospace;
            }
            .texto2{
                font-size: 18px;
                LINE-HEIGHT:0px;
            }
		</style>
        <meta charset="UTF-8">
        <title>Comite Oncologico</title>
    </head>
    <body class="texto">
        <div class="col-xs-12 padding-0" >
            <table class="table" width="100%">
                <tr colspan="12">
                    <td width="10%">
                        <img style="max-width: 60px;" src="vendor/adminlte/dist/img/logo.png" >
                    </td>
                    <td>
                        <p style="margin-bottom:-15px;">Servicio Salud Metropolitano Occidente</p>
                        <p>Hospital San Juan de Dios - CDT</p>
                    </td>    
                    <td width="30%">
                        <img style="max-width: 80px;" src="vendor/adminlte/dist/img/logo-comite.jpg" >
                    </td>               
                </tr>
            </table>
        </div>
        <h4>Fecha de Presentación: {{date('d-m-Y', strtotime($comite->fc_presentacion))}}<br>Comité {{$comite->tipoComite->tx_nombre}} N°: {{$comite->id}}</h4>
        <div class="table-responsive" >
            <table class="table table-bordered " width="100%">
                <tr>
                    <td width="20%" style="background-color: #eeeeee;">
                        <strong>Medico Tratante</strong>
                    </td>
                    <td width="30%" >
                        {{$comite->medico->tx_nombre_minuscula}}
                    </td>
                    <td width="20%" style="background-color: #eeeeee;">
                        <strong>Especialidad</strong>
                    </td>
                    <td width="30%" >
                        {{$comite->especialidad->tx_nombre_minuscula}}
                    </td>
                </tr>
            </table>
        </div>
        <div class="table-responsive" >
            <table class="table table-bordered " width="100%">
                <tr>
                    <td colspan="6" align="center" style="background-color: #999999;" >
                        <strong>Paciente</strong>
                    </td>
                </tr>
                <tr>
                    <td width="10%" style="background-color: #eeeeee;">
                        <strong>Nombre</strong>
                    </td>
                    <td width="35%" >
                        {{$comite->paciente->tx_nombre}}
                    </td>
                    <td width="15%" style="background-color: #eeeeee;">
                        <strong>Apellido Paterno</strong>
                    </td>
                    <td width="15%" >
                        {{$comite->paciente->tx_apellido_paterno}}
                    </td>
                    <td width="10%" style="background-color: #eeeeee;">
                        <strong>Apellido Materno</strong>
                    </td>
                    <td width="15%" >
                        {{$comite->paciente->tx_apellido_materno}}
                    </td>
                </tr>
                <tr>
                    <td style="background-color: #eeeeee;">
                        <strong>F. Nacimiento</strong>
                    </td>
                    <td>
                        {{date('d-m-Y', strtotime($comite->paciente->fc_nacimiento))}} <br> ({{$comite->paciente->edad}})
                    </td>
                    <td style="background-color: #eeeeee;">
                        <strong>Rut</strong>
                    </td>
                    <td>
                        {{$comite->paciente->tx_rut}}
                    </td>
                    <td style="background-color: #eeeeee;">
                        <strong>Ficha</strong>
                    </td>
                    <td>
                        {{$comite->paciente->nr_ficha}}
                    </td>
                </tr>
            </table>
            </div>
            @include('comite.pdf.respiratorio')
    </body>
</html>