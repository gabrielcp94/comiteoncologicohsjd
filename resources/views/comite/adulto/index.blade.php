@extends('adminlte::page')

@section('title', 'Lista de Pacientes Adultos')

@section('content_header')
    @if(session()->has('message'))
        <div class="alert alert-success">
            {{ session()->get('message') }}
        </div>
    @endif
    @if(session()->has('error'))
        <div class="alert alert-danger">
            <i class="fa fa-exclamation-triangle"></i> {{ session()->get('error') }}
        </div>
    @endif
@stop

@section('content')
		{{-- <form class="form-horizontal" id="form" method="POST">
			{{ csrf_field() }} --}}
	<div class="card card-info">
		<div class="card-header">
		<h3 class="card-title">Lista de Pacientes Adulto {{request()->resuelto == 1 ? 'Resueltos' : 'Por Resolver'}}</h3>
		</div>
		<!-- /.card-header -->
		<!-- form start -->
		<div class="card-body">
            <div id="example1_wrapper" class="dataTables_wrapper dt-bootstrap4">
                <div class="row">
                    <div class="col-sm-12">
                        <form class="form-horizontal">
                            <input type="hidden" id="resuelto" name="resuelto" value="{{request()->resuelto}}"/>
                            <div class="form-group row">
                                <div class="col-sm-4">
                                    <label for="fc_inicio">Fecha de Inicio</label>
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text"><i class="far fa-calendar-alt"></i></span>
                                        </div>
                                        <input type="date" class="form-control" id="fc_inicio" name="fc_inicio" value="{{isset(request()->fc_inicio) ? request()->fc_inicio : date("Y-m-d")}}">
                                    </div>	
                                </div>
                                <div class="col-sm-4">
                                    <label for="fc_termino">Fecha de Termino</label>
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text"><i class="far fa-calendar-alt"></i></span>
                                        </div>
                                        <input type="date" class="form-control" id="fc_termino" name="fc_termino" value="{{isset(request()->fc_termino) ? request()->fc_termino : date("Y-m-d")}}">
                                    </div>	
                                </div>
                                <div class="col-sm-4 text-right">
                                    <a href="comiteAdulto?resuelto={{request()->resuelto == 1 ? '0' : '1'}}" class="{{request()->resuelto == 1 ? 'btn btn-warning' : 'btn btn-success'}}">{{request()->resuelto == 1 ? 'Por Resolver' : 'Resuelto'}}</a>
                                    <button type="submit" class="btn btn-info">Filtrar</button>
                                    <button type="submit" onclick="excel()" class="btn btn-success"><i class="fa fa-file-excel" style="color:white"></i></button>
                                    <a href="comiteAdulto/create" title="Nuevo Comite" class="btn btn-success"><i class="fa fa-plus" style="color:white"></i></a>
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-sm-4">
                                    <label for="especialidades">Especialidad:</label>
                                    <select class="select2 select2-hidden-accessible" id="especialidades" name="especialidades[]" multiple="" data-placeholder="Seleccione especialidad(es)" style="width: 100%;" data-select2-id="7" tabindex="-1" aria-hidden="true">
                                        @foreach ($especialidades as $especialidad)
                                            <option value={{$especialidad->id}} >{{$especialidad->tx_nombre_minuscula}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-sm-4">
                                    <label for="medicos">Medico:</label>
                                    <select class="select2 select2-hidden-accessible" id="medicos" name="medicos[]" multiple="" data-placeholder="Seleccione medico(s)" style="width: 100%;" data-select2-id="7" tabindex="-1" aria-hidden="true">
                                        @foreach ($medicos as $medico)
                                            <option value={{$medico->id}} >{{$medico->tx_nombre_minuscula}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </form>
                        <table class="table table-striped table-hover" id="tableUsers">
                            <thead>
                                <th>ID</th>
                                <th>Nombre</th>
                                <th>Rut</th>
                                <th>F. Presentación</th>
                                @if (request()->resuelto == 1)
                                    <th>F. Resolución</th>
                                @endif
                                <th>Médico</th>
                                <th>Especialidad</th>
                                {{-- <th>Estado</th> --}}
                                @if (request()->adulto == 1 && request()->resuelto == 0)
                                    <th>Rev. Imagen</th>
                                    <th>Cateter QT</th>
                                @endif
                                <th><i class="fa fa-cog"></i></th>
                            </thead>
                            <tbody>
                                @foreach ($comites as $comite)
                                    <tr role="row" class="odd">
                                        <td>{{$comite->id}}</td>
                                        <td><a href="paciente/1?rut={{$comite->tx_rut}}">{{$comite->tx_nombre_completo}}<a></td>
                                        <td>{{$comite->tx_rut}}</td>
                                        <td>{{date('d-m-Y', strtotime($comite->fc_ingreso))}}</td>
                                        @if (request()->resuelto == 1)
                                            <td>{{isset($comite->resolucion) ? date('d-m-Y', strtotime($comite->resolucion->fc_resolucion)) : ''}}</td>
                                        @endif
                                        <td>{{$comite->tx_medico}}</td>
                                        <td>{{ucwords(mb_strtolower($comite->tx_especialidad))}}</td>
                                        {{-- <td>{{isset($comite->resolucion) ? isset($comite->resolucion->estado) ? $comite->resolucion->estado->tx_nombre_minuscula : $comite->resolucion->tx_estado : 'Indefinido'}}</td> --}}
                                        @if (request()->adulto == 1 && request()->resuelto == 0)
                                            <td>{{isset($comite->bo_revisar_imagen) && $comite->bo_revisar_imagen == 1 ? 'Si' : 'No'}}</td>
                                            <td>{{isset($comite->resolucion) ? $comite->resolucion->tx_cateter_qt : ''}}</td>
                                        @endif
                                        <td>
                                            <form action="{{ route('comiteAdulto.destroy',$comite->id) }}" method="POST">
                                                <div class="btn-group">
                                                    @if (request()->resuelto == 0)
                                                        @if (isset($comite->resolucion))
                                                            <a class="btn btn-success btn-xs" href="resolucionAdulto/create?id={{$comite->id}}" title="Resolver"><i class="fa fa-check" style="color:white"></i></a>
                                                            {{-- <a class="btn btn-success btn-xs" href="resolucion/{{$comite->id}}/edit?adulto={{request()->adulto}}" title="Resolver (editar)"><i class="fa fa-check" style="color:white"></i></a> --}}
                                                        @else
                                                            <a class="btn btn-success btn-xs" href="resolucionAdulto/create?id={{$comite->id}}" title="Resolver"><i class="fa fa-check" style="color:white"></i></a>
                                                        @endif
                                                        <a class="btn btn-warning btn-xs" href="comiteAdulto/{{$comite->id}}/edit" title="Editar"><i class="fa fa-edit" style="color:white"></i></a>
                                                    @endif
                                                    <a class="btn btn-secondary btn-xs" href="pdfComiteAdulto?resuelto={{request()->resuelto}}&id={{$comite->id}}" title="Ver Comite" target="_blank"><i class="fa fa-file-pdf" style="color:white"></i></a>
                                                    @if (isset($comite->tx_archivo) && $comite->tx_archivo != '')
                                                        <a class="btn btn-primary btn-xs" href="http://10.4.237.11/comite_oncologico/_lib/file/doc/{{$comite->tx_archivo}}" title="Archivo" target="_blank"><i class="fa fa-file" style="color:white"></i></a>
                                                    @endif
                                                    <a class="btn btn-info btn-xs" href="clonarAdulto?id={{$comite->id}}" title="Clonar Comite"><i class="fa fa-copy" style="color:white"></i></a>
                                                    @csrf
                                                    @method('DELETE')
                                                    <button onclick="return confirm('¿Esta seguro de eliminar a este Comite?');" type="submit" class="btn btn-danger btn-xs" title="Eliminar Comite"><i class="fa fa-trash" style="color:white"></i></button>
                                                </div>
                                            </form>
										</td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                        {{ $comites->appends(request()->query())->links() }}
                    </div>
                </div>
            </div>
        </div>
        <!-- /.card-body -->
    </div>
@stop

@section('js')
<script>
    function excel(){
        var url = "excelAdulto?"+$( "form" ).serialize();
        window.location.href = url;
        alert('descargando excel...');
    }

    @if(isset(request()->especialidades))
        var user = @json(request()->all());
        var especialidades = user.especialidades;
        $("#especialidades").val(especialidades).trigger('change');
    @endif

    @if(isset(request()->medicos))
        var user = @json(request()->all());
        var medicos = user.medicos;
        $("#medicos").val(medicos).trigger('change');
    @endif

	$(".alert-success").fadeTo(20000, 500).slideUp(500, function(){
        $(".alert-success").slideUp(1000);
    });

    $(".alert-danger").fadeTo(20000, 5000).slideUp(500, function(){
        $(".alert-danger").slideUp(1000);
    });

    $('.select2').select2();
</script>
@stop