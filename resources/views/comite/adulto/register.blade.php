@extends('adminlte::page')

@section('title', 'Ingresar Paciente Adulto')

@section('content')
    <div class="card card-info">
        <div class="card-header">
            <h3 class="card-title">Ingresar Paciente Adulto</h3>
        </div>
        <form role="form" class="form-horizontal" id="form" method="POST" onSubmit="return validate();" action="{{action('ComiteAdultoController@store')}}" enctype="multipart/form-data">
			{{ csrf_field() }}
			<input type="hidden" id="id" name="id" value="{{isset($comite) && !isset($bo_clonar) ? $comite->id : ''}}"/>
			<input type="hidden" id="id_sexo" name="id_sexo"/>
			<input type="hidden" id="bo_clonar" name="bo_clonar" value="{{isset($bo_clonar) ? '1' : '0'}}"/>
            <div class="card-body">
				<div class="form-group">
					<div class="row">
						<div class="col-sm-3">
							<label for="tx_medico_tratante">Medico Tratante:</label>
							<input class="form-control" id="tx_medico_tratante" value="{{Auth::user()->name}}" readonly>
						</div>
						<div class="col-sm-3">
							<label for="especialidad">Especialidad:</label>
							<input class="form-control" id="tx_especialidad" name="tx_especialidad" value="{{Auth::user()->especialidad->tx_descripcion}}" readonly>
						</div>
						<div class="col-sm-3">
							<label for="nr_hospital_origen">Hosp. Origen de Atención:<span style="color:#FF0000";>*</span></label>
							<select class="select2 select2-hidden-accessible" id="nr_hospital_origen" name="nr_hospital_origen" data-placeholder="Seleccione establecimiento" style="width: 100%;" data-select2-id="7" tabindex="-1" aria-hidden="true" required>
								<option value=""></option>
								@foreach ($establecimientos as $establecimiento)
									<option value={{$establecimiento->id}} {{Auth::user()->establecimiento->id == $establecimiento->id ? "selected" : ""}}>{{$establecimiento->tx_nombre_minuscula}}</option>
								@endforeach
							</select>
						</div>
						<div class="col-sm-3">
							<label for="fc_ingreso">Fecha de Presentación:<span style="color:#FF0000";>*</span></label>
							<div class="input-group">
								<div class="input-group-prepend">
									<span class="input-group-text"><i class="far fa-calendar-alt"></i></span>
								</div>
							<input type="date" class="form-control" id="fc_ingreso" name="fc_ingreso" min="{{!isset($comite->fc_ingreso) ? $today : ''}}" value="{{isset($comite) ? $comite->fc_ingreso : ''}}" required>
							</div>						
						</div>
					</div>
                </div>
				<hr>
				<br>
                <h4>Datos Paciente</h4>
				<div class="form-group">
					<div class="row">
						<div class="col-sm-3">
							<label for="tx_rut">Rut:<span style="color:#FF0000";>*</span></label>
							<input class="form-control" id="tx_rut" name="tx_rut" onblur="getPerson($(this).val());" value="{{isset($comite) ? $comite->tx_rut : ''}}" required>
						</div>
						<div class="col-sm-3">
							<label for="tx_nombre">Nombre:<span style="color:#FF0000";>*</span></label>
							<input class="form-control" id="tx_nombre" name="tx_nombre" value="{{isset($comite) ? $comite->tx_nombre : ''}}" required>
						</div>
						<div class="col-sm-3">
							<label for="tx_apellido_paterno">Apellido Paterno:<span style="color:#FF0000";>*</span></label>
							<input class="form-control" id="tx_apellido_paterno" name="tx_apellido_paterno" value="{{isset($comite) ? $comite->tx_apellido_paterno : ''}}" required>
                        </div>
                        <div class="col-sm-3">
							<label for="tx_apellido_materno">Apellido Materno:</label>
							<input class="form-control" id="tx_apellido_materno" name="tx_apellido_materno" value="{{isset($comite) ? $comite->tx_apellido_materno : ''}}">
						</div>
					</div>
				</div>
				<div class="form-group">
					<div class="row">
						<div class="col-sm-3">
							<label for="fc_nacimiento">Fecha de Nacimiento:<span style="color:#FF0000";>*</span></label>
							<div class="input-group">
								<div class="input-group-prepend">
									<span class="input-group-text"><i class="far fa-calendar-alt"></i></span>
								</div>
								<input type="date" class="form-control" id="fc_nacimiento" name="fc_nacimiento" disabled value="{{isset($comite) ? $comite->fc_nacimiento : ''}}" required>
							</div>						
						</div>
						<div class="col-sm-3">
							<label for="tx_mail">Mail:</label>
							<input class="form-control" type="email" id="tx_mail" name="tx_mail" value="{{isset($comite) ? $comite->tx_mail : ''}}" disabled>
						</div>
						<div class="col-sm-2">
							<label for="nr_telefono">Teléfono: </label>
							<input class="form-control" type="number" maxlength="9" oninput="if(this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);" id="nr_telefono" name="nr_telefono" value="{{isset($comite->nr_telefono) ? $comite->nr_telefono : ''}}">
						</div>
						<div class="col-sm-2">
							<label for="nr_ficha">Ficha</label>
							<input class="form-control" type="number" maxlength="10" oninput="if(this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);" id="nr_ficha" name="nr_ficha" value="{{isset($comite->nr_ficha) ? $comite->nr_ficha : ''}}">
                        </div>
						<div class="col-sm-2">
							<div class="custom-control custom-checkbox">
								<input class="custom-control-input" type="checkbox" id="tx_paciente_nuevo" name="tx_paciente_nuevo" value="SI" {{isset($comite->tx_paciente_nuevo) && ($comite->tx_paciente_nuevo = 'Si') ? 'checked' : ''}}>
								<label for="tx_paciente_nuevo" class="custom-control-label">Paciente Nuevo</label>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-12">
					<div class="card">
						<div class="card-header p-2">
						<ul class="nav nav-pills">
							<li class="nav-item"><a class="nav-link active" href="#antecedentesMorbidos" data-toggle="tab">Antecedentes Mórbidos</a></li>
							<li class="nav-item"><a class="nav-link" href="#historiaClinica" data-toggle="tab">Historia Clínica</a></li>
							<li class="nav-item"><a class="nav-link" href="#diagnosticoActual" data-toggle="tab">Diagnóstico Actual</a></li>
							<li class="nav-item"><a class="nav-link" href="#planPropuesto" data-toggle="tab">Plan Propuesto</a></li>
						</ul>
						</div>
						<div class="card-body">
						<div class="tab-content">
							<div class="tab-pane active" id="antecedentesMorbidos">
								<div class="form-group">
									<div class="row">
										<div class="col-sm-3">
											<label for="tx_antecedentes_familiares">Antecedentes Familiares<span style="color:#FF0000";>*</span></label>
											<div class="row">
												<div class="col-sm-3">
													<label>Sí</label>
												</div>
												<div class="col-sm-1">
													<input class="form-check-input" type="radio" id="tx_antecedentes_familiares" name="tx_antecedentes_familiares" value="SI" {{isset($comite->tx_antecedentes_familiares) && $comite->tx_antecedentes_familiares == "SI" ? "checked" : ""}}>
												</div>
												<div class="col-sm-3">
													<label>No</label>
												</div>
												<div class="col-sm-1">
													<input class="form-check-input" type="radio" id="tx_antecedentes_familiares" name="tx_antecedentes_familiares" value="NO" {{isset($comite->tx_antecedentes_familiares) && $comite->tx_antecedentes_familiares == "NO" ? "checked" : ""}}>
												</div>
											</div>
										</div>
										<div class="col-sm-3">
											<label for="tx_cancer_previo">Cancer Previo<span style="color:#FF0000";>*</span></label>
											<div class="row">
												<div class="col-sm-3">
													<label>Sí</label>
												</div>
												<div class="col-sm-1">
													<input class="form-check-input" type="radio" id="tx_cancer_previo" name="tx_cancer_previo" value="SI" {{isset($comite->tx_cancer_previo) && $comite->tx_cancer_previo == 'SI' ? "checked" : ""}}>
												</div>
												<div class="col-sm-3">
													<label>No</label>
												</div>
												<div class="col-sm-1">
													<input class="form-check-input" type="radio" id="tx_cancer_previo" name="tx_cancer_previo" value="NO" {{isset($comite->tx_cancer_previo) && $comite->tx_cancer_previo == 'NO' ? "checked" : ""}}>
												</div>
											</div>
										</div>
										<div class="col-sm-6">
											<label for="tx_tipo_cancer">Tipo Cancer:</label>
											<input class="form-control" id="tx_tipo_cancer" name="tx_tipo_cancer" value="{{isset($comite->tx_tipo_cancer) ? $comite->tx_tipo_cancer : ''}}">
										</div>
									</div>
								</div>
								<div class="form-group">
									<div class="row">
										<div class="col-sm-3">
											<label for="tx_tabaco">Tabaco<span style="color:#FF0000";>*</span></label>
											<div class="row">
												<div class="col-sm-3">
													<label>Sí</label>
												</div>
												<div class="col-sm-1">
													<input class="form-check-input" type="radio" id="tx_tabaco" name="tx_tabaco" value="SI" {{isset($comite->tx_tabaco) && $comite->tx_tabaco == 'SI' ? "checked" : ""}}>
												</div>
												<div class="col-sm-3">
													<label>No</label>
												</div>
												<div class="col-sm-1">
													<input class="form-check-input" type="radio" id="tx_tabaco" name="tx_tabaco" value="NO" {{isset($comite->tx_tabaco) && $comite->tx_tabaco == 'NO' ? "checked" : ""}}>
												</div>
											</div>
										</div>
										<div class="col-sm-3">
											<label for="tx_oh">OH<span style="color:#FF0000";>*</span></label>
											<div class="row">
												<div class="col-sm-3">
													<label>Sí</label>
												</div>
												<div class="col-sm-1">
													<input class="form-check-input" type="radio" id="tx_oh" name="tx_oh" value="SI" {{isset($comite->tx_oh) && $comite->tx_oh == 'SI' ? "checked" : ""}}>
												</div>
												<div class="col-sm-3">
													<label>No</label>
												</div>
												<div class="col-sm-1">
													<input class="form-check-input" type="radio" id="tx_oh" name="tx_oh" value="NO" {{isset($comite->tx_oh) && $comite->tx_oh == 'NO' ? "checked" : ""}}>
												</div>
											</div>
										</div>
										<div class="col-sm-6">
											<label for="tx_otro_antecedente">Otro Antecedente:</label>
											<input class="form-control" id="tx_otro_antecedente" name="tx_otro_antecedente" value="{{isset($comite->tx_otro_antecedente) ? $comite->tx_otro_antecedente : ''}}">
										</div>
									</div>
								</div>
								<div class="form-group">
									<div class="row">
										<div class="col-sm-3">
											<label>Comorbilidad</label>
											<div class="form-group">
												<div class="custom-control custom-checkbox">
													<input class="custom-control-input" type="checkbox" id="cardiopatia" name="cardiopatia" value="CARDIOPATIA" {{isset($comite->tx_comorbilidad) && strpos($comite->tx_comorbilidad, 'CARDIOPATIA') !== false ? 'checked' : ''}}>
													<label for="cardiopatia" class="custom-control-label">Cardiopatia</label>
												</div>
												<div class="custom-control custom-checkbox">
													<input class="custom-control-input" type="checkbox" id="diabetes" name="diabetes" value="DIABETES" {{isset($comite->tx_comorbilidad) && strpos($comite->tx_comorbilidad, 'DIABETES') !== false ? 'checked' : ''}}>
													<label for="diabetes" class="custom-control-label">Diabetes</label>
												</div>
												<div class="custom-control custom-checkbox">
													<input class="custom-control-input" type="checkbox" id="hipertension" name="hipertension" value="HIPERTENSIÓN" {{isset($comite->tx_comorbilidad) && strpos($comite->tx_comorbilidad, 'HIPERTENSIÓN') !== false ? 'checked' : ''}}>
													<label for="hipertension" class="custom-control-label">Hipertension</label>
												</div>
												<div class="custom-control custom-checkbox">
													<input class="custom-control-input" type="checkbox" id="dhc" name="dhc" value="DHC" {{isset($comite->tx_comorbilidad) && strpos($comite->tx_comorbilidad, 'DHC') !== false ? 'checked' : ''}}>
													<label for="dhc" class="custom-control-label">DHC</label>
												</div>
												<div class="custom-control custom-checkbox">
													<input class="custom-control-input" type="checkbox" id="enfermedad_pulmonar" name="enfermedad_pulmonar" value="ENFERMEDAD PULMONAR" {{isset($comite->tx_comorbilidad) && strpos($comite->tx_comorbilidad, 'ENFERMEDAD PULMONAR') !== false ? 'checked' : ''}}>
													<label for="enfermedad_pulmonar" class="custom-control-label">Enfermedad Pulmonar</label>
												</div>
											</div>
										</div>
										<div class="col-sm-3">
											<label for="tx_imagen">Revisar Imagen</label>
											<div class="row">
												<div class="col-sm-3">
													<label>Sí</label>
												</div>
												<div class="col-sm-1">
													<input class="form-check-input" type="radio" id="tx_imagen" name="tx_imagen" value="SI" {{isset($comite->tx_imagen) && $comite->tx_imagen == 'SI' ? "checked" : ""}}>
												</div>
												<div class="col-sm-3">
													<label>No</label>
												</div>
												<div class="col-sm-1">
													<input class="form-check-input" type="radio" id="tx_imagen" name="tx_imagen" value="NO" {{isset($comite->tx_imagen) && $comite->tx_imagen == 'NO' ? "checked" : ""}}>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="tab-pane" id="historiaClinica">
								<div class="form-group">
									<div class="row">
										<div class="col-sm-6">
											<label for="tx_historia_clinica">Historia Clinica</label>
											<textarea class="form-control noresize ckeditor" id="tx_historia_clinica" name="tx_historia_clinica">{{isset($comite->tx_historia_clinica) ? $comite->tx_historia_clinica : ""}}</textarea>
										</div>
										<div class="col-sm-6">
											<label for="tx_resultado_imagen">Resultado Imagen</label>
											<textarea class="form-control noresize ckeditor" id="tx_resultado_imagen" name="tx_resultado_imagen">{{isset($comite->tx_resultado_imagen) ? $comite->tx_resultado_imagen : ""}}</textarea>
										</div>
									</div>
								</div>
								<div class="form-group">
									<div class="row">
										<div class="col-sm-2">
											<label for="nr_cea">CEA</label>
											<div class="col-sm-8">
												<input type="number" step="any" class="form-control" id="nr_cea" name="nr_cea" min="0" value="{{isset($comite->nr_cea) ? $comite->nr_cea : ""}}">	
											</div>				
										</div>
										<div class="col-sm-2">
											<label for="nr_cea_125">CEA 125</label>
											<div class="col-sm-8">
												<input type="number" step="any" class="form-control" id="nr_cea_125" name="nr_cea_125" min="0" value="{{isset($comite->nr_cea_125) ? $comite->nr_cea_125 : ""}}">	
											</div>				
										</div>
										<div class="col-sm-2">
											<label for="nr_hcg">HCG</label>
											<div class="col-sm-8">
												<input type="number" step="any" class="form-control" id="nr_hcg" name="nr_hcg" min="0" value="{{isset($comite->nr_hcg) ? $comite->nr_hcg : ""}}">	
											</div>				
										</div>
										<div class="col-sm-2">
											<label for="nr_cea_19_9">CEA 19-9</label>
											<div class="col-sm-8">
												<input type="number" step="any" class="form-control" id="nr_cea_19_9" name="nr_cea_19_9" min="0" value="{{isset($comite->nr_cea_19_9) ? $comite->nr_cea_19_9 : ""}}">	
											</div>				
										</div>
										<div class="col-sm-2">
											<label for="nr_afp">AFP</label>
											<div class="col-sm-8">
												<input type="number" step="any" class="form-control" id="nr_afp" name="nr_afp" min="0" value="{{isset($comite->nr_afp) ? $comite->nr_afp : ""}}">	
											</div>				
										</div>
										<div class="col-sm-2">
											<label for="nr_psa">PSA</label>
											<div class="col-sm-8">
												<input type="number" step="any" class="form-control" id="nr_psa" name="nr_psa" min="0" value="{{isset($comite->nr_psa) ? $comite->nr_psa : ""}}">	
											</div>				
										</div>
									</div>
								</div>
							</div>
							<div class="tab-pane" id="diagnosticoActual">
								<div class="form-group">
									<div class="row">
										<div class="col-sm-4">
											<label for="tx_diagnostico_actual">Diagnóstico Actual</label>
											<textarea class="form-control noresize ckeditor" id="tx_diagnostico_actual" name="tx_diagnostico_actual">{{isset($comite->tx_diagnostico_actual) ? $comite->tx_diagnostico_actual : ""}}</textarea>
										</div>
										<div class="col-sm-4">
											<label for="tx_biopsia_pre_Op">Biopsia Pre Operatoria</label>
											<textarea class="form-control noresize ckeditor" id="tx_biopsia_pre_Op" name="tx_biopsia_pre_Op">{{isset($comite->tx_biopsia_pre_Op) ? $comite->tx_biopsia_pre_Op : ""}}</textarea>
										</div>
										<div class="col-sm-4">
											<label for="tx_biopsia__Op">Biopsia Operatoria</label>
											<textarea class="form-control noresize ckeditor" id="tx_biopsia__Op" name="tx_biopsia__Op">{{isset($comite->tx_biopsia__Op) ? $comite->tx_biopsia__Op : ""}}</textarea>
										</div>
									</div>
								</div>
								<div class="form-group">
									<div class="row">
										<div class="col-sm-2">
											<label for="nr_ln_total">Linf. Resecados</label>
											<input type="number" class="form-control" id="nr_ln_total" name="nr_ln_total" min="0" value="{{isset($comite->nr_ln_total) ? $comite->nr_ln_total : ""}}">	
										</div>
										<div class="col-sm-2">
											<label for="nr_ln_pos">Linfonodos (+)</label>
											<input type="number" class="form-control" id="nr_ln_pos" name="nr_ln_pos" min="0" value="{{isset($comite->nr_ln_pos) ? $comite->nr_ln_pos : ""}}">	
										</div>
										<div class="col-sm-2">
											<label for="tx_t">T</label>
											<input class="form-control" maxlength="11" id="tx_t" name="tx_t" value="{{isset($comite->tx_t) ? $comite->tx_t : ''}}">	
										</div>
										<div class="col-sm-2">
											<label for="tx_n">N</label>
											<input class="form-control" maxlength="11" id="tx_n" name="tx_n" value="{{isset($comite->tx_n) ? $comite->tx_n : ''}}">	
										</div>
										<div class="col-sm-2">
											<label for="tx_m">M</label>
											<input class="form-control" maxlength="11" id="tx_m" name="tx_m" value="{{isset($comite->tx_m) ? $comite->tx_m : ''}}">
										</div>
										<div class="col-sm-2">
											<label for="tx_etapa">Etapa</label>
											<input class="form-control" maxlength="50" id="tx_etapa" name="tx_etapa" value="{{isset($comite->tx_etapa) ? $comite->tx_etapa : ''}}">
										</div>
									</div>
								</div>
								<div class="form-group">
									<div class="row">
										<div class="col-sm-2">
											<label for="tx_el">Estrógeno</label>
											<select class="select2 select2-hidden-accessible" id="tx_el" name="tx_el" style="width: 100%;" data-select2-id="7" tabindex="-1" aria-hidden="true">
												<option value=""></option>
												<option value="Positivo" {{isset($comite->tx_el) && $comite->tx_el == "Positivo" ? "selected" : ""}}>Positivo</option>
												<option value="Negativo" {{isset($comite->tx_el) && $comite->tx_el == "Negativo" ? "selected" : ""}}>Negativo</option>
											</select>
										</div>
										<div class="col-sm-2">
											<label for="nr_el_porcentaje">Estrógeno %</label>
											<input type="number" step="any" class="form-control" id="nr_el_porcentaje" name="nr_el_porcentaje" min="0" value="{{isset($comite->nr_el_porcentaje) ? $comite->nr_el_porcentaje : ""}}">	
										</div>
										<div class="col-sm-2">
											<label for="tx_el_valor">Est. Incremento</label>
											<select class="select2 select2-hidden-accessible" id="tx_el_valor" name="tx_el_valor" style="width: 100%;" data-select2-id="7" tabindex="-1" aria-hidden="true">
												<option value=""></option>
												<option value="+" {{isset($comite->tx_el_valor) && $comite->tx_el_valor == "+" ? "selected" : ""}}>+</option>
												<option value="++" {{isset($comite->tx_el_valor) && $comite->tx_el_valor == "++" ? "selected" : ""}}>++</option>
												<option value="+++" {{isset($comite->tx_el_valor) && $comite->tx_el_valor == "+++" ? "selected" : ""}}>+++</option>
											</select>
										</div>
										<div class="col-sm-2">
											<label for="tx_progesterona">Progesterona</label>
											<select class="select2 select2-hidden-accessible" id="tx_progesterona" name="tx_progesterona" style="width: 100%;" data-select2-id="7" tabindex="-1" aria-hidden="true">
												<option value=""></option>
												<option value="Positivo" {{isset($comite->tx_progesterona) && $comite->tx_progesterona == "Positivo" ? "selected" : ""}}>Positivo</option>
												<option value="Negativo" {{isset($comite->tx_progesterona) && $comite->tx_progesterona == "Negativo" ? "selected" : ""}}>Negativo</option>
											</select>
										</div>
										<div class="col-sm-2">
											<label for="nr_progesterona_porcentaje">Progesterona %</label>
											<input type="number" step="any" class="form-control" id="nr_progesterona_porcentaje" name="nr_progesterona_porcentaje" min="0" value="{{isset($comite->nr_progesterona_porcentaje) ? $comite->nr_progesterona_porcentaje : ""}}">	
										</div>
										<div class="col-sm-2">
											<label for="tx_progesterona_valor">Pro. Incremento</label>
											<select class="select2 select2-hidden-accessible" id="tx_progesterona_valor" name="tx_progesterona_valor" style="width: 100%;" data-select2-id="7" tabindex="-1" aria-hidden="true">
												<option value=""></option>
												<option value="+" {{isset($comite->tx_progesterona_valor) && $comite->tx_progesterona_valor == "+" ? "selected" : ""}}>+</option>
												<option value="++" {{isset($comite->tx_progesterona_valor) && $comite->tx_progesterona_valor == "++" ? "selected" : ""}}>++</option>
												<option value="+++" {{isset($comite->tx_progesterona_valor) && $comite->tx_progesterona_valor == "+++" ? "selected" : ""}}>+++</option>
											</select>
										</div>
									</div>
								</div>
								<div class="form-group">
									<div class="row">
										<div class="col-sm-2">
											<label for="tx_ger2">HER 2</label>
											<select class="select2 select2-hidden-accessible" id="tx_ger2" name="tx_ger2" style="width: 100%;" data-select2-id="7" tabindex="-1" aria-hidden="true">
												<option value=""></option>
												<option value="Positivo, Score 3" {{isset($comite->tx_ger2) && $comite->tx_ger2 == "Positivo, Score 3" ? "selected" : ""}}>Positivo, Score 3</option>
												<option value="Equivoco, Score 2" {{isset($comite->tx_ger2) && $comite->tx_ger2 == "Equivoco, Score 2" ? "selected" : ""}}>Equivoco, Score 2</option>
												<option value="Negativo Score 0" {{isset($comite->tx_ger2) && $comite->tx_ger2 == "Negativo Score 0" ? "selected" : ""}}>Negativo Score 0</option>
												<option value="Negativo Score 1" {{isset($comite->tx_ger2) && $comite->tx_ger2 == "Negativo Score 1" ? "selected" : ""}}>Negativo Score 1</option>
											</select>	
										</div>
										<div class="col-sm-2">
											<label for="tx_fish">FISH</label>
											<select class="select2 select2-hidden-accessible" id="tx_fish" name="tx_fish" style="width: 100%;" data-select2-id="7" tabindex="-1" aria-hidden="true">
												<option value=""></option>
												<option value="+" {{isset($comite->tx_fish) && $comite->tx_fish == "+" ? "selected" : ""}}>+</option>
												<option value="-" {{isset($comite->tx_fish) && $comite->tx_fish == "-" ? "selected" : ""}}>-</option>
												<option value="Equivoco" {{isset($comite->tx_fish) && $comite->tx_fish == "Equivoco" ? "selected" : ""}}>Equivoco</option>
											</select>				
										</div>
										<div class="col-sm-2">
											<label for="nr_ki67">Ki 67</label>
											<input type="number" step="any" class="form-control" id="nr_ki67" name="nr_ki67" min="0" value="{{isset($comite->nr_ki67) ? $comite->nr_ki67 : ""}}">	
										</div>
										<div class="col-sm-3">
											<label for="tx_ecog">ECOG</label>
											<select class="select2 select2-hidden-accessible" id="tx_ecog" name="tx_ecog" style="width: 100%;" data-select2-id="7" tabindex="-1" aria-hidden="true">
												<option value=""></option>
												<option value="0" {{isset($comite->tx_ecog) && $comite->tx_ecog == "0" ? "selected" : ""}}>0 - Totalmente asintomático y es capaz de realizar un trabajo y actividades normales de la vida diaria.</option>
												<option value="1" {{isset($comite->tx_ecog) && $comite->tx_ecog == "1" ? "selected" : ""}}>1 - Presenta síntomas que le impiden realizar trabajos pesados, aunque se desempeña normalmente en sus actividades cotidianas y en trabajos ligeros. El paciente sólo permanece en la cama durante las horas de sueño nocturno.</option>
												<option value="2" {{isset($comite->tx_ecog) && $comite->tx_ecog == "2" ? "selected" : ""}}>2 - El paciente no es capaz de desempeñar ningún trabajo, se encuentra con síntomas que le obligan a permanecer en la cama durante varias horas al día, además de las de la noche, pero que no superan el 50% del día. El individuo satisface la mayoría de sus necesidades personales solo.</option>
												<option value="3" {{isset($comite->tx_ecog) && $comite->tx_ecog == "3" ? "selected" : ""}}>3 - El paciente necesita estar encamado más de la mitad del día por la presencia de síntomas. Necesita ayuda para la mayoría de las actividades de la vida diaria como por ejemplo el vestirse.</option>
												<option value="4" {{isset($comite->tx_ecog) && $comite->tx_ecog == "4" ? "selected" : ""}}>4 - El paciente permanece encamado el 100% del día y necesita ayuda para todas las actividades de la vida diaria, como por ejemplo la higiene corporal, la movilización en la cama e incluso la alimentación.</option>
												<option value="5" {{isset($comite->tx_ecog) && $comite->tx_ecog == "5" ? "selected" : ""}}>5 - Paciente Fallecido.</option>
											</select>					
										</div>
										<div class="col-sm-3">
											<label for="id_cie10">Cie-O<span style="color:#FF0000";>*</span></label>
											<div class="col-sm-12">
												<select class="form-control select_diagnosticos" id="id_cie10" name="id_cie10" required>
												</select>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="tab-pane" id="planPropuesto">
								<div class="form-group">
									<div class="row">
										<div class="col-sm-8">
											<label for="tx_plan">Plan<span style="color:#FF0000";>*</span></label>
											<textarea class="form-control noresize ckeditor" required id="tx_plan" name="tx_plan">{{isset($comite->tx_plan) ? $comite->tx_plan : ""}}</textarea>
										</div>
										<div class="col-sm-4">
											<label for="archivo">Archivo Adjunto</label>
											<input type="file" class="form-control" id="archivo" name="archivo">
											<label><span style="color:#FF0000";>{{isset($comite->tx_archivo) && $comite->tx_archivo != '' ? "Ya existe un archivo. Si sube otro se sobreescribira" : ""}}</span></label>
										</div>
									</div>
								</div>
							</div>
						</div>
						</div>
					</div>
				</div>
			</div>
		  	<div class="card-footer text-right">
				<button type="submit" class="btn btn-success" onclick="guardar()">Guardar</button>
		  	</div>
        </form>
        

    </div>
@stop

@section('js')
<script>
	$('.select2').select2();
	var url_diagnosticos = @json(url('getDiagnostico'));

	function validate() {
		var resultado_validacion=true;
		$("#editor_error").html("");
		var editor_val = CKEDITOR.instances.tx_plan.document.getBody().getChild(0).getText().trim();
		if(!(editor_val.length>0)){
			alert('Ingrese Plan');
			resultado_validacion=false;
		}       

		return resultado_validacion;
	}

	function guardar(){
		$("#fc_nacimiento").css('display','').attr('disabled', false);
		@if(request()->adulto == 1)
			$("#bo_antecedentes_familiares").css('display','').attr('required', true);
			$("#bo_cancer_previo").css('display','').attr('required', true);
			$("#bo_tabaco").css('display','').attr('required', true);
			$("#bo_oh").css('display','').attr('required', true);
			$("#tx_Cie10").css('display','').attr('required', true);
		@elseif(request()->adulto == 2)
			$("#bo_antecedentes_familiares").css('display','').attr('required', true);
			$("#bo_cancer_previo").css('display','').attr('required', true);
			$("#bo_tabaco").css('display','').attr('required', true);
			$("#bo_oh").css('display','').attr('required', true);
		@endif
	}

	function getPerson(rut){
		@if (!isset($user))
			$.getJSON("{{action('GetController@getDatosRut')}}?rut="+rut,
			function(data){
				if(data.encontrado == true){
					$("#tx_rut").val(data.rut);
					$("#nr_run").val(data.nr_run);
					$("#tx_digito_verificador").val(data.tx_digito_verificador);
					$("#id_sexo").val(data.id_sexo);
					$("#id_prevision").val(data.id_prevision);
					$("#id_clasificacion_fonasa").val(data.id_clasificacion_fonasa);
					$("#tx_nombre").val(data.tx_nombre_solo);
					$("#tx_nombre").css('display','').attr('readonly', true);
					$("#tx_apellido_paterno").val(data.tx_apellido_paterno);
					$("#tx_apellido_paterno").css('display','').attr('readonly', true);
					$("#tx_apellido_materno").val(data.tx_apellido_materno);
					$("#tx_apellido_materno").css('display','').attr('readonly', true);
					$("#fc_nacimiento").val(data.fc_nacimiento);
					$("#fc_nacimiento").css('display','').attr('disabled', true);
					$("#tx_mail").val(data.tx_mail);
					$("#nr_telefono").val(data.tx_telefono);
					$("#nr_ficha").val(data.nr_ficha);
				}else{
					alert("Rut no encontrado");
					$("#tx_rut").val("");
					$("#tx_nombre").val("");
					$("#tx_nombre").css('display','').attr('readonly', false);
					$("#tx_apellido_paterno").val("");
					$("#tx_apellido_paterno").css('display','').attr('readonly', false);
					$("#tx_apellido_materno").val("");
					$("#tx_apellido_materno").css('display','').attr('readonly', false);
				}
			})
		@endif
	};

	$('.select_diagnosticos').select2({
        width: '99%',
        allowClear: true,
        minimumInputLength: 3,
        placeholder: "Seleccione CIE10",
        language: {
            inputTooShort: function() {
                return 'Ingrese 3 o más caracteres para la búsqueda';
            }
        },
        ajax: {
            url: url_diagnosticos,
            dataType: 'json',
            data: function (params) {
                var query = {
                    search: params.term,
                    type: 'public'
                }
                return query;
            }
        }
    })

	@if (isset($comite))
		var comite = @json($comite);

		var id_cie10 = [];
		var newOption1 = new Option('{{$comite->cie10->dec10}}', {{ $comite->cie10->id }}, true, true);
		id_cie10.push(newOption1);
        $('#id_cie10').append(id_cie10).trigger('change');
	@endif
</script>
<script src="{{ asset('/vendors/ckeditor/ckeditor.js') }}"></script>
@stop