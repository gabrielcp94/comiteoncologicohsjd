<table>
    <tr>
        <th>ID</th>
        <th>Nombre</th>
        <th>Rut</th>
        <th>Edad</th>
        <th>F. Nacimiento</th>
        <th>Sexo</th>
        <th>Paciente Nuevo</th>
        <th>F. Presentación</th>
        <th>Médico</th>
        <th>Especialidad</th>
        <th>Diagnóstico</th>
        <th>Tratamiento comite</th>
        <th>Tratamiento propuesto</th>
        <th>Plan</th>
        <th>Curativo</th>
        <th>CIEO</th>
        <th>Cateter QT</th>
    </tr>
    @foreach($comites as $comite)
    <tr>
        <td>{{$comite->id}}</td>
        <td>{{$comite->tx_nombre_completo}}</td>
        <td>{{$comite->tx_rut}}</td>
        <td>{{$comite->edad}}</td>
        <td>@if((isset($comite->fc_nacimiento) && $comite->fc_nacimiento != '0000-00-00'))
            {{date('d/m/Y', strtotime(str_replace("/",".",$comite->fc_nacimiento)))}}
        @else 
            Sin Información
        @endif</td>
        <td>@if($comite->id_sexo == 1)
                Hombre
            @elseif($comite->id_sexo == 2)
                Mujer
            @endif
        </td>
        <td>{{$comite->tx_paciente_nuevo}}</td>
        <td>{{date('d-m-Y', strtotime($comite->fc_ingreso))}}</td>
        <td>{{$comite->tx_medico}}</td>
        <td>{{ucwords(mb_strtolower($comite->tx_especialidad))}}</td>
        <td>{{$comite->tx_diagnostico_actual}}</td>
        <td>{{isset($comite->resolucion->tx_tratamiento_comite) ? $comite->resolucion->tx_tratamiento_comite : ''}}</td>
        <td>{{isset($comite->resolucion->tx_tratamiento_propuesto) ? $comite->resolucion->tx_tratamiento_propuesto : ''}}</td>
        <td>{{isset($comite->tx_plan) ? $comite->tx_plan : ''}}</td>
        <td>{{isset($comite->resolucion->tx_curativo) ? $comite->resolucion->tx_curativo : ''}}</td>
        <td>{{isset($comite->tx_Cie10) ? $comite->tx_Cie10 : ''}}</td>
        <td>{{$comite->resolucion->tx_cateter_qt ?? ''}}</td>
    </tr>
    @endforeach
</table>