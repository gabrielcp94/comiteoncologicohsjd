<!DOCTYPE html>
<html lang="es">
    <head>
        {{-- <link rel="stylesheet" href="{{url('/')}}/css/base.css" />
        <link rel="stylesheet" href="{{url('/')}}/css/bootstrap.min.css" /> --}}
        <style type="text/css">
			table.table-bordered{ font-size: 11px; }
			table.table-bordered td{ padding: 3px;border-color: #909090!important;color: #000000!important;}
			table.table-bordered th{ padding: 3px;border-color: #909090!important;}
			.table tr td{
                font-family:monospace;
				font-size: 11px;
                /* LINE-HEIGHT:1px; */
			}
			.justify{
				text-align: justify;
			}
			.table-responsive{
				page-break-inside: avoid;
			}
            h4{
                text-align: center;
                text-transform: uppercase;
            }
            .texto{
                font-size: 18px;
                font-family:'Courier New', Courier, monospace;
            }
            .texto2{
                font-size: 18px;
                LINE-HEIGHT:0px;
            }
		</style>
        <meta charset="UTF-8">
        <title>Comite Oncologico Formulario</title>
    </head>
    <body class="texto">
        <div class="col-xs-12 padding-0" >
            <table class="table" width="100%">
                <tr colspan="12">
                    <td width="10%">
                        <img style="max-width: 60px;" src="vendor/adminlte/dist/img/logo.png" >
                    </td>
                    <td>
                        <p style="margin-bottom:-15px;">Servicio Salud Metropolitano Occidente</p>
                        <p>Hospital San Juan de Dios - CDT</p>
                    </td>    
                    <td width="30%">
                        <img style="max-width: 80px;" src="vendor/adminlte/dist/img/logo-comite.jpg" >
                    </td>               
                </tr>
            </table>
        </div>
        <h4>Fecha de Presentación a Comité Adulto: {{date('d-m-Y', strtotime($comite->fc_ingreso))}} <br> N° Comite: {{$comite->id}}</h4>
        <div class="table-responsive" >
            <table class="table table-bordered " width="100%">
                <tr>
                    <td width="20%" style="background-color: #eeeeee;">
                        <strong>Medico Tratante</strong>
                    </td>
                    <td width="30%" >
                        {{$comite->tx_medico}}
                    </td>
                    <td width="20%" style="background-color: #eeeeee;">
                        <strong>Especialidad</strong>
                    </td>
                    <td width="30%" >
                        {{ucwords(mb_strtolower($comite->tx_especialidad))}}
                    </td>
                </tr>
            </table>
        </div>
        <div class="table-responsive" >
            <table class="table table-bordered " width="100%">
                <tr>
                    <td colspan="6" align="center" style="background-color: #999999;" >
                        <strong>Paciente</strong>
                    </td>
                </tr>
                <tr>
                    <td width="10%" style="background-color: #eeeeee;">
                        <strong>Nombre</strong>
                    </td>
                    <td width="35%" >
                        {{$comite->tx_nombre}}
                    </td>
                    <td width="15%" style="background-color: #eeeeee;">
                        <strong>Apellido Paterno</strong>
                    </td>
                    <td width="15%" >
                        {{$comite->tx_apellido_paterno}}
                    </td>
                    <td width="10%" style="background-color: #eeeeee;">
                        <strong>Apellido Materno</strong>
                    </td>
                    <td width="15%" >
                        {{$comite->tx_apellido_materno}}
                    </td>
                </tr>
                <tr>
                    <td style="background-color: #eeeeee;">
                        <strong>F. Nacimiento</strong>
                    </td>
                    <td>
                        {{date('d-m-Y', strtotime($comite->fc_nacimiento))}} <br> ({{$comite->edad}})
                    </td>
                    <td style="background-color: #eeeeee;">
                        <strong>Rut</strong>
                    </td>
                    <td>
                        {{$comite->tx_rut}}
                    </td>
                    <td style="background-color: #eeeeee;">
                        <strong>Ficha</strong>
                    </td>
                    <td>
                        {{$comite->nr_ficha}}
                    </td>
                </tr>
            </table>
            </div>
            <div class="table-responsive" >
                <table class="table table-bordered " width="100%">
                    <tr>
                        <td colspan="8" align="center" style="background-color: #999999;" >
                            <strong>Antecedentes Previos</strong>
                        </td>
                    </tr>
                    <tr>
                        <td width="10%" style="background-color: #eeeeee;">
                            <strong>Cáncer Previo</strong>
                        </td>
                        <td width="10%" >
                            {{$comite->tx_cancer_previo}}
                        </td>
                        <td width="10%" style="background-color: #eeeeee;">
                            <strong>Tipo Cáncer Previo</strong>
                        </td>
                        <td width="15%" >
                            {{isset($comite->tx_tipo_cancer) ? $comite->tx_tipo_cancer : ''}}
                        </td>
                        <td width="10%" style="background-color: #eeeeee;">
                            <strong>Antecedentes Familiares</strong>
                        </td>
                        <td width="10%" >
                            {{$comite->tx_antecedentes_familiares}}
                        </td>
                        <td width="10%" style="background-color: #eeeeee;">
                            <strong>Comorbilidad</strong>
                        </td>
                        <td width="25%" >
                            {{isset($comite->tx_comorbilidad) ? str_replace(';', '; ',$comite->tx_comorbilidad) : ''}}
                        </td>
                    </tr>
                    <tr>
                        <td width="10%" style="background-color: #eeeeee;">
                            <strong>Tabaco</strong>
                        </td>
                        <td width="15%" >
                            {{$comite->tx_tabaco}}
                        </td>
                        <td width="10%" style="background-color: #eeeeee;">
                            <strong>Oh</strong>
                        </td>
                        <td width="15%" >
                            {{$comite->tx_oh}}
                        </td>
                        <td width="10%" style="background-color: #eeeeee;">
                            <strong>Otro Antecedente</strong>
                        </td>
                        <td width="15%" colspan="3">
                            {{isset($comite->tx_otro_antecedente) ? $comite->tx_otro_antecedente : ''}}
                        </td>
                    </tr>
                    <tr>
                        <td width="10%" style="background-color: #eeeeee;">
                            <strong>Historia Clínica</strong>
                        </td>
                        <td width="15%" colspan="7">
                            {!!isset($comite->tx_historia_clinica) ? $comite->tx_historia_clinica : ''!!}
                        </td>
                    </tr>
                    <tr>
                        <td width="10%" style="background-color: #eeeeee;">
                            <strong>Resultado Imagen</strong>
                        </td>
                        <td width="15%" colspan="7">
                            {!!isset($comite->tx_resultado_imagen) ? $comite->tx_resultado_imagen : ''!!}
                        </td>
                    </tr>
                </table>
            </div>
            <div class="table-responsive" >
                <table class="table table-bordered " width="100%">
                    <tr>
                        <td colspan="10" align="center" style="background-color: #999999;" >
                            <strong>Diagnostico</strong>
                        </td>
                    </tr>
                    <tr>
                        <td width="5%" colspan="2" style="background-color: #eeeeee;">
                            <strong>Cie-O</strong>
                        </td>
                        <td width="10%" colspan="8">
                            {{isset($comite->tx_Cie10) ? $comite->tx_Cie10 : ''}}
                        </td>
                    </tr>
                    <tr>
                        <td width="5%" colspan="2" style="background-color: #eeeeee;">
                            <strong>Diagnóstico Actual</strong>
                        </td>
                        <td width="10%" colspan="8">
                            {!!isset($comite->tx_diagnostico_actual) ? $comite->tx_diagnostico_actual : ''!!}
                        </td>
                    </tr>
                    <tr>
                        <td width="5%" colspan="2" style="background-color: #eeeeee;">
                            <strong>Ecog</strong>
                        </td>
                        <td width="10%" colspan="8">
                            @switch($comite->tx_ecog)
                                @case(0)
                                    0 - Totalmente asintomático y es capaz de realizar un trabajo y actividades normales de la vida diaria.
                                    @break
                                @case(1)
                                    1 - Presenta síntomas que le impiden realizar trabajos pesados, aunque se desempeña normalmente en sus actividades cotidianas y en trabajos ligeros. El paciente sólo permanece en la cama durante las horas de sueño nocturno.
                                    @break
                                @case(2)
                                    2 - El paciente no es capaz de desempeñar ningún trabajo, se encuentra con síntomas que le obligan a permanecer en la cama durante varias horas al día, además de las de la noche, pero que no superan el 50% del día. El individuo satisface la mayoría de sus necesidades personales solo.
                                @break
                                @case(3)
                                    3 - El paciente necesita estar encamado más de la mitad del día por la presencia de síntomas. Necesita ayuda para la mayoría de las actividades de la vida diaria como por ejemplo el vestirse.
                                @break
                                @case(4)
                                    4 - El paciente permanece encamado el 100% del día y necesita ayuda para todas las actividades de la vida diaria, como por ejemplo la higiene corporal, la movilización en la cama e incluso la alimentación.
                                @break
                                @case(5)
                                    5 - Paciente Fallecido.
                                @break
                                @default
                            @endswitch
                        </td>
                    </tr>
                    <tr>
                        <td width="5%" colspan="2" style="background-color: #eeeeee;">
                            <strong>Biopsia Pre Operatoria</strong>
                        </td>
                        <td width="10%" colspan="8">
                            {!!isset($comite->tx_biopsia_pre_Op) ? $comite->tx_biopsia_pre_Op : ''!!}
                        </td>
                    </tr>
                    <tr>
                        <td width="5%" colspan="2" style="background-color: #eeeeee;">
                            <strong>Biopsia Operatoria</strong>
                        </td>
                        <td width="10%" colspan="8">
                            {!!isset($comite->tx_biopsia__Op) ? $comite->tx_biopsia__Op : ''!!}
                        </td>
                    </tr>
                    <tr>
                        <td width="5%" style="background-color: #eeeeee;">
                            <strong>Estrógeno</strong>
                        </td>
                        <td width="10%" >
                            {{isset($comite->tx_el) ? $comite->tx_el : ''}}
                        </td>
                        <td width="5%" style="background-color: #eeeeee;">
                            <strong>Estrógeno %</strong>
                        </td>
                        <td width="10%" >
                            {{isset($comite->nr_el_porcentaje) ? $comite->nr_el_porcentaje : ''}}
                        </td>
                        <td width="5%" colspan="2" style="background-color: #eeeeee;">
                            <strong>Estrógeno Incremento</strong>
                        </td>
                        <td width="5%" colspan="4">
                            {{isset($comite->tx_el_valor) ? $comite->tx_el_valor : ''}}
                        </td>
                    </tr>
                    <tr>
                        <td width="5%" style="background-color: #eeeeee;">
                            <strong>Progesterona</strong>
                        </td>
                        <td width="10%" >
                            {{isset($comite->tx_progesterona) ? $comite->tx_progesterona : ''}}
                        </td>
                        <td width="5%" style="background-color: #eeeeee;">
                            <strong>Progesterona %</strong>
                        </td>
                        <td width="10%" >
                            {{isset($comite->nr_progesterona_porcentaje) ? $comite->nr_progesterona_porcentaje : ''}}
                        </td>
                        <td width="5%" colspan="2" style="background-color: #eeeeee;">
                            <strong>Progesterona Incremento</strong>
                        </td>
                        <td width="5%" colspan="4">
                            {{isset($comite->tx_progesterona_valor) ? $comite->tx_progesterona_valor : ''}}
                        </td>
                    </tr>
                    <tr>
                        <td width="5%" style="background-color: #eeeeee;">
                            <strong>Her 2</strong>
                        </td>
                        <td width="10%" >
                            {{isset($comite->tx_ger2) ? $comite->tx_ger2 : ''}}
                        </td>
                        <td width="5%" style="background-color: #eeeeee;">
                            <strong>FISH</strong>
                        </td>
                        <td width="10%" >
                            {{isset($comite->tx_fish) ? $comite->tx_fish : ''}}
                        </td>
                        <td width="5%" colspan="2" style="background-color: #eeeeee;">
                            <strong>KI 67</strong>
                        </td>
                        <td width="5%" colspan="4">
                            {{isset($comite->nr_ki67) ? $comite->nr_ki67 : ''}}
                        </td>
                    </tr>
                    <tr>
                        <td width="5%" style="background-color: #eeeeee;">
                            <strong>Linfonodos Resecados</strong>
                        </td>
                        <td width="5%" >
                            {{isset($comite->nr_ln_total) ? $comite->nr_ln_total : ''}}
                        </td>
                        <td width="5%" style="background-color: #eeeeee;">
                            <strong>Linfonodos (+)</strong>
                        </td>
                        <td width="5%" >
                            {{isset($comite->nr_ln_pos) ? $comite->nr_ln_pos : ''}}
                        </td>
                        <td width="5%" style="background-color: #eeeeee;">
                            <strong>CEA</strong>
                        </td>
                        <td width="5%" >
                            {{isset($comite->nr_cea) ? $comite->nr_cea : ''}}
                        </td>
                        <td width="5%" style="background-color: #eeeeee;">
                            <strong>CEA 19-9</strong>
                        </td>
                        <td width="5%" >
                            {{isset($comite->nr_cea_19_9) ? $comite->nr_cea_19_9 : ''}}
                        </td>
                        <td width="5%" style="background-color: #eeeeee;">
                            <strong>CEA 125</strong>
                        </td>
                        <td width="5%" >
                            {{isset($comite->nr_cea_125) ? $comite->nr_cea_125 : ''}}
                        </td>
                    </tr>
                    <tr>
                        <td width="5%" style="background-color: #eeeeee;">
                            <strong>PSA</strong>
                        </td>
                        <td width="5%" >
                            {{isset($comite->nr_psa) ? $comite->nr_psa : ''}}
                        </td>
                        <td width="5%" style="background-color: #eeeeee;">
                            <strong>AFP</strong>
                        </td>
                        <td width="5%" >
                            {{isset($comite->nr_afp) ? $comite->nr_afp : ''}}
                        </td>
                        <td width="5%" style="background-color: #eeeeee;">
                            <strong>HCG</strong>
                        </td>
                        <td width="5%" >
                            {{isset($comite->nr_hcg) ? $comite->nr_hcg : ''}}
                        </td>
                    </tr>
                    <tr>
                        <td width="5%" style="background-color: #eeeeee;">
                            <strong>T</strong>
                        </td>
                        <td width="10%" >
                            {{isset($comite->tx_t) ? $comite->tx_t : ''}}
                        </td>
                        <td width="5%" style="background-color: #eeeeee;">
                            <strong>N</strong>
                        </td>
                        <td width="10%" >
                            {{isset($comite->tx_n) ? $comite->tx_n : ''}}
                        </td>
                        <td width="5%" style="background-color: #eeeeee;">
                            <strong>M</strong>
                        </td>
                        <td width="5%" >
                            {{isset($comite->tx_m) ? $comite->tx_m : ''}}
                        </td>
                        <td width="5%" style="background-color: #eeeeee;">
                            <strong>Etapa</strong>
                        </td>
                        <td width="5%" colspan="3">
                            {{isset($comite->tx_etapa) ? $comite->tx_etapa : ''}}
                        </td>
                    </tr>
                    <tr>
                        <td width="5%" colspan="2" style="background-color: #eeeeee;">
                            <strong>Plan</strong>
                        </td>
                        <td width="10%" colspan="8">
                            {!!isset($comite->tx_plan) ? $comite->tx_plan : ''!!}
                        </td>
                    </tr>
                    <tr>
                        <td width="5%" colspan="2" style="background-color: #eeeeee;">
                            <strong>N° Comité</strong>
                        </td>
                        <td width="10%" colspan="8">
                            {{isset($comite->id_comite) ? $comite->id_comite : $comite->id}}
                        </td>
                    </tr>
                </table>
                @if(request()->resuelto == 1)
                </div>
                <div class="table-responsive" >
                    <table class="table table-bordered " width="100%">
                        <tr>
                            <td colspan="8" align="center" style="background-color: #999999;" >
                                <strong>Resolucion Comité</strong>
                            </td>
                        </tr>
                        <tr>
                            <td width="10%" style="background-color: #eeeeee;">
                                <strong>F. Resolucion</strong>
                            </td>
                            <td width="15%" >
                                {{date('d-m-Y', strtotime($comite->resolucion->fc_resolucion))}}
                            </td>
                            <td width="10%" style="background-color: #eeeeee;">
                                <strong>Tratamiento Propuesto</strong>
                            </td>
                            <td width="15%" >
                                {{isset($comite->resolucion->tx_tratamiento_propuesto) ? $comite->resolucion->tx_tratamiento_propuesto : ''}}
                            </td>
                            <td width="10%" style="background-color: #eeeeee;">
                                <strong>Curativo</strong>
                            </td>
                            <td width="15%" >
                                {{isset($comite->resolucion->tx_curativo) ? $comite->resolucion->tx_curativo : ''}}
                            </td>
                            <td width="10%" style="background-color: #eeeeee;">
                                <strong>Cateter QMT</strong>
                            </td>
                            <td width="15%" >
                                {{isset($comite->resolucion->tx_cateter_qt) ? $comite->resolucion->tx_cateter_qt : ''}}
                            </td>
                        </tr>
                        <tr>
                            <td width="10%" colspan="2" style="background-color: #eeeeee;">
                                <strong>Tratamiento Comité</strong>
                            </td>
                            <td width="15%" colspan="6">
                                <strong>{!!isset($comite->resolucion->tx_tratamiento_comite) ? $comite->resolucion->tx_tratamiento_comite : ''!!}</strong>
                            </td>
                        </tr>
                        <tr>
                            <td width="10%" colspan="2" style="background-color: #eeeeee;">
                                <strong>Estado</strong>
                            </td>
                            <td width="15%" colspan="6">
                                {{isset($comite->resolucion->tx_estado) ? $comite->resolucion->tx_estado : ''}}
                            </td>
                        </tr>
                        <tr>
                            <td width="10%" colspan="2" style="background-color: #eeeeee;">
                                <strong>Integrantes</strong>
                            </td>
                            <td width="15%" colspan="6">
                                {{isset($comite->resolucion->id_integrantes_fecha) ? $comite->resolucion->id_integrantes_fecha : ''}}
                            </td>
                        </tr>
                    </table>
                @endif
        </div>
    </body>
</html>