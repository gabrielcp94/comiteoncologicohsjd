@extends('adminlte::page')

@section('title', 'Lista de Pacientes Adultos')

@section('content_header')
    @if(session()->has('message'))
        <div class="alert alert-success">
            {{ session()->get('message') }}
        </div>
    @endif
    @if(session()->has('error'))
        <div class="alert alert-danger">
            <i class="fa fa-exclamation-triangle"></i> {{ session()->get('error') }}
        </div>
    @endif
@stop

@section('content')
		{{-- <form class="form-horizontal" id="form" method="POST">
			{{ csrf_field() }} --}}
	<div class="card card-info">
		<div class="card-header">
		<h3 class="card-title">Estadisticas de Pacientes Adulto</h3>
		</div>
		<!-- /.card-header -->
		<!-- form start -->
		<div class="card-body">
            <div id="example1_wrapper" class="dataTables_wrapper dt-bootstrap4">
                <div class="row">
                    <div class="col-sm-12">
                        <form class="form-horizontal">
                            <div class="form-group row">
                                <div class="col-sm-4">
                                    <label for="fc_inicio">Fecha de Inicio</label>
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text"><i class="far fa-calendar-alt"></i></span>
                                        </div>
                                        <input type="date" class="form-control" id="fc_inicio" name="fc_inicio" value="{{isset(request()->fc_inicio) ? request()->fc_inicio : date("Y-m-d")}}">
                                    </div>	
                                </div>
                                <div class="col-sm-4">
                                    <label for="fc_termino">Fecha de Termino</label>
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text"><i class="far fa-calendar-alt"></i></span>
                                        </div>
                                        <input type="date" class="form-control" id="fc_termino" name="fc_termino" value="{{isset(request()->fc_termino) ? request()->fc_termino : date("Y-m-d")}}">
                                    </div>	
                                </div>
                                <div class="col-sm-4 text-right">
                                    <button type="submit" class="btn btn-info">Filtrar</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.card-body -->
    </div>
    <div class="card bg-gradient collapsed-card">
        <button type="button" class="btn btn btn-sm card-info" data-card-widget="collapse">
            <div class="card-header border-0 ui-sortable-handle">
            <h3 class="card-title">
                <i class="fa fa-user"></i>
                Por Diagnóstico Cie10
            </h3>
            <!-- tools card -->
            <div class="card-tools">
                <!-- button with a dropdown -->
                <i class="fas fa-minus"></i>
            </div>
            <!-- /. tools -->
            </div>
        </button>
        <!-- /.card-header -->
        <div class="card-body pt-0">
            <!--The calendar -->
            <table class="table table-striped table-hover" id="tableUsers">
                <thead>
                    <th>Cie10</th>
                    <th>Resuelto</th>
                    <th>Por Resolver</th>
                </thead>
                <tbody>
                    @foreach ($comitesPorCie10->sortByDesc('total') as $comite)
                        <tr role="row" class="odd">
                            <td>{{$comite->nombre != '' ? ucwords(mb_strtolower($comite->nombre)) : 'Sin Cie10'}}</td>
                            <td>{{$comite->resuelto}}</td>
                            <td>{{$comite->porResolver}}</td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
        <!-- /.card-body -->
    </div>
    <div class="card bg-gradient collapsed-card">
        <button type="button" class="btn btn btn-sm card-info" data-card-widget="collapse">
            <div class="card-header border-0 ui-sortable-handle">
            <h3 class="card-title">
                <i class="fa fa-user"></i>
                Por Edad
            </h3>
            <!-- tools card -->
            <div class="card-tools">
                <!-- button with a dropdown -->
                <i class="fas fa-minus"></i>
            </div>
            <!-- /. tools -->
            </div>
        </button>
        <!-- /.card-header -->
        <div class="card-body pt-0">
            <!--The calendar -->
            <table class="table table-striped table-hover" id="tableUsers">
                <thead>
                    <th>Edad</th>
                    <th>Resuelto</th>
                    <th>Por Resolver</th>
                </thead>
                <tbody>
                    <tr role="row" class="odd">
                        <td>Menor de 18</td>
                        <td>{{$rangoResuelto[1]}}</td>
                        <td>{{$rangoPorResolver[1]}}</td>
                    </tr>
                    <tr role="row" class="odd">
                        <td>18-40</td>
                        <td>{{$rangoResuelto[2]}}</td>
                        <td>{{$rangoPorResolver[2]}}</td>
                    </tr>
                    <tr role="row" class="odd">
                        <td>40-60</td>
                        <td>{{$rangoResuelto[3]}}</td>
                        <td>{{$rangoPorResolver[3]}}</td>
                    </tr>
                    <tr role="row" class="odd">
                        <td>Mayor de 60</td>
                        <td>{{$rangoResuelto[4]}}</td>
                        <td>{{$rangoPorResolver[4]}}</td>
                    </tr>
                </tbody>
            </table>
        </div>
        <!-- /.card-body -->
    </div>
@stop

@section('js')
<script>
    function excel(){
        var url = "excelAdulto?"+$( "form" ).serialize();
        window.location.href = url;
        alert('descargando excel...');
    }

    @if(isset(request()->especialidades))
        var user = @json(request()->all());
        var especialidades = user.especialidades;
        $("#especialidades").val(especialidades).trigger('change');
    @endif

    @if(isset(request()->medicos))
        var user = @json(request()->all());
        var medicos = user.medicos;
        $("#medicos").val(medicos).trigger('change');
    @endif

	$(".alert-success").fadeTo(20000, 500).slideUp(500, function(){
        $(".alert-success").slideUp(1000);
    });

    $(".alert-danger").fadeTo(20000, 5000).slideUp(500, function(){
        $(".alert-danger").slideUp(1000);
    });

    $('.select2').select2();
</script>
@stop