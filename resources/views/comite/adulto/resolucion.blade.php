@extends('adminlte::page')

@section('title', 'Resolver Comite')

@section('content')
		{{-- <form class="form-horizontal" id="form" method="POST">
			{{ csrf_field() }} --}}
		<!-- /.card-header -->
		<!-- form start -->
		<form role="form" class="form-horizontal" id="form" method="POST" action="{{action('ResolucionAdultoController@store')}}">
			{{ csrf_field() }}
			<input type="hidden" id="id" name="id" value="{{request()->id}}"/>
			<input type="hidden" id="tx_rut" name="tx_rut" value="{{$comite->tx_rut}}"/>
			<input type="hidden" id="fc_resolucion" name="fc_resolucion" value="{{$comite->fc_ingreso}}"/>

			<div class="card bg-gradient sticky-top card-info">
				<button type="button" class="btn btn btn-sm card-info" data-card-widget="collapse">
					<div class="card-header border-0 ui-sortable-handle">
					<h3 class="card-title">
						<i class="fa fa-user"></i>
						Datos Generales. (<strong>N° Comité:</strong> {{$comite->id}})
					</h3>
					<!-- tools card -->
					<div class="card-tools">
						<!-- button with a dropdown -->
						<i class="fas fa-minus"></i>
					</div>
					<!-- /. tools -->
					</div>
				</button>
				<!-- /.card-header -->
				<div class="card-body pt-0">
					<!--The calendar -->
					<div class="row invoice-info">
						<div class="col-sm-4 invoice-col">
							<strong>Médico Tratante: </strong>{{$comite->tx_medico}}<br><br>
							<strong>Rut: </strong>{{$comite->tx_rut}}<br>
							<strong>Ficha: </strong>{{$comite->nr_ficha}}<br>
						</div>
						<div class="col-sm-4 invoice-col">
							<strong>Especialidad: </strong>{{ucwords(mb_strtolower($comite->tx_especialidad))}}<br><br>
							<strong>Nombre: </strong>{{$comite->tx_nombre_completo}}<br>
						</div>
						<div class="col-sm-4 invoice-col">
							<strong>{{isset($comite->establecimiento->tx_nombre) ? 'Hospital Origen:' : ''}} </strong>{{isset($comite->establecimiento->tx_nombre) ? $comite->establecimiento->tx_nombre : ''}}<br><br>
							<strong>F. Nacimiento: </strong>{{date('d-m-Y', strtotime($comite->fc_nacimiento))}} ({{$comite->nr_edad}})<br>
							<strong>F. Presentación: </strong>{{date('d-m-Y', strtotime($comite->fc_ingreso))}}<br>
						</div>
					</div>
				</div>
				<!-- /.card-body -->
			</div>

			<div class="card bg-gradient collapsed-card">
				<button type="button" class="btn btn btn-sm card-info" data-card-widget="collapse">
					<div class="card-header border-0 ui-sortable-handle">
						<h3 class="card-title">
							<i class="fa fa-book-medical"></i>
							Antecedentes Mórbidos
						</h3>
						<div class="card-tools">
							<i class="fas fa-plus"></i>
						</div>
					<!-- /. tools -->
					</div>
				</button>
				<!-- /.card-header -->
				<div class="card-body pt-0">
					<div class="row invoice-info">
						<div class="col-sm-3 invoice-col">
							<strong>Antecedentes Familiares: </strong>{{$comite->tx_antecedentes_familiares}}<br>
							<strong>Tabaco : </strong>{{$comite->tx_tabaco}}<br>
						</div>
						<div class="col-sm-3 invoice-col">
							<strong>Cáncer Previo: </strong>{{$comite->tx_cancer_previo}}<br>
							<strong>OH: </strong>{{$comite->tx_oh}}<br>
							<strong>Revisar Imagen: </strong>{{$comite->tx_imagen}}<br>
						</div>
						<div class="col-sm-6 invoice-col">
							<strong>Tipo Cáncer Previo: </strong>{{isset($comite->tx_tipo_cancer) ? $comite->tx_tipo_cancer : ''}}<br>
							<strong>Otro Antecedente: </strong>{{isset($comite->tx_otro_antecedente) ? $comite->tx_otro_antecedente : ''}}<br>
							<strong>Comorbilidad: </strong>{{isset($comite->tx_comorbilidad) ? $comite->tx_comorbilidad : ''}}<br>
						</div>
					</div>
				</div>
			</div>
			<div class="card bg-gradient collapsed-card">
				<button type="button" class="btn btn btn-sm card-info" data-card-widget="collapse">
					<div class="card-header border-0 ui-sortable-handle">
						<h3 class="card-title">
							<i class="fa fa-book-medical"></i>
							Historia Clínica
						</h3>
						<div class="card-tools">
							<i class="fas fa-plus"></i>
						</div>
					<!-- /. tools -->
					</div>
				</button>
				<!-- /.card-header -->
				<div class="card-body pt-0">
					<div class="row invoice-info">
						<div class="col-sm-12 invoice-col">
							<div class="form-group">
								<div class="row">
									<div class="col-sm-12">
										<label for="tx_historia_clinica">Historia Clinica</label>
										<textarea class="form-control noresize ckeditor" id="tx_historia_clinica" name="tx_historia_clinica">{{isset($comite->tx_historia_clinica) ? $comite->tx_historia_clinica : ""}}</textarea>
									</div>
								</div>
							</div>							
							<strong>Resultado Imagen: </strong>{!!$comite->tx_resultado_imagen!!}<br>
						</div>
						<div class="col-sm-6 invoice-col">
							<strong>CEA: </strong>{{isset($comite->nr_cea) ? $comite->nr_cea : ''}}<br>
							<strong>CEA 125: </strong>{{isset($comite->nr_cea_125) ? $comite->nr_cea_125 : ''}}<br>
							<strong>HCG: </strong>{{isset($comite->nr_hcg) ? $comite->nr_hcg : ''}}<br>
						</div>
						<div class="col-sm-6 invoice-col">
							<strong>CEA 19-9: </strong>{{isset($comite->nr_cea_19_9) ? $comite->nr_cea_19_9 : ''}}<br>
							<strong>AFP: </strong>{{isset($comite->nr_afp) ? $comite->nr_afp : ''}}<br>
							<strong>PSA: </strong>{{isset($comite->nr_psa) ? $comite->nr_psa : ''}}<br>
						</div>
					</div>
				</div>
			</div>
			<div class="card bg-gradient collapsed-card">
				<button type="button" class="btn btn btn-sm card-info" data-card-widget="collapse">
					<div class="card-header border-0 ui-sortable-handle">
						<h3 class="card-title">
							<i class="fa fa-book-medical"></i>
							Diagnóstico
						</h3>
						<div class="card-tools">
							<i class="fas fa-plus"></i>
						</div>
					<!-- /. tools -->
					</div>
				</button>
				<!-- /.card-header -->
				<div class="card-body pt-0">
					<div class="row invoice-info">
						<div class="col-sm-4 invoice-col">
							<strong>Diagnóstico Actual: </strong>{!!$comite->tx_diagnostico_actual!!}<br>
							<strong>Linfonodos Resecados: </strong>{{isset($comite->nr_ln_total) ? $comite->nr_ln_total : ''}}<br>
							<strong>N: </strong>{{isset($comite->tx_n) ? $comite->tx_n : ''}}<br>
							<strong>Estrógeno: </strong>{{isset($comite->tx_el) ? $comite->tx_el : ''}}<br>
							<strong>Progesterona: </strong>{{isset($comite->tx_progesterona) ? $comite->tx_progesterona : ''}}<br>
							<strong>HER 2: </strong>{{isset($comite->tx_ger2) ? $comite->tx_ger2 : ''}}<br>
							<strong>ECOG: </strong>{{isset($comite->tx_ecog) ? $comite->tx_ecog : ''}}<br>
						</div>
						<div class="col-sm-4 invoice-col">
							<strong>Biopsia Pre Operatoria: </strong>{!!isset($comite->tx_biopsia_pre_Op) ? $comite->tx_biopsia_pre_Op : ''!!}<br>
							<strong>Linfonodos (+): </strong>{{isset($comite->nr_ln_pos) ? $comite->nr_ln_pos : ''}}<br>
							<strong>M: </strong>{{isset($comite->tx_m) ? $comite->tx_m : ''}}<br>
							<strong>Estrógeno %: </strong>{{isset($comite->nr_el_porcentaje) ? $comite->nr_el_porcentaje : ''}}<br>
							<strong>Progesterona %: </strong>{{isset($comite->nr_progesterona_porcentaje) ? $comite->nr_progesterona_porcentaje : ''}}<br>
							<strong>FISH: </strong>{{isset($comite->tx_fish) ? $comite->tx_fish : ''}}<br>
							<strong>Cie-O: </strong>{{isset($comite->tx_Cie10) ? $comite->tx_Cie10 : ''}}<br>
						</div>
						<div class="col-sm-4 invoice-col">
							<strong>Biopsia Operatoria: </strong>{!!isset($comite->tx_biopsia__Op) ? $comite->tx_biopsia__Op : ''!!}<br>
							<strong>T: </strong>{{isset($comite->tx_t) ? $comite->tx_t : ''}}<br>
							<strong>Etapa: </strong>{{isset($comite->tx_etapa) ? $comite->tx_etapa : ''}}<br>
							<strong>Estrógeno Incremento: </strong>{{isset($comite->tx_el_valor) ? $comite->tx_el_valor : ''}}<br>
							<strong>Progesterona Incremento: </strong>{{isset($comite->tx_progesterona_valor) ? $comite->tx_progesterona_valor : ''}}<br>
							<strong>Ki 67: </strong>{{isset($comite->nr_ki67) ? $comite->nr_ki67 : ''}}<br>
						</div>
					</div>
				</div>
			</div>
			<div class="card bg-gradient collapsed-card">
				<button type="button" class="btn btn btn-sm card-info" data-card-widget="collapse">
					<div class="card-header border-0 ui-sortable-handle">
						<h3 class="card-title">
							<i class="fa fa-book-medical"></i>
							Plan
						</h3>
						<div class="card-tools">
							<i class="fas fa-plus"></i>
						</div>
					<!-- /. tools -->
					</div>
				</button>
				<!-- /.card-header -->
				<div class="card-body pt-0">
					<div class="row invoice-info">
						<div class="col-sm-12 invoice-col">
							<strong>Plan: </strong>{!!$comite->tx_plan!!}<br>
						</div>
					</div>
				</div>
			</div>

			<div class="card bg-gradient collapsed-card">
				<button type="button" class="btn btn btn-sm card-info" data-card-widget="collapse">
					<div class="card-header border-0 ui-sortable-handle">
						<h3 class="card-title">
							<i class="fa fa-book-medical"></i>
							{{(isset($arr_epicrises) && count($arr_epicrises) > 0) || (isset($informes) && count($informes) > 0 && !empty($informes[0])) || (isset($solicitudesPabellon) && count($solicitudesPabellon) > 0)   ? 'Reportes Asociados' : 'NO EXISTEN REPORTES ASOCIADOS!'}}
						</h3>
						<div class="card-tools">
							<i class="fas fa-plus"></i>
						</div>
					<!-- /. tools -->
					</div>
				</button>
				<!-- /.card-header -->
				<div class="card-body pt-0">
					<div class="col-12 table-responsive">
						@if (isset($arr_epicrises) && count($arr_epicrises) > 0)
							<strong>Epicrisis</strong>
							<table class="table table-striped">
								<thead style="background-color: lightblue">
									<tr>
										<th>Codigo</th>
										<th>Tipo</th>
										<th>Medico</th>
										<th>F. Ingreso</th>
										<th>F. Egreso</th>
										<th><i class="fas fa-cog"></i></th>
									</tr>
								</thead>
								<tbody>
									@foreach ($arr_epicrises as $arr_epicrisis)
										<tr>
											<td>{{$arr_epicrisis['pac_corr']}}</td>
											<td>{{$arr_epicrisis['tipo']}}</td>
											<td>{{$arr_epicrisis['medico']['name']}}</td>
											<td>{{date('d-m-Y', strtotime($arr_epicrisis['fecha_ingreso']))}}</td>
											<td>{{date('d-m-Y', strtotime($arr_epicrisis['fecha_egreso']))}}</td>
											<td>
												<a class="btn btn-danger btn-xs" onclick="pdfEpicrisis({{$arr_epicrisis['id']}},'{{$arr_epicrisis['tipo']}}');" href="http://10.4.237.27/epicrisis-hsjd/api/epicrisis/pdf?token=5b7dbd9cf2d88&epicrisis_id={{$arr_epicrisis['id']}}&tipo={{$arr_epicrisis['tipo']}}" title="Archivo" target="_blank"><i class="fa fa-file-pdf" style="color:white"></i></a>
											</td>
										</tr>
									@endforeach
								</tbody>
							</table>
						@endif
					</div>
					<div class="col-12 table-responsive">
						@if (isset($informes) && count($informes) > 0 && !empty($informes[0]))
							<strong>Biopsia</strong>
							<table class="table table-striped">
								<thead style="background-color: lightcyan">
									<tr>
										<th>Tipo Estudio</th>
										<th>Código Estudio</th>
										<th>Servicio Solicitante</th>
										<th>F. Toma</th>
										<th>F. Cierre</th>
										<th><i class="fas fa-cog"></i></th>
									</tr>
								</thead>
								<tbody>
									
									@foreach ($informes as $key => $informe)
										<tr>
										<td>{{$informe->tipoEstudio}}</td>
											<td>{{$informe->codigoEstudio}}</td>
											<td>{{$informe->servicio_solitante}}</td>
											<td>{{date('d-m-Y', strtotime($informe->fecha_toma_muestra))}}</td>
											<td>{{date('d-m-Y', strtotime($informe->fechaCierre))}}</td>
											<td>
												<a class="btn btn-danger btn-xs" onclick="pdfBiopsia('{{isset($comite->paciente) ? $comite->paciente->tx_rut : $comite->tx_rut}}',{{$key}});" href="/getBiopsia?rut={{isset($comite->paciente) ? $comite->paciente->tx_rut : $comite->tx_rut}}&fila={{$key}}" title="Archivo" target="_blank"><i class="fa fa-file-pdf" style="color:white"></i></a>
											</td>
										</tr>
									@endforeach
								</tbody>
							</table>
						@endif
					</div>
					<div class="col-12 table-responsive">
						@if (isset($solicitudesPabellon) && count($solicitudesPabellon) > 0)
							<strong>Protocolo</strong>
							<table class="table table-striped">
								<thead style="background-color: lightyellow">
									<tr>
										<th>Fecha Protocolo</th>
										<th>Diagnóstico Pre-Operatorio</th>
										<th>Cirugía Practictada</th>
										<th><i class="fas fa-cog"></i></th>
									</tr>
								</thead>
								<tbody>
									@foreach ($solicitudesPabellon as $solicitudPabellon)
										<tr>
											<td>{{date('d-m-Y', strtotime($solicitudPabellon->protocolo->fc_entrada_pabellon))}}</td>
											<td>{{$solicitudPabellon->protocolo->gl_diagnostico_pre}}</td>
											<td>{{$solicitudPabellon->protocolo->gl_cirugia_practicada}}</td>
											<td>
												<a class="btn btn-danger btn-xs" onclick="pdfProtocolo({{$solicitudPabellon->protocolo->id}});" href="/getProtocolos?protocolo_id={{$solicitudPabellon->protocolo->id}}" title="Archivo" target="_blank"><i class="fa fa-file-pdf" style="color:white"></i></a>
											</td>
										</tr>
									@endforeach
								</tbody>
							</table>
						@endif
					</div>
				</div>
			</div>

			<div class="card bg-gradient collapsed-card">
				<button type="button" class="btn btn btn-sm card-info" data-card-widget="collapse">
					<div class="card-header border-0 ui-sortable-handle">
						<h3 class="card-title">
							<i class="fa fa-book-medical"></i>
							Resolver
						</h3>
						<div class="card-tools">
							<i class="fas fa-plus"></i>
						</div>
					<!-- /. tools -->
					</div>
				</button>
				<!-- /.card-header -->
				<div class="card-body pt-0">
				<!--The calendar -->
				<div class="card-body">
					<div class="form-group">
						<div class="row">
							<div class="col-sm-3">
								<label for="tx_curativo">Curativo</label>
								<div class="row">
									<div class="col-sm-3">
										<label>Sí</label>
									</div>
									<div class="col-sm-1">
										<input class="form-check-input" type="radio" id="tx_curativo" name="tx_curativo" value="SI" {{isset($comite->tx_curativo) && $comite->tx_curativo == 'SI' ? "checked" : ""}}>
									</div>
									<div class="col-sm-3">
										<label>No</label>
									</div>
									<div class="col-sm-1">
										<input class="form-check-input" type="radio" id="tx_curativo" name="tx_curativo" value="NO" {{isset($comite->tx_curativo) && $comite->tx_curativo == 'NO' ? "checked" : ""}}>
									</div>
								</div>
							</div>
							<div class="col-sm-4">
								<label for="tx_estado">Estado<span style="color:#FF0000";>*</span></label>
								<div class="row">
									<div class="col-sm-5">
										<label>RESUELTO</label>
									</div>
									<div class="col-sm-1">
										<input class="form-check-input" type="radio" id="tx_estado" name="tx_estado" value="RESUELTO" {{isset($comite->tx_estado) && $comite->tx_estado == 'RESUELTO' ? "checked" : ""}} required>
									</div>
									<div class="col-sm-5">
										<label>REPRESENTAR</label>
									</div>
									<div class="col-sm-1">
										<input class="form-check-input" type="radio" id="tx_estado" name="tx_estado" value="REPRESENTAR" {{isset($comite->tx_estado) && $comite->tx_estado == 'REPRESENTAR' ? "checked" : ""}}>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="form-group">
						<div class="row">
							<div class="col-sm-12">
								<label for="tx_tratamiento_comite">Tratamiento Comité</label>
								<textarea class="form-control noresize ckeditor" id="tx_tratamiento_comite" name="tx_tratamiento_comite">{{isset($comite->formulario->tx_tratamiento_comite) ? $comite->formulario->tx_tratamiento_comite : ""}}</textarea>
							</div>
						</div>
					</div>
					<div class="form-group">
						<div class="row">
							<div class="col-sm-3">
								<label for="tratamientos">Tratamiento Propuesto:</label>
								<select class="select2 select2-hidden-accessible" id="tratamientos" name="tratamientos[]" multiple data-placeholder="Seleccione tratamientos" style="width: 100%;" data-select2-id="7" tabindex="-1" aria-hidden="true">
									<option value=""></option>
									@foreach ($tratamientos as $tratamiento)
										<option value={{$tratamiento->id}}>{{$tratamiento->tx_descripcion}}</option>
									@endforeach
								</select>
							</div>
							<div class="col-sm-6">
								<label for="id_integrantes_fecha">Integrantes:</label>
								<select class="select2 select2-hidden-accessible" id="id_integrantes_fecha" name="id_integrantes_fecha" data-placeholder="Seleccione tratamientos" style="width: 100%;" data-select2-id="7" tabindex="-1" aria-hidden="true" required>
									<option value=""></option>
									@foreach ($integrantes as $integrante)
										<option value={{$integrante->id}} selected>{{isset($integrante->fc_lista) ? date('d-m-Y', strtotime($integrante->fc_lista)) : ''}} {{$integrante->tx_integrantes}}</option>
									@endforeach
								</select>
							</div>
							<div class="col-sm-3">
								<label for="tx_cateter_qt">Requiere Cateter QMT</label>
								<div class="row">
									<div class="col-sm-3">
										<label>Sí</label>
									</div>
									<div class="col-sm-1">
										<input class="form-check-input" type="radio" id="tx_cateter_qt" name="tx_cateter_qt" value="SI" {{isset($comite->tx_cateter_qt) && $comite->tx_cateter_qt == 'SI' ? "checked" : ""}}>
									</div>
									<div class="col-sm-3">
										<label>No</label>
									</div>
									<div class="col-sm-1">
										<input class="form-check-input" type="radio" id="tx_cateter_qt" name="tx_cateter_qt" value="NO" {{isset($comite->tx_cateter_qt) && $comite->tx_cateter_qt == 'NO' ? "checked" : ""}}>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="card-footer text-right">
					<button type="submit" class="btn btn-success">Guardar</button>
				</div>				
			</div>
				<!-- /.card-body -->
			</div>

			
		</form>
@stop

@section('js')
<script>
	$("body").addClass("sidebar-collapse");
	function getPerson(tx_rut){
		@if (!isset($user))
			$.getJSON("{{action('GetController@getDatosRut')}}?rut="+tx_rut,
			function(data){
				if(data.user && data.encontrado){
					var opcion = confirm("Este Usuario ya existe desea editarlo?");
					if (opcion == true) {
						location.href = data.id+"/edit";
					} else {
						$("#tx_rut").val("");
					}
				}
				else{
					if(data.encontrado == true){
						$("#tx_rut").val(data.tx_rut);
						$("#tx_nombre").val(data.tx_nombre);
						$("#tx_nombre").css('display','').attr('readonly', true);
					}else{
						alert("Rut no encontrado");
						$("#tx_rut").val("");
						$("#tx_nombre").val("");
						$("#tx_nombre").css('display','').attr('readonly', false);
					}
				}
			})
		@endif
	};

	function pdfEpicrisis(id, tipo){
		window.open('http://10.4.237.27/epicrisis-hsjd/api/epicrisis/pdf?token=5b7dbd9cf2d88&epicrisis_id='+id+'&tipo='+tipo,"Informe" , "width=800,height=600,fullscreen=yes,scrollbars=NO")
	}

	function pdfBiopsia(rut, key){
		window.open('../getBiopsia?rut='+rut+'&fila='+key,"Informe" , "width=800,height=600,fullscreen=yes,scrollbars=NO")
	}

	function pdfProtocolo(id){
		window.open('../getProtocolos?protocolo_id='+id,"Informe" , "width=800,height=600,fullscreen=yes,scrollbars=NO")
	}

	$('.select2').select2();
</script>
<script src="{{ asset('/vendors/ckeditor/ckeditor.js') }}"></script>
@stop