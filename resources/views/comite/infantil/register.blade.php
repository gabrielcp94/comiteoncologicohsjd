@extends('adminlte::page')

@section('title', 'Ingresar Paciente Infantil')

@section('content')
    <div class="card card-info">
        <div class="card-header">
            <h3 class="card-title">Ingresar Paciente Infantil</h3>
        </div>
        <form role="form" class="form-horizontal" id="form" method="POST" onSubmit="return validate();" action="{{action('ComiteInfantilController@store')}}" enctype="multipart/form-data">
			{{ csrf_field() }}
			<input type="hidden" id="nr_comite" name="nr_comite" value="{{isset($comite) && !isset($bo_clonar) ? $comite->nr_comite : ''}}"/>
			<input type="hidden" id="id_sexo" name="id_sexo" />
			<input type="hidden" id="id_prevision" name="id_prevision" />
			<input type="hidden" id="id_clasificacion_fonasa" name="id_clasificacion_fonasa" />
			<input type="hidden" id="bo_clonar" name="bo_clonar" value="{{isset($bo_clonar) ? '1' : '0'}}"/>
            <div class="card-body">
				<div class="form-group">
					<div class="row">
						<div class="col-sm-3">
							<label for="tx_medico_tratante">Medico Tratante:</label>
							<input class="form-control" value="{{Auth::user()->name}}" readonly>
						</div>
						<div class="col-sm-3">
							<label for="especialidad">Especialidad:</label>
							<input class="form-control" value="{{Auth::user()->especialidad->tx_descripcion}}" readonly>
						</div>
						<div class="col-sm-3">
							<label for="nr_hospital_origen">Hosp. Origen de Atención:<span style="color:#FF0000";>*</span></label>
							<select class="select2 select2-hidden-accessible" id="nr_hospital_origen" name="nr_hospital_origen" data-placeholder="Seleccione establecimiento" style="width: 100%;" data-select2-id="7" tabindex="-1" aria-hidden="true" required>
								<option value=""></option>
								@foreach ($establecimientos as $establecimiento)
									<option value={{$establecimiento->id}} {{Auth::user()->establecimiento->id == $establecimiento->id ? "selected" : ""}}>{{$establecimiento->tx_nombre_minuscula}}</option>
								@endforeach
							</select>
						</div>
						<div class="col-sm-3">
							<label for="fc_presentacion">Fecha de Presentación:<span style="color:#FF0000";>*</span></label>
							<div class="input-group">
								<div class="input-group-prepend">
									<span class="input-group-text"><i class="far fa-calendar-alt"></i></span>
								</div>
							<input type="date" class="form-control" id="fc_presentacion" name="fc_presentacion" min="{{!isset($comite->fc_presentacion) ? date("Y-m-d") : ''}}" value="{{isset($comite) ? $comite->fc_presentacion : ''}}" required>
							</div>						
						</div>
					</div>
                </div>
				<hr>
				<br>
                <h4>Datos Paciente</h4>
				<div class="form-group">
					<div class="row">
						<div class="col-sm-3">
							<label for="tx_rut">Rut:<span style="color:#FF0000";>*</span></label>
							<input class="form-control" id="tx_rut" name="tx_rut" onblur="getPerson($(this).val());" value="{{isset($comite) ? $comite->paciente->tx_rut : ''}}" required>
						</div>
						<div class="col-sm-3">
							<label for="tx_nombre">Nombre:<span style="color:#FF0000";>*</span></label>
							<input class="form-control" id="tx_nombre" name="tx_nombre" value="{{isset($comite) ? $comite->paciente->tx_nombre : ''}}" required>
						</div>
						<div class="col-sm-3">
							<label for="tx_apellido_paterno">Apellido Paterno:<span style="color:#FF0000";>*</span></label>
							<input class="form-control" id="tx_apellido_paterno" name="tx_apellido_paterno" value="{{isset($comite) ? $comite->paciente->tx_apellido_paterno : ''}}" required>
                        </div>
                        <div class="col-sm-3">
							<label for="tx_apellido_materno">Apellido Materno:</label>
							<input class="form-control" id="tx_apellido_materno" name="tx_apellido_materno" value="{{isset($comite) ? $comite->paciente->tx_apellido_materno : ''}}">
						</div>
					</div>
				</div>
				<div class="form-group">
					<div class="row">
						<div class="col-sm-3">
							<label for="fc_nacimiento">Fecha de Nacimiento:<span style="color:#FF0000";>*</span></label>
							<div class="input-group">
								<div class="input-group-prepend">
									<span class="input-group-text"><i class="far fa-calendar-alt"></i></span>
								</div>
								<input type="date" class="form-control" id="fc_nacimiento" name="fc_nacimiento" disabled value="{{isset($comite) ? $comite->paciente->fc_nacimiento : ''}}" required>
							</div>						
						</div>
						<div class="col-sm-3">
							<label for="tx_mail">Mail:</label>
							<input class="form-control" type="email" id="tx_mail" name="tx_mail" value="{{isset($comite) ? $comite->paciente->tx_mail : ''}}">
						</div>
						<div class="col-sm-3">
							<label for="tx_telefono">Teléfono: </label>
							<input class="form-control" type="number" maxlength="9" oninput="if(this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);" id="tx_telefono" name="tx_telefono" value="{{isset($comite->paciente->tx_telefono) ? $comite->paciente->tx_telefono : ''}}">
						</div>
						<div class="col-sm-3">
							<label for="nr_ficha">Ficha</label>
							<input class="form-control" type="number" maxlength="10" oninput="if(this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);" id="nr_ficha" name="nr_ficha" value="{{isset($comite->paciente->nr_ficha) ? $comite->paciente->nr_ficha : ''}}">
                        </div>
					</div>
				</div>
				<div class="col-md-12">
					<div class="card">
					<div class="card-header p-2">
						<ul class="nav nav-pills">
						<li class="nav-item"><a class="nav-link active" href="#antecedentesMorbidos" data-toggle="tab">Antecedentes Mórbidos</a></li>
						<li class="nav-item"><a class="nav-link" href="#historiaClinica" data-toggle="tab">Historia Clínica</a></li>
						<li class="nav-item"><a class="nav-link" href="#diagnosticoActual" data-toggle="tab">Diagnóstico Actual</a></li>
						<li class="nav-item"><a class="nav-link" href="#planPropuesto" data-toggle="tab">Plan Propuesto</a></li>
						</ul>
					</div>
					<div class="card-body">
						<div class="tab-content">
							<div class="tab-pane active" id="antecedentesMorbidos">
								<div class="form-group">
									<div class="row">
										<div class="col-sm-6">
											<label for="tx_antecedentes_personales">Antecedentes Personales</label>
											<textarea class="form-control noresize ckeditor" id="tx_antecedentes_personales" name="tx_antecedentes_personales">{{isset($comite->formulario->tx_antecedentes_personales) ? $comite->formulario->tx_antecedentes_personales : ""}}</textarea>
										</div>
										<div class="col-sm-6">
											<label for="tx_antecedentes_familiares">Antecedentes Familiares</label>
											<textarea class="form-control noresize ckeditor" id="tx_antecedentes_familiares" name="tx_antecedentes_familiares">{{isset($comite->formulario->tx_antecedentes_familiares) ? $comite->formulario->tx_antecedentes_familiares : ""}}</textarea>
										</div>
									</div>
								</div>
							</div>
							<div class="tab-pane" id="historiaClinica">
								<div class="form-group">
									<div class="row">
										<div class="col-sm-6">
											<label for="tx_historia">Historia Clinica</label>
											<textarea class="form-control noresize ckeditor" id="tx_historia" name="tx_historia">{{isset($comite->formulario->tx_historia) ? $comite->formulario->tx_historia : ""}}</textarea>
										</div>
										<div class="col-sm-6">
											<label for="tx_resultado_laboratorio">Laboratorio</label>
											<textarea class="form-control noresize ckeditor" id="tx_resultado_laboratorio" name="tx_resultado_laboratorio">{{isset($comite->formulario->tx_resultado_laboratorio) ? $comite->formulario->tx_resultado_laboratorio : ""}}</textarea>
										</div>
									</div>
								</div>
							</div>
							<div class="tab-pane" id="diagnosticoActual">
								<div class="form-group">
									<div class="row">
										<div class="col-sm-6">
											<label for="tx_diagnostico_actual">Diagnóstico Actual</label>
											<textarea class="form-control noresize ckeditor" id="tx_diagnostico_actual" name="tx_diagnostico_actual">{{isset($comite->formulario->tx_diagnostico_actual) ? $comite->formulario->tx_diagnostico_actual : ""}}</textarea>
										</div>
										<div class="col-sm-6">
											<label for="tx_examen">Exámenes Diagnóstico</label>
											<textarea class="form-control noresize ckeditor" id="tx_examen" name="tx_examen">{{isset($comite->formulario->tx_examen) ? $comite->formulario->tx_examen : ""}}</textarea>
										</div>
									</div>
								</div>
							</div>
							<div class="tab-pane" id="planPropuesto">
								<div class="form-group">
									<div class="row">
										<div class="col-sm-12">
											<label for="tx_plan">Plan</label>
											<textarea class="form-control noresize ckeditor" id="tx_plan" name="tx_plan">{{isset($comite->formulario->tx_plan) ? $comite->formulario->tx_plan : ""}}</textarea>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					</div>
				</div>
			</div>
		  	<div class="card-footer text-right">
				<button type="submit" class="btn btn-success" onclick="guardar()">Guardar</button>
		  	</div>
        </form>
        

    </div>
@stop

@section('js')
<script>
	$('.select2').select2();
	var url_diagnosticos = @json(url('getDiagnostico'));

	function validate() {
		var resultado_validacion=true;
		$("#editor_error").html("");
		var editor_val = CKEDITOR.instances.tx_plan.document.getBody().getChild(0).getText().trim();
		if(!(editor_val.length>0)){
			alert('Ingrese Plan');
			resultado_validacion=false;
		}       

		return resultado_validacion;
	}

	function guardar(){
		$("#fc_nacimiento").css('display','').attr('disabled', false);
		@if(request()->adulto == 1)
			$("#bo_antecedentes_familiares").css('display','').attr('required', true);
			$("#bo_cancer_previo").css('display','').attr('required', true);
			$("#bo_tabaco").css('display','').attr('required', true);
			$("#bo_oh").css('display','').attr('required', true);
			$("#tx_Cie10").css('display','').attr('required', true);
		@elseif(request()->adulto == 2)
			$("#bo_antecedentes_familiares").css('display','').attr('required', true);
			$("#bo_cancer_previo").css('display','').attr('required', true);
			$("#bo_tabaco").css('display','').attr('required', true);
			$("#bo_oh").css('display','').attr('required', true);
		@endif
	}

	function getPerson(rut){
		@if (!isset($user))
			$.getJSON("{{action('GetController@getDatosRut')}}?rut="+rut,
			function(data){
				if(data.encontrado == true){
					$("#tx_rut").val(data.rut);
					$("#nr_run").val(data.nr_run);
					$("#tx_digito_verificador").val(data.tx_digito_verificador);
					$("#id_sexo").val(data.id_sexo);
					$("#id_prevision").val(data.id_prevision);
					$("#id_clasificacion_fonasa").val(data.id_clasificacion_fonasa);
					$("#tx_nombre").val(data.tx_nombre_solo);
					$("#tx_nombre").css('display','').attr('readonly', true);
					$("#tx_apellido_paterno").val(data.tx_apellido_paterno);
					$("#tx_apellido_paterno").css('display','').attr('readonly', true);
					$("#tx_apellido_materno").val(data.tx_apellido_materno);
					$("#tx_apellido_materno").css('display','').attr('readonly', true);
					$("#fc_nacimiento").val(data.fc_nacimiento);
					$("#fc_nacimiento").css('display','').attr('disabled', true);
					$("#tx_mail").val(data.tx_mail);
					$("#tx_telefono").val(data.tx_telefono);
					$("#nr_ficha").val(data.nr_ficha);
				}else{
					alert("Rut no encontrado");
					$("#tx_rut").val("");
					$("#tx_nombre").val("");
					$("#tx_nombre").css('display','').attr('readonly', false);
					$("#tx_apellido_paterno").val("");
					$("#tx_apellido_paterno").css('display','').attr('readonly', false);
					$("#tx_apellido_materno").val("");
					$("#tx_apellido_materno").css('display','').attr('readonly', false);
				}
			})
		@endif
	};

	$('.select_diagnosticos').select2({
        width: '99%',
        allowClear: true,
        minimumInputLength: 3,
        placeholder: "Seleccione CIE10",
        language: {
            inputTooShort: function() {
                return 'Ingrese 3 o más caracteres para la búsqueda';
            }
        },
        ajax: {
            url: url_diagnosticos,
            dataType: 'json',
            data: function (params) {
                var query = {
                    search: params.term,
                    type: 'public'
                }
                return query;
            }
        }
    })

	@if (isset($comite))
		var comite = @json($comite);
	@endif
</script>
<script src="{{ asset('/vendors/ckeditor/ckeditor.js') }}"></script>
@stop