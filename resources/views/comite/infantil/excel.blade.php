<table>
    <tr>
        <th>ID</th>
        <th>Nombre</th>
        <th>Rut</th>
        <th>Edad</th>
        <th>F. Nacimiento</th>
        <th>Sexo</th>
        <th>Paciente Nuevo</th>
        <th>F. Presentación</th>
        <th>Médico</th>
        <th>Especialidad</th>
        <th>Diagnóstico</th>
    </tr>
    @foreach($comites as $comite)
    <tr>
        <td>{{$comite->id}}</td>
        <td>{{$comite->paciente->tx_nombre_completo}}</td>
        <td>{{$comite->paciente->tx_rut}}</td>
        <td>{{$comite->paciente->edad}}</td>
        <td>@if((isset($comite->paciente->fc_nacimiento) && $comite->paciente->fc_nacimiento != '0000-00-00'))
            {{date('d/m/Y', strtotime(str_replace("/",".",$comite->paciente->fc_nacimiento)))}}
        @else 
            Sin Información
        @endif</td>
        <td>@if($comite->paciente->id_sexo == 1)
                Hombre
            @elseif($comite->paciente->id_sexo == 2)
                Mujer
            @endif
        </td>
        <td>{{$comite->tx_paciente_nuevo}}</td>
        <td>{{date('d-m-Y', strtotime($comite->fc_presentacion))}}</td>
        <td>{{$comite->medico->tx_nombre_minuscula}}</td>
        <td>{{ucwords(mb_strtolower($comite->medico->especialidad->tx_nombre_minuscula))}}</td>
        <td>{!!$comite->formulario->tx_diagnostico_actual!!}</td>
    </tr>
    @endforeach
</table>