@extends('adminlte::page')

@section('title', 'Resolver Comite')

@section('content')
		{{-- <form class="form-horizontal" id="form" method="POST">
			{{ csrf_field() }} --}}
		<!-- /.card-header -->
		<!-- form start -->
		<form role="form" class="form-horizontal" id="form" method="POST" action="{{action('ResolucionInfantilController@store')}}">
			{{ csrf_field() }}
			<input type="hidden" id="id" name="id" value="{{request()->id}}"/>
			<input type="hidden" id="tx_rut" name="tx_rut" value="{{$comite->tx_rut}}"/>
			<input type="hidden" id="fc_resolucion" name="fc_resolucion" value="{{$comite->fc_ingreso}}"/>

			<div class="card bg-gradient sticky-top card-info">
				<button type="button" class="btn btn btn-sm card-info" data-card-widget="collapse">
					<div class="card-header border-0 ui-sortable-handle">
					<h3 class="card-title">
						<i class="fa fa-user"></i>
						Datos Generales. (<strong>N° Comité:</strong> {{$comite->nr_comite}})
					</h3>
					<!-- tools card -->
					<div class="card-tools">
						<!-- button with a dropdown -->
						<i class="fas fa-minus"></i>
					</div>
					<!-- /. tools -->
					</div>
				</button>
				<!-- /.card-header -->
				<div class="card-body pt-0">
					<!--The calendar -->
					<div class="row invoice-info">
						<div class="col-sm-4 invoice-col">
							<strong>Médico Tratante: </strong>{{$comite->medico->tx_nombre_minuscula}}<br><br>
							<strong>Rut: </strong>{{$comite->paciente->tx_rut}}<br>
							<strong>Ficha: </strong>{{$comite->paciente->nr_ficha}}<br>
						</div>
						<div class="col-sm-4 invoice-col">
							<strong>Especialidad: </strong>{{$comite->medico->especialidad->tx_nombre_minuscula}}<br><br>
							<strong>Nombre: </strong>{{$comite->paciente->tx_nombre_completo}}<br>
						</div>
						<div class="col-sm-4 invoice-col">
							<strong>{{isset($comite->establecimiento->tx_nombre) ? 'Hospital Origen:' : ''}} </strong>{{isset($comite->establecimiento->tx_nombre) ? $comite->establecimiento->tx_nombre : ''}}<br><br>
							<strong>F. Nacimiento: </strong>{{date('d-m-Y', strtotime($comite->paciente->fc_nacimiento))}} ({{$comite->paciente->edad}})<br>
							<strong>F. Presentación: </strong>{{date('d-m-Y', strtotime($comite->fc_presentacion))}}<br>
						</div>
					</div>
				</div>
				<!-- /.card-body -->
			</div>

			<div class="card bg-gradient collapsed-card">
				<button type="button" class="btn btn btn-sm card-info" data-card-widget="collapse">
					<div class="card-header border-0 ui-sortable-handle">
						<h3 class="card-title">
							<i class="fa fa-book-medical"></i>
							Antecedentes Mórbidos
						</h3>
						<div class="card-tools">
							<i class="fas fa-plus"></i>
						</div>
					<!-- /. tools -->
					</div>
				</button>
				<!-- /.card-header -->
				<div class="card-body pt-0">
					<div class="row invoice-info">
						<div class="col-sm-6 invoice-col">
							<strong>Antecedentes Personales: </strong>{!!$comite->formulario->tx_antecedentes_personales!!}<br>
						</div>
						<div class="col-sm-6 invoice-col">
							<strong>Antecedentes Familiares: </strong>{!!$comite->formulario->tx_antecedentes_familiares!!}<br>
						</div>
					</div>
				</div>
			</div>
			<div class="card bg-gradient collapsed-card">
				<button type="button" class="btn btn btn-sm card-info" data-card-widget="collapse">
					<div class="card-header border-0 ui-sortable-handle">
						<h3 class="card-title">
							<i class="fa fa-book-medical"></i>
							Historia Clínica
						</h3>
						<div class="card-tools">
							<i class="fas fa-plus"></i>
						</div>
					<!-- /. tools -->
					</div>
				</button>
				<!-- /.card-header -->
				<div class="card-body pt-0">
					<div class="row invoice-info">
						<div class="col-sm-6 invoice-col">
							<strong>Historia Clínica: </strong>{!!$comite->formulario->tx_historia!!}<br>
						</div>
						<div class="col-sm-6 invoice-col">
							<strong>Laboratorio: </strong>{!!$comite->formulario->tx_resultado_laboratorio!!}<br>
						</div>
					</div>
				</div>
			</div>
			<div class="card bg-gradient collapsed-card">
				<button type="button" class="btn btn btn-sm card-info" data-card-widget="collapse">
					<div class="card-header border-0 ui-sortable-handle">
						<h3 class="card-title">
							<i class="fa fa-book-medical"></i>
							Diagnóstico
						</h3>
						<div class="card-tools">
							<i class="fas fa-plus"></i>
						</div>
					<!-- /. tools -->
					</div>
				</button>
				<!-- /.card-header -->
				<div class="card-body pt-0">
					<div class="row invoice-info">
						<div class="col-sm-6 invoice-col">
							<strong>Diagnóstico Actual: </strong>{!!$comite->formulario->tx_diagnostico_actual!!}<br>
						</div>
						<div class="col-sm-6 invoice-col">
							<strong>Exámenes Diagnóstico: </strong>{!!$comite->formulario->tx_examen!!}<br>
						</div>
					</div>
				</div>
			</div>
			<div class="card bg-gradient collapsed-card">
				<button type="button" class="btn btn btn-sm card-info" data-card-widget="collapse">
					<div class="card-header border-0 ui-sortable-handle">
						<h3 class="card-title">
							<i class="fa fa-book-medical"></i>
							Plan
						</h3>
						<div class="card-tools">
							<i class="fas fa-plus"></i>
						</div>
					<!-- /. tools -->
					</div>
				</button>
				<!-- /.card-header -->
				<div class="card-body pt-0">
					<div class="row invoice-info">
						<div class="col-sm-12 invoice-col">
							<strong>Plan: </strong>{!!$comite->formulario->tx_plan!!}<br>
						</div>
					</div>
				</div>
			</div>

			<div class="card bg-gradient collapsed-card">
				<button type="button" class="btn btn btn-sm card-info" data-card-widget="collapse">
					<div class="card-header border-0 ui-sortable-handle">
						<h3 class="card-title">
							<i class="fa fa-book-medical"></i>
							{{(isset($arr_epicrises) && count($arr_epicrises) > 0) || (isset($informes) && count($informes) > 0 && !empty($informes[0])) || (isset($solicitudesPabellon) && count($solicitudesPabellon) > 0)   ? 'Reportes Asociados' : 'NO EXISTEN REPORTES ASOCIADOS!'}}
						</h3>
						<div class="card-tools">
							<i class="fas fa-plus"></i>
						</div>
					<!-- /. tools -->
					</div>
				</button>
				<!-- /.card-header -->
				<div class="card-body pt-0">
					<div class="col-12 table-responsive">
						@if (isset($arr_epicrises) && count($arr_epicrises) > 0)
							<strong>Epicrisis</strong>
							<table class="table table-striped">
								<thead style="background-color: lightblue">
									<tr>
										<th>Codigo</th>
										<th>Tipo</th>
										<th>Medico</th>
										<th>F. Ingreso</th>
										<th>F. Egreso</th>
										<th><i class="fas fa-cog"></i></th>
									</tr>
								</thead>
								<tbody>
									@foreach ($arr_epicrises as $arr_epicrisis)
										<tr>
											<td>{{$arr_epicrisis['pac_corr']}}</td>
											<td>{{$arr_epicrisis['tipo']}}</td>
											<td>{{$arr_epicrisis['medico']['name']}}</td>
											<td>{{date('d-m-Y', strtotime($arr_epicrisis['fecha_ingreso']))}}</td>
											<td>{{date('d-m-Y', strtotime($arr_epicrisis['fecha_egreso']))}}</td>
											<td>
												<a class="btn btn-danger btn-xs" onclick="pdfEpicrisis({{$arr_epicrisis['id']}},'{{$arr_epicrisis['tipo']}}');" href="http://10.4.237.27/epicrisis-hsjd/api/epicrisis/pdf?token=5b7dbd9cf2d88&epicrisis_id={{$arr_epicrisis['id']}}&tipo={{$arr_epicrisis['tipo']}}" title="Archivo" target="_blank"><i class="fa fa-file-pdf" style="color:white"></i></a>
											</td>
										</tr>
									@endforeach
								</tbody>
							</table>
						@endif
					</div>
					<div class="col-12 table-responsive">
						@if (isset($informes) && count($informes) > 0 && !empty($informes[0]))
							<strong>Biopsia</strong>
							<table class="table table-striped">
								<thead style="background-color: lightcyan">
									<tr>
										<th>Tipo Estudio</th>
										<th>Código Estudio</th>
										<th>Servicio Solicitante</th>
										<th>F. Toma</th>
										<th>F. Cierre</th>
										<th><i class="fas fa-cog"></i></th>
									</tr>
								</thead>
								<tbody>
									
									@foreach ($informes as $key => $informe)
										<tr>
										<td>{{$informe->tipoEstudio}}</td>
											<td>{{$informe->codigoEstudio}}</td>
											<td>{{$informe->servicio_solitante}}</td>
											<td>{{date('d-m-Y', strtotime($informe->fecha_toma_muestra))}}</td>
											<td>{{date('d-m-Y', strtotime($informe->fechaCierre))}}</td>
											<td>
												<a class="btn btn-danger btn-xs" onclick="pdfBiopsia('{{isset($comite->paciente) ? $comite->paciente->tx_rut : $comite->tx_rut}}',{{$key}});" href="/getBiopsia?rut={{isset($comite->paciente) ? $comite->paciente->tx_rut : $comite->tx_rut}}&fila={{$key}}" title="Archivo" target="_blank"><i class="fa fa-file-pdf" style="color:white"></i></a>
											</td>
										</tr>
									@endforeach
								</tbody>
							</table>
						@endif
					</div>
					<div class="col-12 table-responsive">
						@if (isset($solicitudesPabellon) && count($solicitudesPabellon) > 0)
							<strong>Protocolo</strong>
							<table class="table table-striped">
								<thead style="background-color: lightyellow">
									<tr>
										<th>Fecha Protocolo</th>
										<th>Diagnóstico Pre-Operatorio</th>
										<th>Cirugía Practictada</th>
										<th><i class="fas fa-cog"></i></th>
									</tr>
								</thead>
								<tbody>
									@foreach ($solicitudesPabellon as $solicitudPabellon)
										<tr>
											<td>{{date('d-m-Y', strtotime($solicitudPabellon->protocolo->fc_entrada_pabellon))}}</td>
											<td>{{$solicitudPabellon->protocolo->gl_diagnostico_pre}}</td>
											<td>{{$solicitudPabellon->protocolo->gl_cirugia_practicada}}</td>
											<td>
												<a class="btn btn-danger btn-xs" onclick="pdfProtocolo({{$solicitudPabellon->protocolo->id}});" href="/getProtocolos?protocolo_id={{$solicitudPabellon->protocolo->id}}" title="Archivo" target="_blank"><i class="fa fa-file-pdf" style="color:white"></i></a>
											</td>
										</tr>
									@endforeach
								</tbody>
							</table>
						@endif
					</div>
				</div>
			</div>

			<div class="card bg-gradient collapsed-card">
				<button type="button" class="btn btn btn-sm card-info" data-card-widget="collapse">
					<div class="card-header border-0 ui-sortable-handle">
						<h3 class="card-title">
							<i class="fa fa-book-medical"></i>
							Resolver
						</h3>
						<div class="card-tools">
							<i class="fas fa-plus"></i>
						</div>
					<!-- /. tools -->
					</div>
				</button>
				<!-- /.card-header -->
				<div class="card-body pt-0">
				<!--The calendar -->
				<div class="card-body">
					<div class="form-group">
						<div class="row">
							<div class="col-sm-3">
								<label for="fc_lista">Fecha de Comité:<span style="color:#FF0000";>*</span></label>
								<div class="input-group">
									<div class="input-group-prepend">
										<span class="input-group-text"><i class="far fa-calendar-alt"></i></span>
									</div>
								<input type="date" class="form-control" id="fc_lista" name="fc_lista" value="{{isset($comite->fc_presentacion) ? $comite->fc_presentacion : ''}}" readonly>
								</div>						
							</div>
							<div class="col-sm-4">
								<label for="tx_estado">Estado<span style="color:#FF0000";>*</span></label>
								<div class="row">
									<div class="col-sm-5">
										<label>RESUELTO</label>
									</div>
									<div class="col-sm-1">
										<input class="form-check-input" type="radio" id="tx_estado" name="tx_estado" value="73" {{isset($comite->tx_estado) && $comite->tx_estado == 'RESUELTO' ? "checked" : ""}} required>
									</div>
									<div class="col-sm-5">
										<label>REPRESENTAR</label>
									</div>
									<div class="col-sm-1">
										<input class="form-check-input" type="radio" id="tx_estado" name="tx_estado" value="72" {{isset($comite->tx_estado) && $comite->tx_estado == 'REPRESENTAR' ? "checked" : ""}}>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="form-group">
						<div class="row">
							<div class="col-sm-12">
								<label for="tx_tratamiento">Tratamiento Comité<span style="color:#FF0000";>*</span></label>
								<textarea class="form-control noresize ckeditor" id="tx_tratamiento" name="tx_tratamiento" required>{{isset($comite->tx_tratamiento) ? $comite->tx_tratamiento : ""}}</textarea>
							</div>
						</div>
					</div>
					<div class="form-group">
						<div class="row">
							<div class="col-sm-6">
								<label for="id_integrantes_fecha">Integrantes:</label>
								<select class="select2 select2-hidden-accessible" id="id_integrantes_fecha" name="id_integrantes_fecha" data-placeholder="Seleccione integrantes" style="width: 100%;" data-select2-id="7" tabindex="-1" aria-hidden="true" required>
									<option value=""></option>
									@foreach ($integrantes as $integrante)
										<option value={{$integrante->id}} selected>{{isset($integrante->fc_lista) ? date('d-m-Y', strtotime($integrante->fc_lista)) : ''}} {{$integrante->tx_integrantes}}</option>
									@endforeach
								</select>
							</div>
						</div>
					</div>
				</div>
				<div class="card-footer text-right">
					<button type="submit" class="btn btn-success">Guardar</button>
				</div>				
			</div>
				<!-- /.card-body -->
			</div>

			
		</form>
@stop

@section('js')
<script>
	$("body").addClass("sidebar-collapse");
	function getPerson(tx_rut){
		@if (!isset($user))
			$.getJSON("{{action('GetController@getDatosRut')}}?rut="+tx_rut,
			function(data){
				if(data.user && data.encontrado){
					var opcion = confirm("Este Usuario ya existe desea editarlo?");
					if (opcion == true) {
						location.href = data.id+"/edit";
					} else {
						$("#tx_rut").val("");
					}
				}
				else{
					if(data.encontrado == true){
						$("#tx_rut").val(data.tx_rut);
						$("#tx_nombre").val(data.tx_nombre);
						$("#tx_nombre").css('display','').attr('readonly', true);
					}else{
						alert("Rut no encontrado");
						$("#tx_rut").val("");
						$("#tx_nombre").val("");
						$("#tx_nombre").css('display','').attr('readonly', false);
					}
				}
			})
		@endif
	};

	function pdfEpicrisis(id, tipo){
		window.open('http://10.4.237.27/epicrisis-hsjd/api/epicrisis/pdf?token=5b7dbd9cf2d88&epicrisis_id='+id+'&tipo='+tipo,"Informe" , "width=800,height=600,fullscreen=yes,scrollbars=NO")
	}

	function pdfBiopsia(rut, key){
		window.open('../getBiopsia?rut='+rut+'&fila='+key,"Informe" , "width=800,height=600,fullscreen=yes,scrollbars=NO")
	}

	function pdfProtocolo(id){
		window.open('../getProtocolos?protocolo_id='+id,"Informe" , "width=800,height=600,fullscreen=yes,scrollbars=NO")
	}

	$('.select2').select2();
</script>
<script src="{{ asset('/vendors/ckeditor/ckeditor.js') }}"></script>
@stop