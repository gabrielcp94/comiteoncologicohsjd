<div class="card bg-gradient collapsed-card">
    <button type="button" class="btn btn btn-sm card-info" data-card-widget="collapse">
        <div class="card-header border-0 ui-sortable-handle">
            <h3 class="card-title">
                <i class="fa fa-book-medical"></i>
                Antecedentes Mórbidos
            </h3>
            <div class="card-tools">
                <i class="fas fa-plus"></i>
            </div>
        <!-- /. tools -->
        </div>
    </button>
    <!-- /.card-header -->
    <div class="card-body pt-0">
        <div class="row invoice-info">
            <div class="col-sm-3 invoice-col">
                <strong>Antecedentes Familiares: </strong>{{$comite->formulario->bo_antecedentes_familiares == 1 ? 'SI' : 'NO'}}<br>
                <strong>Tabaco : </strong>{{$comite->formulario->bo_tabaco == 1 ? 'SI' : 'NO'}}<br>
            </div>
            <div class="col-sm-3 invoice-col">
                <strong>Cáncer Previo: </strong>{{$comite->formulario->bo_cancer_previo == 1 ? 'SI' : 'NO'}}<br>
                <strong>OH: </strong>{{$comite->formulario->bo_oh == 1 ? 'SI' : 'NO'}}<br>
                <strong>Revisar Imagen: </strong>{{$comite->formulario->bo_revisar_imagen == 1 ? 'SI' : 'NO'}}<br>
            </div>
            <div class="col-sm-6 invoice-col">
                <strong>Tipo Cáncer Previo: </strong>{{isset($comite->formulario->tx_tipo_cancer) ? $comite->formulario->tx_tipo_cancer : ''}}<br>
                <strong>Otro Antecedente: </strong>{{isset($comite->formulario->tx_otro_antecedente) ? $comite->formulario->tx_otro_antecedente : ''}}<br>
                <strong>Comorbilidad: </strong>{{$comite->comorbilidades->pluck('tx_nombre')->implode(', ')}}<br>
            </div>
        </div>
    </div>
</div>

<div class="card bg-gradient collapsed-card">
    <button type="button" class="btn btn btn-sm card-info" data-card-widget="collapse">
        <div class="card-header border-0 ui-sortable-handle">
            <h3 class="card-title">
                <i class="fa fa-book-medical"></i>
                Historia Clínica
            </h3>
            <div class="card-tools">
                <i class="fas fa-plus"></i>
            </div>
        <!-- /. tools -->
        </div>
    </button>
    <!-- /.card-header -->
    <div class="card-body pt-0">
        <div class="row invoice-info">
            <div class="col-sm-6 invoice-col">
                <strong>Historia Clínica: </strong>{!!$comite->formulario->tx_historia_clinica!!}<br>
                <strong>FX Pulmonar: </strong>{{isset($comite->formulario->nr_fx_pulmonar) ? $comite->formulario->nr_fx_pulmonar : ''}}<br>
                <strong>VEF1: </strong>{{isset($comite->formulario->nr_vef1) ? $comite->formulario->nr_vef1 : ''}}<br>
                <strong>TIFFENAU: </strong>{{isset($comite->formulario->nr_tiffenau) ? $comite->formulario->nr_tiffenau : ''}}<br>
            </div>
            <div class="col-sm-6 invoice-col">
                <strong>Resultado Imagen: </strong>{!!$comite->formulario->tx_resultado_imagen!!}<br>
                <strong>IPA: </strong>{{isset($comite->formulario->nr_ipa) ? $comite->formulario->nr_ipa : ''}}<br>
                <strong>CVF: </strong>{{isset($comite->formulario->nr_cvf) ? $comite->formulario->nr_cvf : ''}}<br>
                <strong>DLCO: </strong>{{isset($comite->formulario->nr_dlco) ? $comite->formulario->nr_dlco : ''}}<br>
            </div>
        </div>
    </div>
</div>

<div class="card bg-gradient collapsed-card">
    <button type="button" class="btn btn btn-sm card-info" data-card-widget="collapse">
        <div class="card-header border-0 ui-sortable-handle">
            <h3 class="card-title">
                <i class="fa fa-book-medical"></i>
                Diagnóstico
            </h3>
            <div class="card-tools">
                <i class="fas fa-plus"></i>
            </div>
        <!-- /. tools -->
        </div>
    </button>
    <!-- /.card-header -->
    <div class="card-body pt-0">
        <div class="row invoice-info">
            <div class="col-sm-4 invoice-col">
                <strong>Diagnóstico Actual: </strong>{!!$comite->formulario->tx_diagnostico_actual!!}<br>
                <strong>ECOG: </strong>{{isset($comite->formulario->tx_ecog) ? $comite->formulario->tx_ecog : ''}}<br>
            </div>
            <div class="col-sm-4 invoice-col">
                <strong>Biopsia Pre Operatoria: </strong>{!!isset($comite->formulario->tx_biopsia_pre_op) ? $comite->formulario->tx_biopsia_pre_op : ''!!}<br>
                <strong>Cie-O: </strong>{{$comite->cie10->tx_nombre_minuscula}}<br>
            </div>
            <div class="col-sm-4 invoice-col">
                <strong>Broncoscopia: </strong>{!!isset($comite->formulario->tx_broncoscopia) ? $comite->formulario->tx_broncoscopia : ''!!}<br>
            </div>
        </div>
    </div>
</div>

<div class="card bg-gradient collapsed-card">
    <button type="button" class="btn btn btn-sm card-info" data-card-widget="collapse">
        <div class="card-header border-0 ui-sortable-handle">
            <h3 class="card-title">
                <i class="fa fa-book-medical"></i>
                Plan
            </h3>
            <div class="card-tools">
                <i class="fas fa-plus"></i>
            </div>
        <!-- /. tools -->
        </div>
    </button>
    <!-- /.card-header -->
    <div class="card-body pt-0">
        <div class="row invoice-info">
            <div class="col-sm-12 invoice-col">
                <strong>Plan: </strong>{!!$comite->formulario->tx_plan!!}<br>
            </div>
        </div>
    </div>
</div>

@include('comite.resolucion.reportesAsociados')

<div class="card bg-gradient collapsed-card">
    <button type="button" class="btn btn btn-sm card-info" data-card-widget="collapse">
        <div class="card-header border-0 ui-sortable-handle">
            <h3 class="card-title">
                <i class="fa fa-book-medical"></i>
                Resolver
            </h3>
            <div class="card-tools">
                <i class="fas fa-plus"></i>
            </div>
        <!-- /. tools -->
        </div>
    </button>
    <!-- /.card-header -->
    <div class="card-body pt-0">
    <!--The calendar -->
        <div class="card-body">
            <div class="form-group">
                <div class="row">
                    <div class="col-sm-3">
                        <label for="bo_curativo">Curativo</label>
                        <div class="row">
                            <div class="col-sm-3">
                                <label>Sí</label>
                            </div>
                            <div class="col-sm-1">
                                <input class="form-check-input" type="radio" id="bo_curativo" name="bo_curativo" value="1" {{isset($resolucion->bo_curativo) && $resolucion->bo_curativo == 1 ? "checked" : ""}}>
                            </div>
                            <div class="col-sm-3">
                                <label>No</label>
                            </div>
                            <div class="col-sm-1">
                                <input class="form-check-input" type="radio" id="bo_curativo" name="bo_curativo" value="0" {{isset($resolucion->bo_curativo) && $resolucion->bo_curativo == 0 ? "checked" : ""}}>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <label for="bo_fbc">FBC</label>
                        <div class="row">
                            <div class="col-sm-3">
                                <label>Sí</label>
                            </div>
                            <div class="col-sm-1">
                                <input class="form-check-input" type="radio" id="bo_fbc" name="bo_fbc" value="1" {{isset($resolucion->bo_fbc) && $resolucion->bo_fbc == 1 ? "checked" : ""}}>
                            </div>
                            <div class="col-sm-3">
                                <label>No</label>
                            </div>
                            <div class="col-sm-1">
                                <input class="form-check-input" type="radio" id="bo_fbc" name="bo_fbc" value="0" {{isset($resolucion->bo_fbc) && $resolucion->bo_fbc == 0 ? "checked" : ""}}>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <label for="bo_pet_ct">PET/CT</label>
                        <div class="row">
                            <div class="col-sm-3">
                                <label>Sí</label>
                            </div>
                            <div class="col-sm-1">
                                <input class="form-check-input" type="radio" id="bo_pet_ct" name="bo_pet_ct" value="1" {{isset($resolucion->bo_pet_ct) && $resolucion->bo_pet_ct == 1 ? "checked" : ""}}>
                            </div>
                            <div class="col-sm-3">
                                <label>No</label>
                            </div>
                            <div class="col-sm-1">
                                <input class="form-check-input" type="radio" id="bo_pet_ct" name="bo_pet_ct" value="0" {{isset($resolucion->bo_pet_ct) && $resolucion->bo_pet_ct == 0 ? "checked" : ""}}>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <label for="bo_ebus">EBUS</label>
                        <div class="row">
                            <div class="col-sm-3">
                                <label>Sí</label>
                            </div>
                            <div class="col-sm-1">
                                <input class="form-check-input" type="radio" id="bo_ebus" name="bo_ebus" value="1" {{isset($resolucion->bo_ebus) && $resolucion->bo_ebus == 1 ? "checked" : ""}}>
                            </div>
                            <div class="col-sm-3">
                                <label>No</label>
                            </div>
                            <div class="col-sm-1">
                                <input class="form-check-input" type="radio" id="bo_ebus" name="bo_ebus" value="0" {{isset($resolucion->bo_ebus) && $resolucion->bo_ebus == 0 ? "checked" : ""}}>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <div class="row">
                    <div class="col-sm-12">
                        <label for="tx_tratamiento_comite">Tratamiento Comité</label>
                        <textarea class="form-control noresize ckeditor" id="tx_tratamiento_comite" name="tx_tratamiento_comite">{{isset($resolucion->tx_tratamiento_comite) ? $resolucion->tx_tratamiento_comite : ""}}</textarea>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <div class="row">
                    <div class="col-sm-3">
                        <label for="tratamientos">Tratamiento Propuesto:</label>
                        <select class="select2 select2-hidden-accessible" id="tratamientos" name="tratamientos[]" multiple data-placeholder="Seleccione tratamientos" style="width: 100%;" data-select2-id="7" tabindex="-1" aria-hidden="true">
                            <option value=""></option>
                            @foreach ($tratamientos as $tratamiento)
                                <option value={{$tratamiento->id}} {{in_array($tratamiento->id, $comite->tratamientos->pluck('id')->toArray()) ? 'selected' : ''}}>{{$tratamiento->tx_nombre}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="col-sm-5">
                        <label for="integrantes">Integrantes:</label>
                        <select class="select2 select2-hidden-accessible" id="integrantes" name="integrantes[]" data-placeholder="Seleccione integrantes" style="width: 100%;" data-select2-id="7" tabindex="-1" aria-hidden="true" multiple required>
                            <option value=""></option>
                            @foreach ($medicos as $medico)
                                <option value={{$medico->id}} {{in_array($medico->id, $integrantes) ? 'selected' : ''}}>{{$medico->tx_nombre_minuscula}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="col-sm-4">
                        <label for="bo_resuelto">Estado<span style="color:#FF0000";>*</span></label>
                        <div class="row">
                            <div class="col-sm-5">
                                <label>RESUELTO</label>
                            </div>
                            <div class="col-sm-1">
                                <input class="form-check-input" type="radio" id="bo_resuelto" name="bo_resuelto" value=72 {{isset($comite->bo_resuelto) && $comite->bo_resuelto == 72 ? "checked" : ""}} required>
                            </div>
                            <div class="col-sm-5">
                                <label>REPRESENTAR</label>
                            </div>
                            <div class="col-sm-1">
                                <input class="form-check-input" type="radio" id="bo_resuelto" name="bo_resuelto" value=73 {{isset($comite->bo_resuelto) && $comite->bo_resuelto == 73 ? "checked" : ""}}>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="card-footer text-right">
            <button type="submit" class="btn btn-success">Guardar</button>
        </div>				
    </div>
    <!-- /.card-body -->
</div>