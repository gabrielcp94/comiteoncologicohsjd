<div class="card bg-gradient collapsed-card">
    <button type="button" class="btn btn btn-sm card-info" data-card-widget="collapse">
        <div class="card-header border-0 ui-sortable-handle">
            <h3 class="card-title">
                <i class="fa fa-book-medical"></i>
                {{(isset($arr_epicrises) && count($arr_epicrises) > 0) || (isset($informes) && count($informes) > 0 && !empty($informes[0])) || (isset($solicitudesPabellon) && count($solicitudesPabellon) > 0)   ? 'Reportes Asociados' : 'NO EXISTEN REPORTES ASOCIADOS!'}}
            </h3>
            <div class="card-tools">
                <i class="fas fa-plus"></i>
            </div>
        <!-- /. tools -->
        </div>
    </button>
    <!-- /.card-header -->
    <div class="card-body pt-0">
        <div class="col-12 table-responsive">
            @if (isset($arr_epicrises) && count($arr_epicrises) > 0)
                <strong>Epicrisis</strong>
                <table class="table table-striped">
                    <thead style="background-color: lightblue">
                        <tr>
                            <th>Codigo</th>
                            <th>Tipo</th>
                            <th>Medico</th>
                            <th>F. Ingreso</th>
                            <th>F. Egreso</th>
                            <th><i class="fas fa-cog"></i></th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($arr_epicrises as $arr_epicrisis)
                            <tr>
                                <td>{{$arr_epicrisis['pac_corr']}}</td>
                                <td>{{$arr_epicrisis['tipo']}}</td>
                                <td>{{$arr_epicrisis['medico']['name']}}</td>
                                <td>{{date('d-m-Y', strtotime($arr_epicrisis['fecha_ingreso']))}}</td>
                                <td>{{date('d-m-Y', strtotime($arr_epicrisis['fecha_egreso']))}}</td>
                                <td>
                                    <a class="btn btn-danger btn-xs" onclick="pdfEpicrisis({{$arr_epicrisis['id']}},'{{$arr_epicrisis['tipo']}}');" href="http://10.4.237.27/epicrisis-hsjd/api/epicrisis/pdf?token=5b7dbd9cf2d88&epicrisis_id={{$arr_epicrisis['id']}}&tipo={{$arr_epicrisis['tipo']}}" title="Archivo" target="_blank"><i class="fa fa-file-pdf" style="color:white"></i></a>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            @endif
        </div>
        <div class="col-12 table-responsive">
            @if (isset($informes) && count($informes) > 0 && !empty($informes[0]))
                <strong>Biopsia</strong>
                <table class="table table-striped">
                    <thead style="background-color: lightcyan">
                        <tr>
                            <th>Tipo Estudio</th>
                            <th>Código Estudio</th>
                            <th>Servicio Solicitante</th>
                            <th>F. Toma</th>
                            <th>F. Cierre</th>
                            <th><i class="fas fa-cog"></i></th>
                        </tr>
                    </thead>
                    <tbody>
                        
                        @foreach ($informes as $key => $informe)
                            <tr>
                            <td>{{$informe->tipoEstudio}}</td>
                                <td>{{$informe->codigoEstudio}}</td>
                                <td>{{$informe->servicio_solitante}}</td>
                                <td>{{date('d-m-Y', strtotime($informe->fecha_toma_muestra))}}</td>
                                <td>{{date('d-m-Y', strtotime($informe->fechaCierre))}}</td>
                                <td>
                                    <a class="btn btn-danger btn-xs" onclick="pdfBiopsia('{{isset($comite->paciente) ? $comite->paciente->tx_rut : $comite->tx_rut}}',{{$key}});" href="/getBiopsia?rut={{isset($comite->paciente) ? $comite->paciente->tx_rut : $comite->tx_rut}}&fila={{$key}}" title="Archivo" target="_blank"><i class="fa fa-file-pdf" style="color:white"></i></a>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            @endif
        </div>
        <div class="col-12 table-responsive">
            @if (isset($solicitudesPabellon) && count($solicitudesPabellon) > 0)
                <strong>Protocolo</strong>
                <table class="table table-striped">
                    <thead style="background-color: lightyellow">
                        <tr>
                            <th>Fecha Protocolo</th>
                            <th>Diagnóstico Pre-Operatorio</th>
                            <th>Cirugía Practictada</th>
                            <th><i class="fas fa-cog"></i></th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($solicitudesPabellon as $solicitudPabellon)
                            <tr>
                                <td>{{date('d-m-Y', strtotime($solicitudPabellon->protocolo->fc_entrada_pabellon))}}</td>
                                <td>{{$solicitudPabellon->protocolo->gl_diagnostico_pre}}</td>
                                <td>{{$solicitudPabellon->protocolo->gl_cirugia_practicada}}</td>
                                <td>
                                    <a class="btn btn-danger btn-xs" onclick="pdfProtocolo({{$solicitudPabellon->protocolo->id}});" href="/getProtocolos?protocolo_id={{$solicitudPabellon->protocolo->id}}" title="Archivo" target="_blank"><i class="fa fa-file-pdf" style="color:white"></i></a>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            @endif
        </div>
    </div>
</div>