<div class="col-md-12">
    <div class="card">
    <div class="card-header p-2">
        <ul class="nav nav-pills">
        <li class="nav-item"><a class="nav-link active" href="#antecedentesMorbidos" data-toggle="tab">Antecedentes Mórbidos</a></li>
        <li class="nav-item"><a class="nav-link" href="#historiaClinica" data-toggle="tab">Historia Clínica</a></li>
        <li class="nav-item"><a class="nav-link" href="#diagnosticoActual" data-toggle="tab">Diagnóstico Actual</a></li>
        <li class="nav-item"><a class="nav-link" href="#planPropuesto" data-toggle="tab">Plan Propuesto</a></li>
        </ul>
    </div>
    <div class="card-body">
        <div class="tab-content">
            <div class="tab-pane active" id="antecedentesMorbidos">
                <div class="form-group">
                    <div class="row">
                        <div class="col-sm-3">
                            <label for="bo_antecedentes_familiares">Antecedentes Familiares<span style="color:#FF0000";>*</span></label>
                            <div class="row">
                                <div class="col-sm-3">
                                    <label>Sí</label>
                                </div>
                                <div class="col-sm-1">
                                    <input class="form-check-input" type="radio" id="bo_antecedentes_familiares" name="bo_antecedentes_familiares" value="1" {{isset($formulario->bo_antecedentes_familiares) && $formulario->bo_antecedentes_familiares == 1 ? "checked" : ""}}>
                                </div>
                                <div class="col-sm-3">
                                    <label>No</label>
                                </div>
                                <div class="col-sm-1">
                                    <input class="form-check-input" type="radio" id="bo_antecedentes_familiares" name="bo_antecedentes_familiares" value="0" {{isset($formulario->bo_antecedentes_familiares) && $formulario->bo_antecedentes_familiares == 0 ? "checked" : ""}}>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <label for="bo_cancer_previo">Cancer Previo<span style="color:#FF0000";>*</span></label>
                            <div class="row">
                                <div class="col-sm-3">
                                    <label>Sí</label>
                                </div>
                                <div class="col-sm-1">
                                    <input class="form-check-input" type="radio" id="bo_cancer_previo" name="bo_cancer_previo" value="1" {{isset($formulario->bo_cancer_previo) && $formulario->bo_cancer_previo == 1 ? "checked" : ""}}>
                                </div>
                                <div class="col-sm-3">
                                    <label>No</label>
                                </div>
                                <div class="col-sm-1">
                                    <input class="form-check-input" type="radio" id="bo_cancer_previo" name="bo_cancer_previo" value="0" {{isset($formulario->bo_cancer_previo) && $formulario->bo_cancer_previo == 0 ? "checked" : ""}}>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <label for="tx_tipo_cancer">Tipo Cancer:</label>
                            <input class="form-control" id="tx_tipo_cancer" name="tx_tipo_cancer" value="{{isset($formulario->tx_tipo_cancer) ? $formulario->tx_tipo_cancer : ''}}">
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <div class="col-sm-3">
                            <label for="bo_tabaco">Tabaco<span style="color:#FF0000";>*</span></label>
                            <div class="row">
                                <div class="col-sm-3">
                                    <label>Sí</label>
                                </div>
                                <div class="col-sm-1">
                                    <input class="form-check-input" type="radio" id="bo_tabaco" name="bo_tabaco" value="1" {{isset($formulario->bo_tabaco) && $formulario->bo_tabaco == 1 ? "checked" : ""}}>
                                </div>
                                <div class="col-sm-3">
                                    <label>No</label>
                                </div>
                                <div class="col-sm-1">
                                    <input class="form-check-input" type="radio" id="bo_tabaco" name="bo_tabaco" value="0" {{isset($formulario->bo_tabaco) && $formulario->bo_tabaco == 0 ? "checked" : ""}}>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <label for="bo_oh">OH<span style="color:#FF0000";>*</span></label>
                            <div class="row">
                                <div class="col-sm-3">
                                    <label>Sí</label>
                                </div>
                                <div class="col-sm-1">
                                    <input class="form-check-input" type="radio" id="bo_oh" name="bo_oh" value="1" {{isset($formulario->bo_oh) && $formulario->bo_oh == 1 ? "checked" : ""}}>
                                </div>
                                <div class="col-sm-3">
                                    <label>No</label>
                                </div>
                                <div class="col-sm-1">
                                    <input class="form-check-input" type="radio" id="bo_oh" name="bo_oh" value="0" {{isset($formulario->bo_oh) && $formulario->bo_oh == 0 ? "checked" : ""}}>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <label for="tx_otro_antecedente">Otro Antecedente:</label>
                            <input class="form-control" id="tx_otro_antecedente" name="tx_otro_antecedente" value="{{isset($formulario->tx_otro_antecedente) ? $formulario->tx_otro_antecedente : ''}}">
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <div class="col-sm-3">
                            <label for="comorbilidades">Comorbilidad:</label>
                            <select class="select2 select2-hidden-accessible" id="comorbilidades" name="comorbilidades[]" multiple="" data-placeholder="Seleccione comorbilidad(es)" style="width: 100%;" data-select2-id="7" tabindex="-1" aria-hidden="true">
                                @foreach ($comorbilidades as $comorbilidad)
                                    <option value={{$comorbilidad->id}} >{{$comorbilidad->tx_nombre}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-sm-3">
                            <label for="bo_revisar_imagen">Revisar Imagen</label>
                            <div class="row">
                                <div class="col-sm-3">
                                    <label>Sí</label>
                                </div>
                                <div class="col-sm-1">
                                    <input class="form-check-input" type="radio" id="bo_revisar_imagen" name="bo_revisar_imagen" value="1" {{isset($formulario->bo_revisar_imagen) && $formulario->bo_revisar_imagen == 1 ? "checked" : ""}}>
                                </div>
                                <div class="col-sm-3">
                                    <label>No</label>
                                </div>
                                <div class="col-sm-1">
                                    <input class="form-check-input" type="radio" id="bo_revisar_imagen" name="bo_revisar_imagen" value="0" {{isset($formulario->bo_revisar_imagen) && $formulario->bo_revisar_imagen == 0 ? "checked" : ""}}>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="tab-pane" id="historiaClinica">
                <div class="form-group">
                    <div class="row">
                        <div class="col-sm-6">
                            <label for="tx_historia_clinica">Historia Clinica</label>
                            <textarea class="form-control noresize ckeditor" id="tx_historia_clinica" name="tx_historia_clinica">{{isset($formulario->tx_historia_clinica) ? $formulario->tx_historia_clinica : ""}}</textarea>
                        </div>
                        <div class="col-sm-6">
                            <label for="tx_resultado_imagen">Resultado Imagen</label>
                            <textarea class="form-control noresize ckeditor" id="tx_resultado_imagen" name="tx_resultado_imagen">{{isset($formulario->tx_resultado_imagen) ? $formulario->tx_resultado_imagen : ""}}</textarea>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <div class="col-sm-2">
                            <label for="nr_ipa">IPA</label>
                            <div class="col-sm-8">
                                <input type="number" step="any" class="form-control" id="nr_ipa" name="nr_ipa" min="0" value="{{isset($formulario->nr_ipa) ? $formulario->nr_ipa : ""}}">	
                            </div>				
                        </div>
                        <div class="col-sm-2">
                            <label for="nr_vef1">VEF1</label>
                            <div class="col-sm-8">
                                <input type="number" step="any" class="form-control" id="nr_vef1" name="nr_vef1" min="0" value="{{isset($formulario->nr_vef1) ? $formulario->nr_vef1 : ""}}">	
                            </div>				
                        </div>
                        <div class="col-sm-2">
                            <label for="nr_cvf">CVF</label>
                            <div class="col-sm-8">
                                <input type="number" step="any" class="form-control" id="nr_cvf" name="nr_cvf" min="0" value="{{isset($formulario->nr_cvf) ? $formulario->nr_cvf : ""}}">	
                            </div>				
                        </div>
                        <div class="col-sm-2">
                            <label for="nr_tiffenau">TIFFENAU</label>
                            <div class="col-sm-8">
                                <input type="number" step="any" class="form-control" id="nr_tiffenau" name="nr_tiffenau" min="0" value="{{isset($formulario->nr_tiffenau) ? $formulario->nr_tiffenau : ""}}">	
                            </div>				
                        </div>
                        <div class="col-sm-2">
                            <label for="nr_dlco">DLCO</label>
                            <div class="col-sm-8">
                                <input type="number" step="any" class="form-control" id="nr_dlco" name="nr_dlco" min="0" value="{{isset($formulario->nr_dlco) ? $formulario->nr_dlco : ""}}">	
                            </div>				
                        </div>
                        <div class="col-sm-2">
                            <label for="nr_fx_pulmonar">Fx Pulmonar</label>
                            <div class="col-sm-8">
                                <input type="number" step="any" class="form-control" id="nr_fx_pulmonar" name="nr_fx_pulmonar" min="0" value="{{isset($formulario->nr_fx_pulmonar) ? $formulario->nr_fx_pulmonar : ""}}">	
                            </div>				
                        </div>
                    </div>
                </div>
            </div>
            <div class="tab-pane" id="diagnosticoActual">
                <div class="form-group">
                    <div class="row">
                        <div class="col-sm-4">
                            <label for="tx_diagnostico_actual">Diagnóstico Actual</label>
                            <textarea class="form-control noresize ckeditor" id="tx_diagnostico_actual" name="tx_diagnostico_actual">{{isset($formulario->tx_diagnostico_actual) ? $formulario->tx_diagnostico_actual : ""}}</textarea>
                        </div>
                        <div class="col-sm-4">
                            <label for="tx_biopsia_pre_op">Biopsia Pre Operatoria</label>
                            <textarea class="form-control noresize ckeditor" id="tx_biopsia_pre_op" name="tx_biopsia_pre_op">{{isset($formulario->tx_biopsia_pre_op) ? $formulario->tx_biopsia_pre_op : ""}}</textarea>
                        </div>
                        <div class="col-sm-4">
                            <label for="tx_broncoscopia">Broncoscopia</label>
                            <textarea class="form-control noresize ckeditor" id="tx_broncoscopia" name="tx_broncoscopia">{{isset($formulario->tx_broncoscopia) ? $formulario->tx_broncoscopia : ""}}</textarea>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <div class="col-sm-3">
                            <label for="id_ecog">ECOG</label>
                            <select class="select2 select2-hidden-accessible" id="id_ecog" name="id_ecog" style="width: 100%;" data-select2-id="7" tabindex="-1" aria-hidden="true">
                                <option value=""></option>
                                <option value="0" {{isset($formulario->id_ecog) && $formulario->id_ecog == "0" ? "selected" : ""}}>0 - Totalmente asintomático y es capaz de realizar un trabajo y actividades normales de la vida diaria.</option>
                                <option value="1" {{isset($formulario->id_ecog) && $formulario->id_ecog == "1" ? "selected" : ""}}>1 - Presenta síntomas que le impiden realizar trabajos pesados, aunque se desempeña normalmente en sus actividades cotidianas y en trabajos ligeros. El paciente sólo permanece en la cama durante las horas de sueño nocturno.</option>
                                <option value="2" {{isset($formulario->id_ecog) && $formulario->id_ecog == "2" ? "selected" : ""}}>2 - El paciente no es capaz de desempeñar ningún trabajo, se encuentra con síntomas que le obligan a permanecer en la cama durante varias horas al día, además de las de la noche, pero que no superan el 50% del día. El individuo satisface la mayoría de sus necesidades personales solo.</option>
                                <option value="3" {{isset($formulario->id_ecog) && $formulario->id_ecog == "3" ? "selected" : ""}}>3 - El paciente necesita estar encamado más de la mitad del día por la presencia de síntomas. Necesita ayuda para la mayoría de las actividades de la vida diaria como por ejemplo el vestirse.</option>
                                <option value="4" {{isset($formulario->id_ecog) && $formulario->id_ecog == "4" ? "selected" : ""}}>4 - El paciente permanece encamado el 100% del día y necesita ayuda para todas las actividades de la vida diaria, como por ejemplo la higiene corporal, la movilización en la cama e incluso la alimentación.</option>
                                <option value="5" {{isset($formulario->id_ecog) && $formulario->id_ecog == "5" ? "selected" : ""}}>5 - Paciente Fallecido.</option>
                            </select>					
                        </div>
                        <div class="col-sm-3">
                            <label for="id_cie10">Cie-O<span style="color:#FF0000";>*</span></label>
                            <div class="col-sm-12">
                                <select class="form-control select_diagnosticos" id="id_cie10" name="id_cie10" required>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="tab-pane" id="planPropuesto">
                <div class="form-group">
                    <div class="row">
                        <div class="col-sm-8">
                            <label for="tx_plan">Plan<span style="color:#FF0000";>*</span></label>
                            <textarea class="form-control noresize ckeditor" required id="tx_plan" name="tx_plan">{{isset($formulario->tx_plan) ? $formulario->tx_plan : ""}}</textarea>
                        </div>
                        <div class="col-sm-4">
                            <label for="archivo">Archivo Adjunto</label>
                            <input type="file" class="form-control" id="archivo" name="archivo">
                            <label><span style="color:#FF0000";>{{isset($comite->tx_archivo) && $bo_clonar != 1 ? "Ya existe un archivo. Si sube otro se sobreescribira" : ""}}</span></label>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
</div>