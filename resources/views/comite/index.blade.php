@extends('adminlte::page')

@section('title', 'Lista de Comites')

@section('content_header')
    @if(session()->has('message'))
        <div class="alert alert-success">
            {{ session()->get('message') }}
        </div>
    @endif
    @if(session()->has('error'))
        <div class="alert alert-danger">
            <i class="fa fa-exclamation-triangle"></i> {{ session()->get('error') }}
        </div>
    @endif
@stop

@section('content')
		{{-- <form class="form-horizontal" id="form" method="POST">
			{{ csrf_field() }} --}}
	<div class="card card-info">
		<div class="card-header">
		<h3 class="card-title">Lista de Pacientes {{$tipoComite->tx_nombre_mayuscula}} {{request()->resuelto == 1 ? 'Resueltos' : 'Por Resolver'}}</h3>
		</div>
		<!-- /.card-header -->
		<!-- form start -->
		<div class="card-body">
            <div id="example1_wrapper" class="dataTables_wrapper dt-bootstrap4">
                <div class="row">
                    <div class="col-sm-12">
                        <form class="form-horizontal">
                            <input type="hidden" id="resuelto" name="resuelto" value="{{request()->resuelto}}"/>
                            <input type="hidden" id="tipo_comite" name="tipo_comite" value="{{request()->tipo_comite}}"/>
                            <div class="form-group row">
                                <div class="col-sm-4">
                                    <label for="fc_inicio">Fecha de Inicio</label>
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text"><i class="far fa-calendar-alt"></i></span>
                                        </div>
                                        <input type="date" class="form-control" id="fc_inicio" name="fc_inicio" value="{{isset(request()->fc_inicio) ? request()->fc_inicio : date("Y-m-d")}}">
                                    </div>	
                                </div>
                                <div class="col-sm-4">
                                    <label for="fc_termino">Fecha de Termino</label>
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text"><i class="far fa-calendar-alt"></i></span>
                                        </div>
                                        <input type="date" class="form-control" id="fc_termino" name="fc_termino" value="{{isset(request()->fc_termino) ? request()->fc_termino : date("Y-m-d")}}">
                                    </div>	
                                </div>
                                <div class="col-sm-4 text-right">
                                    <a href="comite?resuelto={{request()->resuelto == 1 ? '0' : '1'}}&tipo_comite={{request()->tipo_comite}}" class="{{request()->resuelto == 1 ? 'btn btn-warning' : 'btn btn-success'}}">{{request()->resuelto == 1 ? 'Por Resolver' : 'Resuelto'}}</a>
                                    <button type="submit" class="btn btn-info">Filtrar</button>
                                    <button type="submit" onclick="excel()" class="btn btn-success"><i class="fa fa-file-excel" style="color:white"></i></button>
                                    <a href="comite/create?tipo_comite={{request()->tipo_comite}}" title="Nuevo Comite" class="btn btn-success"><i class="fa fa-plus" style="color:white"></i></a>
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-sm-4">
                                    <label for="especialidades">Especialidad:</label>
                                    <select class="select2 select2-hidden-accessible" id="especialidades" name="especialidades[]" multiple="" data-placeholder="Seleccione especialidad(es)" style="width: 100%;" data-select2-id="7" tabindex="-1" aria-hidden="true">
                                        @foreach ($especialidades as $especialidad)
                                            <option value={{$especialidad->id}} >{{$especialidad->tx_nombre_minuscula}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-sm-4">
                                    <label for="medicos">Medico:</label>
                                    <select class="select2 select2-hidden-accessible" id="medicos" name="medicos[]" multiple="" data-placeholder="Seleccione medico(s)" style="width: 100%;" data-select2-id="7" tabindex="-1" aria-hidden="true">
                                        @foreach ($medicos as $medico)
                                            <option value={{$medico->id}} >{{$medico->tx_nombre_minuscula}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </form>
                        <table class="table table-striped table-hover" id="tableUsers">
                            <thead>
                                <th>ID</th>
                                <th>Nombre</th>
                                <th>Rut</th>
                                <th>F. Presentación</th>
                                <th>Médico</th>
                                <th>Especialidad</th>
                                <th><i class="fa fa-cog"></i></th>
                            </thead>
                            <tbody>
                                @foreach ($comites as $comite)
                                    <tr role="row" class="odd">
                                        @if (request()->resuelto == 0 && isset($comite->resolucion))
                                            <td><span style="color:#FF0000";>{{$comite->id}}</span></td>
                                        @else
                                            <td>{{$comite->id}}</td>
                                        @endif
                                        <td><a href="paciente/1?rut={{$comite->paciente->tx_rut}}">{{$comite->paciente->tx_nombre_completo}}<a></td>
                                        <td>{{$comite->paciente->tx_rut}}</td>
                                        <td>{{date('d-m-Y', strtotime($comite->fc_presentacion))}}</td>
                                        <td>{{$comite->medico->tx_nombre_minuscula}}</td>
                                        <td>{{$comite->especialidad->tx_nombre_minuscula}}</td>
                                        <td>
                                            <div class="btn-group">
                                                @if (request()->resuelto == 0)
                                                    @if (!isset($comite->resolucion))
                                                        <a class="btn btn-success btn-xs" href="resolucion/create?id={{$comite->id}}&tipo_comite={{request()->tipo_comite}}" title="Resolver"><i class="fa fa-check" style="color:white"></i></a>
                                                    @else
                                                        <a class="btn btn-success btn-xs" href="resolucion/{{$comite->id}}/edit" title="Terminar de Resolver"><i class="fa fa-spinner" style="color:white"></i></a>
                                                    @endif
                                                    @if ($comite->created_by == Auth::user()->id)
                                                        <a class="btn btn-warning btn-xs" href="comite/{{$comite->id}}/edit" title="Editar"><i class="fa fa-edit" style="color:white"></i></a>
                                                    @endif
                                                @endif
                                                <a class="btn btn-danger btn-xs" href="pdfComite?id={{$comite->id}}" title="Ver Comite" target="_blank"><i class="fa fa-file-pdf" style="color:white"></i></a>
                                                @if (isset($comite->tx_archivo))
                                                    <a class="btn btn-primary btn-xs" href="storage/{{$comite->tx_archivo}}" title="Archivo" target="_blank"><i class="fa fa-file" style="color:white"></i></a>
                                                @endif
                                                <a class="btn btn-info btn-xs" href="clonar?id={{$comite->id}}" title="Clonar Comite"><i class="fa fa-copy" style="color:white"></i></a>
                                            </div>
										</td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                        {{ $comites->appends(request()->query())->links() }}
                    </div>
                </div>
            </div>
        </div>
        <!-- /.card-body -->
    </div>
@stop

@section('js')
<script>
    function excel(){
        var url = "excelComite?"+$( "form" ).serialize();
        window.location.href = url;
        alert('descargando excel...');
    }

    @if(isset(request()->especialidades))
        var user = @json(request()->all());
        var especialidades = user.especialidades;
        $("#especialidades").val(especialidades).trigger('change');
    @endif

    @if(isset(request()->medicos))
        var user = @json(request()->all());
        var medicos = user.medicos;
        $("#medicos").val(medicos).trigger('change');
    @endif

	$(".alert-success").fadeTo(20000, 500).slideUp(500, function(){
        $(".alert-success").slideUp(1000);
    });

    $(".alert-danger").fadeTo(20000, 5000).slideUp(500, function(){
        $(".alert-danger").slideUp(1000);
    });

    $('.select2').select2();
</script>
@stop