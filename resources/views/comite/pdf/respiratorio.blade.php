<div class="table-responsive" >
    <table class="table table-bordered " width="100%">
        <tr>
            <td colspan="8" align="center" style="background-color: #999999;" >
                <strong>Antecedentes Previos</strong>
            </td>
        </tr>
        <tr>
            <td width="10%" style="background-color: #eeeeee;">
                <strong>Cáncer Previo</strong>
            </td>
            <td width="10%" >
                {{$comite->formulario->bo_cancer_previo == 1 ? 'SI' : 'NO'}}
            </td>
            <td width="10%" style="background-color: #eeeeee;">
                <strong>Tipo Cáncer Previo</strong>
            </td>
            <td width="15%" >
                {{isset($comite->formulario->tx_tipo_cancer) ? $comite->formulario->tx_tipo_cancer : ''}}
            </td>
            <td width="10%" style="background-color: #eeeeee;">
                <strong>Antecedentes Familiares</strong>
            </td>
            <td width="10%" >
                {{$comite->formulario->bo_antecedentes_familiares == 1 ? 'SI' : 'NO'}}
            </td>
            <td width="10%" style="background-color: #eeeeee;">
                <strong>Comorbilidad</strong>
            </td>
            <td width="25%" >
                {{$comite->comorbilidades->pluck('tx_nombre')->implode(', ')}}
            </td>
        </tr>
        <tr>
            <td width="10%" style="background-color: #eeeeee;">
                <strong>Tabaco</strong>
            </td>
            <td width="15%" >
                {{$comite->formulario->bo_tabaco == 1 ? 'SI' : 'NO'}}
            </td>
            <td width="10%" style="background-color: #eeeeee;">
                <strong>Oh</strong>
            </td>
            <td width="15%" >
                {{$comite->formulario->bo_oh == 1 ? 'SI' : 'NO'}}
            </td>
            <td width="10%" style="background-color: #eeeeee;">
                <strong>Otro Antecedente</strong>
            </td>
            <td width="15%" colspan="3">
                {{isset($comite->formulario->tx_otro_antecedente) ? $comite->formulario->tx_otro_antecedente : ''}}
            </td>
        </tr>
        <tr>
            <td width="10%" style="background-color: #eeeeee;">
                <strong>Historia Clínica</strong>
            </td>
            <td width="15%" colspan="7">
                {!!$comite->formulario->tx_historia_clinica!!}
            </td>
        </tr>
        <tr>
            <td width="10%" style="background-color: #eeeeee;">
                <strong>Resultado Imagen</strong>
            </td>
            <td width="15%" colspan="7">
                {!!$comite->formulario->tx_resultado_imagen!!}
            </td>
        </tr>
    </table>
</div>
<div class="table-responsive" >
    <table class="table table-bordered " width="100%">
        <tr>
            <td colspan="10" align="center" style="background-color: #999999;" >
                <strong>Diagnostico</strong>
            </td>
        </tr>
        <tr>
            <td width="5%" colspan="2" style="background-color: #eeeeee;">
                <strong>Cie-O</strong>
            </td>
            <td width="10%" colspan="8">
                {{$comite->cie10->tx_nombre_minuscula}}
            </td>
        </tr>
        <tr>
            <td width="5%" colspan="2" style="background-color: #eeeeee;">
                <strong>Diagnóstico Actual</strong>
            </td>
            <td width="10%" colspan="8">
                {!!$comite->formulario->tx_diagnostico_actual!!}
            </td>
        </tr>
        <tr>
            <td width="5%" colspan="2" style="background-color: #eeeeee;">
                <strong>Ecog</strong>
            </td>
            <td width="10%" colspan="8">
                {{isset($comite->formulario->tx_ecog) ? $comite->formulario->tx_ecog : ''}}
            </td>
        </tr>
        <tr>
            <td width="5%" colspan="2" style="background-color: #eeeeee;">
                <strong>Biopsia Pre Operatoria</strong>
            </td>
            <td width="10%" colspan="8">
                {!!isset($comite->formulario->tx_biopsia_pre_op) ? $comite->formulario->tx_biopsia_pre_op : ''!!}
            </td>
        </tr>
        <tr>
            <td width="5%" colspan="2" style="background-color: #eeeeee;">
                <strong>Broncoscopia</strong>
            </td>
            <td width="10%" colspan="8">
                {!!isset($comite->formulario->tx_broncoscopia) ? $comite->formulario->tx_broncoscopia : ''!!}
            </td>
        </tr>
        <tr>
            <td width="5%" style="background-color: #eeeeee;">
                <strong>IPA</strong>
            </td>
            <td width="5%" >
                {{isset($comite->formulario->nr_ipa) ? $comite->formulario->nr_ipa : ''}}
            </td>
            <td width="5%" style="background-color: #eeeeee;">
                <strong>VEF1</strong>
            </td>
            <td width="5%" >
                {{isset($comite->formulario->nr_vef1) ? $comite->formulario->nr_vef1 : ''}}
            </td>
            <td width="5%" style="background-color: #eeeeee;">
                <strong>TIFFENAU</strong>
            </td>
            <td width="5%" >
                {{isset($comite->formulario->nr_tiffenau) ? $comite->formulario->nr_tiffenau : ''}}
            </td>
        </tr>
        <tr>
            <td width="5%" style="background-color: #eeeeee;">
                <strong>CVF</strong>
            </td>
            <td width="10%" >
                {{isset($comite->formulario->nr_cvf) ? $comite->formulario->nr_cvf : ''}}
            </td>
            <td width="5%" style="background-color: #eeeeee;">
                <strong>DLCO</strong>
            </td>
            <td width="5%" >
                {{isset($comite->formulario->nr_dlco) ? $comite->formulario->nr_dlco : ''}}
            </td>
            <td width="5%" style="background-color: #eeeeee;">
                <strong>Fx Pulmonar</strong>
            </td>
            <td width="5%" >
                {{isset($comite->formulario->nr_fx_pulmonar) ? $comite->formulario->nr_fx_pulmonar : ''}}
            </td>
        </tr>
        <tr>
            <td width="5%" colspan="2" style="background-color: #eeeeee;">
                <strong>Plan</strong>
            </td>
            <td width="10%" colspan="8">
                {!!isset($comite->formulario->tx_plan) ? $comite->formulario->tx_plan : ''!!}
            </td>
        </tr>
        <tr>
            <td width="5%" colspan="2" style="background-color: #eeeeee;">
                <strong>N° Comité</strong>
            </td>
            <td width="10%" colspan="8">
                {{isset($comite->id_comite) ? $comite->id_comite : $comite->id}}
            </td>
        </tr>
    </table>
</div>
@if(isset($comite->resolucion))
<div class="table-responsive" >
        <table class="table table-bordered " width="100%">
            <tr>
                <td colspan="8" align="center" style="background-color: #999999;" >
                    <strong>Resolucion Comité</strong>
                </td>
            </tr>
            <tr>
                <td width="15%" style="background-color: #eeeeee;">
                    <strong>F. Resolucion</strong>
                </td>
                <td width="15%" >
                    {{date('d-m-Y', strtotime($comite->resolucion->created_at))}}
                </td>
                <td width="10%" style="background-color: #eeeeee;">
                    <strong>Tratamiento Propuesto</strong>
                </td>
                <td width="60%" colspan="5">
                    {{$comite->tratamientos->pluck('tx_nombre')->implode(', ')}}
                </td>
            </tr>
            <tr>
                <td width="10%" style="background-color: #eeeeee;">
                    <strong>Curativo</strong>
                </td>
                <td width="15%" >
                    {{$comite->resolucion->bo_curativo == 1 ? 'Si' : 'No'}}
                </td>
                <td width="10%" style="background-color: #eeeeee;">
                    <strong>FBC</strong>
                </td>
                <td width="15%" >
                    {{$comite->resolucion->bo_fbc == 1 ? 'Si' : 'No'}}
                </td>
                <td width="10%" style="background-color: #eeeeee;">
                    <strong>PET CT</strong>
                </td>
                <td width="15%" >
                    {{$comite->resolucion->bo_pet_ct == 1 ? 'Si' : 'No'}}
                </td>
                <td width="10%" style="background-color: #eeeeee;">
                    <strong>EBUS</strong>
                </td>
                <td width="15%" >
                    {{$comite->resolucion->bo_ebus == 1 ? 'Si' : 'No'}}
                </td>
            </tr>
            <tr>
                <td width="10%" colspan="2" style="background-color: #eeeeee;">
                    <strong>Tratamiento Comité</strong>
                </td>
                <td width="15%" colspan="6">
                    <strong>{!!isset($comite->resolucion->tx_tratamiento_comite) ? $comite->resolucion->tx_tratamiento_comite : ''!!}</strong>
                </td>
            </tr>
            <tr>
                <td width="10%" colspan="2" style="background-color: #eeeeee;">
                    <strong>Estado</strong>
                </td>
                <td width="15%" colspan="6">
                    {{$comite->bo_resuelto == 72 ? 'Resuelto' : 'Representar'}}
                </td>
            </tr>
            <tr>
                <td width="10%" colspan="2" style="background-color: #eeeeee;">
                    <strong>Integrantes</strong>
                </td>
                <td width="15%" colspan="6">
                    {{$comite->integrantes->pluck('tx_nombre_minuscula')->implode(', ')}}
                </td>
            </tr>
        </table>
    @endif
</div>