@extends('adminlte::page')

@section('title', 'Resolver Comite')

@section('content')
		{{-- <form class="form-horizontal" id="form" method="POST">
			{{ csrf_field() }} --}}
		<!-- /.card-header -->
		<!-- form start -->
		<form role="form" class="form-horizontal" id="form" method="POST" action="{{action('ResolucionController@store')}}">
			{{ csrf_field() }}
			<input type="hidden" id="id_comite" name="id_comite" value="{{isset($comite) ? $comite->id : request()->id}}"/>
			<input type="hidden" id="id_tipo_comite" name="id_tipo_comite" value="{{isset($comite) ? $comite->id_tipo_comite :request()->tipo_comite}}"/>
			{{-- <input type="hidden" id="tx_rut" name="tx_rut" value="{{$comite->tx_rut}}"/> --}}

			<div class="card bg-gradient sticky-top card-info">
				<button type="button" class="btn btn btn-sm card-info" data-card-widget="collapse">
					<div class="card-header border-0 ui-sortable-handle">
					<h3 class="card-title">
						<i class="fa fa-user"></i>
						Datos Generales. (<strong>N° Comité:</strong> {{$comite->id}})
					</h3>
					<!-- tools card -->
					<div class="card-tools">
						<!-- button with a dropdown -->
						<i class="fas fa-minus"></i>
					</div>
					<!-- /. tools -->
					</div>
				</button>
				<!-- /.card-header -->
				<div class="card-body pt-0">
					<!--The calendar -->
					<div class="row invoice-info">
						<div class="col-sm-4 invoice-col">
							<strong>Médico Tratante: </strong>{{$comite->medico->tx_nombre_minuscula}}<br><br>
							<strong>Rut: </strong>{{$comite->paciente->tx_rut}}<br>
							<strong>Ficha: </strong>{{$comite->paciente->nr_ficha}}<br>
						</div>
						<div class="col-sm-4 invoice-col">
							<strong>Especialidad: </strong>{{$comite->especialidad->tx_nombre_minuscula}}<br><br>
							<strong>Nombre: </strong>{{$comite->paciente->tx_nombre_completo}}<br>
						</div>
						<div class="col-sm-4 invoice-col">
							<strong>{{isset($comite->establecimiento->tx_nombre) ? 'Hospital Origen:' : ''}} </strong>{{isset($comite->establecimiento->tx_nombre) ? $comite->establecimiento->tx_nombre : ''}}<br><br>
							<strong>F. Nacimiento: </strong>{{date('d-m-Y', strtotime($comite->paciente->fc_nacimiento))}} ({{$comite->paciente->edad}})<br>
							<strong>F. Presentación: </strong>{{date('d-m-Y', strtotime($comite->fc_presentacion))}}<br>
						</div>
					</div>
				</div>
				<!-- /.card-body -->
			</div>
			@include('comite.resolucion.'.$comite->tipoComite->tx_nombre)		
		</form>
@stop

@section('js')
<script>
	$("body").addClass("sidebar-collapse");
	function getPerson(tx_rut){
		@if (!isset($user))
			$.getJSON("{{action('GetController@getDatosRut')}}?rut="+tx_rut,
			function(data){
				if(data.user && data.encontrado){
					var opcion = confirm("Este Usuario ya existe desea editarlo?");
					if (opcion == true) {
						location.href = data.id+"/edit";
					} else {
						$("#tx_rut").val("");
					}
				}
				else{
					if(data.encontrado == true){
						$("#tx_rut").val(data.tx_rut);
						$("#tx_nombre").val(data.tx_nombre);
						$("#tx_nombre").css('display','').attr('readonly', true);
					}else{
						alert("Rut no encontrado");
						$("#tx_rut").val("");
						$("#tx_nombre").val("");
						$("#tx_nombre").css('display','').attr('readonly', false);
					}
				}
			})
		@endif
	};

	function pdfEpicrisis(id, tipo){
		window.open('http://10.4.237.27/epicrisis-hsjd/api/epicrisis/pdf?token=5b7dbd9cf2d88&epicrisis_id='+id+'&tipo='+tipo,"Informe" , "width=800,height=600,fullscreen=yes,scrollbars=NO")
	}

	function pdfBiopsia(rut, key){
		window.open('../getBiopsia?rut='+rut+'&fila='+key,"Informe" , "width=800,height=600,fullscreen=yes,scrollbars=NO")
	}

	function pdfProtocolo(id){
		window.open('../getProtocolos?protocolo_id='+id,"Informe" , "width=800,height=600,fullscreen=yes,scrollbars=NO")
	}

	$('.select2').select2();
</script>
<script src="{{ asset('/vendors/ckeditor/ckeditor.js') }}"></script>
@stop