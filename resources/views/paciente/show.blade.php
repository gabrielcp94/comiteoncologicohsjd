@extends('adminlte::page')

@section('title', 'Perfil Paciente')

@section('content')
	<div class="invoice p-3 mb-3">
		@if(session()->has('message'))
            <div class="alert alert-success">
                {{ session()->get('message') }}
            </div>
        @endif
        @if(session()->has('error'))
            <div class="alert alert-danger">
               <i class="fa fa-exclamation-triangle"></i> {{ session()->get('error') }}
            </div>
        @endif
        <!-- title row -->
        <div class="row">
			<div class="col-10 row">
				<h4>
					<i class="ace-icon fa fa-user"></i>  {{$paciente->tx_nombre_completo}}
                </h4>
            </div>
            @if (isset($paciente))
                <div class="col-2 box-tools pull-right">
                    <a class="btn btn-xxs btn-success btn-xs pull-right" href="http://10.4.237.27/login?rUser={{Auth::user()->login}}&tUser={{session('token')}}&pac={{$paciente->id}}" target="_blank"><i class="fa fa-eye"></i> Perfil Hospitalizado </a>
                </div>
            @endif
			<!-- /.col -->
        </div>
              <!-- info row -->
		<div class="row invoice-info">
			<div class="col-sm-6 invoice-col">
				<strong>Rut: </strong>{{$paciente->tx_rut}}<br>
				<strong>Fecha de Nacimiento: </strong>
				@if((isset($paciente->fc_nacimiento) && $paciente->fc_nacimiento != '0000-00-00'))
                    {{date('d/m/Y', strtotime(str_replace("/",".",$paciente->fc_nacimiento)))}}
				@else 
					Sin Información
				@endif<br>
				{{-- <strong>Previsión: </strong>{{$paciente->prevision->tx_descripcion}} {{$paciente->clasificacionFonasa->tx_descripcion}} --}}
			</div>
			<!-- /.col -->
			<div class="col-sm-6 invoice-col">
                <strong>Ficha: </strong>
                @if(isset($paciente->nr_ficha) && $paciente->nr_ficha != 0)
                    {{$paciente->nr_ficha}}
                @else
                    Sin Información
                @endif<br>
				{{-- <strong>Sexo: </strong>{{$paciente->sexo->tx_descripcion}}<br> --}}
				{{-- <strong>Edad: </strong>{{$paciente->edad}}<br> --}}
				{{-- <strong>Dirección: </strong>{{isset($pacienteGen) ? $pacienteGen->tx_direccion : ''}}<br> --}}
				<strong>Telefono: </strong>
				@if(isset($paciente->tx_telefono))
                    {{$paciente->tx_telefono}}
				@else
					Sin Información
				@endif
			</div>
			<!-- /.col -->
		</div>
        <br>
        <div class="col-md-12">
            <div class="card">
              <div class="card-header p-2">
                <ul class="nav nav-pills">
                  <li class="nav-item"><a class="nav-link active" href="#comite" data-toggle="tab">Comite</a></li>
                  @if (isset($arr_epicrises) && count($arr_epicrises) > 0)
                    <li class="nav-item"><a class="nav-link" href="#epicrisis" data-toggle="tab">Epicrisis</a></li>
                  @endif
                  @if (isset($informes) && count($informes) > 0)
                    <li class="nav-item"><a class="nav-link" href="#biopsia" data-toggle="tab">Biopsias</a></li>
                  @endif
                  {{-- <li class="nav-item"><a class="nav-link" href="#biopsia" data-toggle="tab">Biopsia</a></li> --}}
                </ul>
              </div><!-- /.card-header -->
              <div class="card-body">
                <div class="tab-content">
                    <div class="tab-pane active" id="comite">
                        <div class="row">
                            <div class="col-12 table-responsive">
                                @if (isset($comitesAdulto) && count($comitesAdulto) > 0)
                                    <strong>Adulto</strong>
                                    <table class="table table-striped">
                                        <thead>
                                            <tr>
                                                <th>ID</th>
                                                <th>F. Presentación</th>
                                                <th>Medico</th>
                                                <th>Especialidad</th>
                                                <th>Estado</th>
                                                <th><i class="fas fa-cog"></i></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach ($comitesAdulto->sortByDesc('fc_ingreso') as $comiteAdulto)
                                                <tr>
                                                    <td>{{$comiteAdulto->id}}</td>
                                                    <td>{{date('d-m-Y', strtotime($comiteAdulto->fc_ingreso))}}</td>
                                                    <td>{{$comiteAdulto->tx_medico}}</td>
                                                    <td>{{ucwords(mb_strtolower($comiteAdulto->tx_especialidad))}}</td>
                                                    <td>				
                                                        @if (isset($comiteAdulto->resolucion))
                                                            @if ($comiteAdulto->resolucion->tx_estado == 'RESUELTO')
                                                                <small class="badge badge-success">Resuelto</small>
                                                            @else
                                                                <small class="badge badge-primary">Representar</small>
                                                            @endif
                                                        @else
                                                            <small class="badge badge-warning">Por Resolver</small>
                                                        @endif						
                                                    </td>
                                                    <td>
                                                        <form action="{{ route('comiteAdulto.destroy',$comiteAdulto->id) }}" method="POST">
                                                            <div class="btn-group">
                                                                @if (!isset($comiteAdulto->resolucion))
                                                                    <a class="btn btn-success btn-xs" href={{url('resolucionAdulto/create?id='.$comiteAdulto->id)}} title="Resolver"><i class="fa fa-check" style="color:white"></i></a>
                                                                    @if ($comiteAdulto->tx_especialidad == Auth::user()->especialidad->tx_descripcion)
                                                                        <a class="btn btn-warning btn-xs" href={{url('comiteAdulto/'.$comiteAdulto->id.'/edit')}} title="Editar"><i class="fa fa-edit" style="color:white"></i></a>
                                                                    @endif
                                                                @elseif($comiteAdulto->resolucion->tx_estado != 'RESUELTO')
                                                                    {{-- <a class="btn btn-success btn-xs" href={{url('resolucion/'.$comiteAdulto->id.'/edit?adulto=1')}} title="Resolver (editar)"><i class="fa fa-check" style="color:white"></i></a> --}}
                                                                    @if ($comiteAdulto->tx_medico_tratante == Auth::user()->name)
                                                                        <a class="btn btn-warning btn-xs" href={{url('comiteAdulto/'.$comiteAdulto->id.'/edit')}} title="Editar"><i class="fa fa-edit" style="color:white"></i></a>
                                                                    @endif
                                                                @endif
                                                                <a class="btn btn-secondary btn-xs" href={{url('pdfComiteAdulto?resuelto='.(isset($comiteAdulto->resolucion) && $comiteAdulto->resolucion->tx_estado == 'RESUELTO' ? 1 : 0).'&id='.$comiteAdulto->id)}} title="Ver Comite"><i class="fa fa-file-pdf" style="color:white"></i></a>
                                                                @if (isset($comiteAdulto->tx_archivo)  && $comiteAdulto->tx_archivo != '')
                                                                    <a class="btn btn-primary btn-xs" href={{url('storage/'.$comiteAdulto->tx_archivo)}} title="Archivo"><i class="fa fa-file" style="color:white"></i></a>
                                                                @endif
                                                                <a class="btn btn-info btn-xs" href={{url('clonarAdulto?id='.$comiteAdulto->id)}} title="Clonar Comite"><i class="fa fa-copy" style="color:white"></i></a>
                                                                @csrf
                                                                @method('DELETE')
                                                                <button onclick="return confirm('¿Esta seguro de eliminar a este Comite?');" type="submit" class="btn btn-danger btn-xs" title="Eliminar Comite"><i class="fa fa-trash" style="color:white"></i></button>
                                                            </div>
                                                        </form>
                                                    </td>
                                                </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                @endif
                                @if (isset($comitesInfantil) && count($comitesInfantil) > 0)
                                    <strong>Infantil</strong>
                                    <table class="table table-striped">
                                        <thead>
                                            <tr>
                                                <th>ID</th>
                                                <th>F. Presentación</th>
                                                <th>Medico</th>
                                                <th>Especialidad</th>
                                                <th>Estado</th>
                                                <th><i class="fas fa-cog"></i></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach ($comitesInfantil->sortByDesc('fc_presentacion') as $comiteInfantil)
                                                <tr>
                                                    <td>{{$comiteInfantil->nr_comite}}</td>
                                                    <td>{{date('d-m-Y', strtotime($comiteInfantil->fc_presentacion))}}</td>
                                                    <td>{{$comiteInfantil->medico->tx_nombre_minuscula}}</td>
                                                    <td>{{$comiteInfantil->medico->especialidad->tx_nombre_minuscula}}</td>
                                                    <td>				
                                                        @if (isset($comiteInfantil->resolucion))
                                                            @if ($comiteInfantil->resolucion->tx_estado == 73)
                                                                <small class="badge badge-success">Resuelto</small>
                                                            @else
                                                                <small class="badge badge-primary">Representar</small>
                                                            @endif
                                                        @else
                                                            <small class="badge badge-warning">Por Resolver</small>
                                                        @endif						
                                                    </td>
                                                    <td>
                                                        <div class="btn-group">
                                                            @if (isset($comiteInfantil->resolucion) && $comiteInfantil->resolucion->tx_estado == 73)
                                                                <a class="btn btn-secondary btn-xs" href={{url('pdfComiteInfantil?resuelto=1&id='.$comiteInfantil->id)}} title="Ver Comite"><i class="fa fa-file-pdf" style="color:white"></i></a>
                                                            @else
                                                                <a class="btn btn-success btn-xs" href={{url('resolucionInfantil/create?id='.$comiteInfantil->id)}} title="Resolver"><i class="fa fa-check" style="color:white"></i></a>
                                                                <a class="btn btn-warning btn-xs" href={{url('comiteInfantil/'.$comiteInfantil->id.'/edit')}} title="Editar"><i class="fa fa-edit" style="color:white"></i></a>
                                                                <a class="btn btn-secondary btn-xs" href={{url('pdfComiteInfantil?resuelto=0&id='.$comiteInfantil->id)}} title="Ver Comite"><i class="fa fa-file-pdf" style="color:white"></i></a>
                                                            @endif
                                                            <a class="btn btn-info btn-xs" href={{url('clonarInfantil?id='.$comiteInfantil->id)}} title="Clonar Comite"><i class="fa fa-copy" style="color:white"></i></a>
                                                        </div>
                                                    </td>
                                                </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                @endif
                                @if (isset($comites) && count($comites) > 0)
                                    <strong>Otros</strong>
                                    <table class="table table-striped">
                                        <thead>
                                            <tr>
                                                <th>ID</th>
                                                <th>Tipo de Comite</th>
                                                <th>F. Presentación</th>
                                                <th>Medico</th>
                                                <th>Especialidad</th>
                                                <th>Estado</th>
                                                <th><i class="fas fa-cog"></i></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach ($comites->sortByDesc('fc_presentacion') as $comite)
                                            <tr role="row" class="odd">
                                                <td>{{$comite->id}}</td>
                                                <td>{{$comite->tipoComite->tx_nombre_mayuscula}}</td>
                                                <td>{{date('d-m-Y', strtotime($comite->fc_presentacion))}}</td>
                                                <td>{{$comite->medico->tx_nombre_minuscula}}</td>
                                                <td>{{$comite->especialidad->tx_nombre_minuscula}}</td>
                                                <td>@if ($comite->bo_resuelto == 72)
                                                        <small class="badge badge-success">Resuelto</small>
                                                    @elseif ($comite->bo_resuelto == 73)
                                                        <small class="badge badge-danger">Representar</small>
                                                    @else
                                                        <small class="badge badge-warning">Por Resolver</small>
                                                    @endif
                                                </td>
                                                <td>
                                                    <div class="btn-group">
                                                        @if ($comite->bo_resuelto == 0)
                                                            <a class="btn btn-success btn-xs" href={{url('resolucion/create?id='.$comite->id.'&tipo_comite='.$comite->id_tipo_comite)}} title="Resolver"><i class="fa fa-check" style="color:white"></i></a>
                                                            @if ($comite->created_by == Auth::user()->id)
                                                                <a class="btn btn-warning btn-xs" href={{url('comite/'.$comite->id.'/edit')}} title="Editar"><i class="fa fa-edit" style="color:white"></i></a>
                                                            @endif
                                                        @endif
                                                        @if ($comite->bo_resuelto == 73)
                                                            <a class="btn btn-success btn-xs" href={{url('resolucion/'.$comite->id.'/edit')}} title="Terminar de Resolver"><i class="fa fa-spinner" style="color:white"></i></a>
                                                        @endif
                                                        <a class="btn btn-danger btn-xs" href={{url('pdfComite?id='.$comite->id)}} title="Ver Comite" target="_blank"><i class="fa fa-file-pdf" style="color:white"></i></a>
                                                        @if (isset($comite->tx_archivo))
                                                            <a class="btn btn-primary btn-xs" href={{url('storage/'.$comite->tx_archivo)}} title="Archivo" target="_blank"><i class="fa fa-file" style="color:white"></i></a>
                                                        @endif
                                                        <a class="btn btn-info btn-xs" href={{url('clonar?id='.$comite->id)}} title="Clonar Comite"><i class="fa fa-copy" style="color:white"></i></a>
                                                    </div>
                                                </td>
                                            </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                @endif
                            </div>
                            <!-- /.col -->
                        </div>
                    </div>
                    <!-- /.tab-pane -->
                    <div class="tab-pane" id="epicrisis">
                        <div class="row">
                            <div class="col-12 table-responsive">
                                @if (isset($arr_epicrises) && count($arr_epicrises) > 0)
                                    <table class="table table-striped">
                                        <thead>
                                            <tr>
                                                <th>Codigo</th>
                                                <th>Tipo</th>
                                                <th>Medico</th>
                                                <th>F. Ingreso</th>
                                                <th>F. Egreso</th>
                                                <th><i class="fas fa-cog"></i></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach ($arr_epicrises as $arr_epicrisis)
                                                <tr>
                                                    <td>{{$arr_epicrisis['pac_corr']}}</td>
                                                    <td>{{$arr_epicrisis['tipo']}}</td>
                                                    <td>{{$arr_epicrisis['medico']['name']}}</td>
                                                    <td>{{date('d-m-Y', strtotime($arr_epicrisis['fecha_ingreso']))}}</td>
                                                    <td>{{date('d-m-Y', strtotime($arr_epicrisis['fecha_egreso']))}}</td>
                                                    <td>
                                                        <a class="btn btn-danger btn-xs" href="http://10.4.237.27/epicrisis-hsjd/api/epicrisis/pdf?token=5b7dbd9cf2d88&epicrisis_id={{$arr_epicrisis['id']}}&tipo={{$arr_epicrisis['tipo']}}" title="Archivo"><i class="fa fa-file-pdf" style="color:white"></i></a>
                                                    </td>
                                                </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                @endif
                            </div>
                            <!-- /.col -->
                        </div>
                    </div>
                    <!-- /.tab-pane -->

                    <div class="tab-pane" id="biopsia">
                        <div class="row">
                            <div class="col-12 table-responsive">
                                @if (isset($informes) && count($informes) > 0)
                                    <table class="table table-striped">
                                        <thead>
                                            <tr>
                                                <th>Tipo Estudio</th>
                                                <th>Código Estudio</th>
                                                <th>Servicio Solicitante</th>
                                                <th>F. Toma</th>
                                                <th>F. Cierre</th>
                                                <th><i class="fas fa-cog"></i></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach ($informes as $informe)
                                                <tr>
                                                    <td>{{$informe->tipoEstudio}}</td>
                                                    <td>{{$informe->codigoEstudio}}</td>
                                                    <td>{{$informe->servicio_solitante}}</td>
                                                    <td>{{date('d-m-Y', strtotime($informe->fecha_toma_muestra))}}</td>
                                                    <td>{{date('d-m-Y', strtotime($informe->fechaCierre))}}</td>
                                                    <td>
                                                        <a id="{{$informe->codigoEstudio}}" class="btn btn-danger btn-xs bioPDF"><i class="fa fa-print" data-toggle="tooltip" data-placement="left" title="Informe PDF"></i></a>
                                                        <input type="hidden" id="pdf{{$informe->codigoEstudio}}" value="{{$informe->informepdf}}">
                                                    </td>
                                                </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                @endif
                            </div>
                            <!-- /.col -->
                        </div>
                    </div>
                    <!-- /.tab-pane -->
                </div>
                <!-- /.tab-content -->
              </div><!-- /.card-body -->
            </div>
            <!-- /.nav-tabs-custom -->
          </div>
    </div>
@stop

@section('js')
<script>
    $(".bioPDF").click(function(){
        var id = $(this).attr("id");
        var documento=$("#pdf"+id).val();
        var dataURI = "data:application/pdf;base64,"+ documento;
        var link = document.createElement('a');
        link.download = 'biopsia'+id+'.pdf';
        link.href= dataURI;
        link.textContent = 'Download PDF';
        link.click();
        document.body.appendChild(link);
    });
</script>
@stop