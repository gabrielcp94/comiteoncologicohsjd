@extends('adminlte::page')

@section('title', 'Comite Oncologico')

@section('content_header')
    <div class="alert alert-success">
        Bienvenido al nuevo sistema de Comite Oncologico
    </div>
    {{-- <div class="alert alert-warning">
        <strong>IMPORTANTE!</strong> Momentaneamente solo se encuentra en funcionanmiento Comite Adulto, por lo que si desea ir al sistema anterior haga <strong><a href="http://10.4.237.11/comite_oncologico/login_1/?rUser={{Auth::user()->login}}&tUser={{session('token')}}">click aqui<a></strong>.
    </div> --}}
    @if(session()->has('message'))
        <div class="alert alert-success">
            {{ session()->get('message') }}
        </div>
    @endif
    @if(session()->has('error'))
        <div class="alert alert-danger">
            <i class="fa fa-exclamation-triangle"></i> {{ session()->get('error') }}
        </div>
    @endif
@stop

@section('content')
		{{-- <form class="form-horizontal" id="form" method="POST">
			{{ csrf_field() }} --}}
            
            <div class="row">
                <div class="col-lg-3 col-6">
                    <!-- small box -->
                    <div class="small-box bg-info">
                      <div class="inner">
                        <div class="row">
                            <div class="col-md-4">
                                <h1>
                                {{$estadistica['porResolver']}}
                                </h1>
                            </div>
                            <div class="col-md-8">
                                <p style="font-size: 16px; margin-bottom:-10px;"><strong>{{$estadistica['porResolverMes']}}</strong> en <?php setlocale(LC_TIME, "spanish"); echo strftime("%B");?></p>
                                <p style="font-size: 16px"><strong>{{$estadistica['porResolverAno']}}</strong> en <?php setlocale(LC_TIME, "spanish"); echo strftime("%Y");?></p>
                            </div>
                        </div>
                        <p>Tus comites por resolver</p>
                      </div>
                      <div class="icon">
                        <i class="fas fa-fw fa-spinner"></i>
                      </div>
                      <a href="comiteAdulto?resuelto=0&medicos%5B%5D={{Auth::user()->id}}" class="small-box-footer">Mas info <i class="fas fa-arrow-circle-right"></i></a>
                    </div>
                  </div>

                <div class="col-lg-3 col-6">
                  <!-- small box -->
                  <div class="small-box bg-success">
                    <div class="inner">
                        <div class="row">
                            <div class="col-md-4">
                                <h1>
                                {{$estadistica['resuelto']}}
                                </h1>
                            </div>
                            <div class="col-md-8">
                                <p style="font-size: 16px; margin-bottom:-10px;"><strong>{{$estadistica['resueltoMes']}}</strong> en <?php setlocale(LC_TIME, "spanish"); echo strftime("%B");?></p>
                                <p style="font-size: 16px"><strong>{{$estadistica['resueltoAno']}}</strong> en <?php setlocale(LC_TIME, "spanish"); echo strftime("%Y");?></p>
                            </div>
                        </div>
                        <p>Tus comites resueltos</p>
                    </div>
                    <div class="icon">
                      <i class="fas fa-fw fa-check"></i>
                    </div>
                    <a href="comiteAdulto?resuelto=1&medicos%5B%5D={{Auth::user()->id}}" class="small-box-footer">Mas info <i class="fas fa-arrow-circle-right"></i></a>
                  </div>
                </div>
                <!-- ./col -->
                <div class="col-lg-3 col-6">
                  <!-- small box -->
                  <div class="small-box bg-warning">
                    <div class="inner" style="color: white">
                        <div class="row">
                            <div class="col-md-4">
                                <h1>
                                {{$estadistica['porResolverEspecialidad']}}
                                </h1>
                            </div>
                            <div class="col-md-8">
                                <p style="font-size: 16px; margin-bottom:-10px;"><strong>{{$estadistica['porResolverEspecialidadMes']}}</strong> en <?php setlocale(LC_TIME, "spanish"); echo strftime("%B");?></p>
                                <p style="font-size: 16px"><strong>{{$estadistica['porResolverEspecialidadAno']}}</strong> en <?php setlocale(LC_TIME, "spanish"); echo strftime("%Y");?></p>
                            </div>
                        </div>
                        <p>Comites de {{Auth::user()->especialidad->tx_nombre_minuscula}} por resolver</p>
                    </div>
                    <div class="icon">
                      <i class="fas fa-fw fa-clinic-medical"></i>
                    </div>
                    <a href="comiteAdulto?resuelto=0&especialidades%5B%5D={{Auth::user()->tx_especialidad}}" class="small-box-footer"> <span style="color:white";>Mas info <i class="fas fa-arrow-circle-right"></i></span></a>
                  </div>
                </div>
                <!-- ./col -->
                <div class="col-lg-3 col-6">
                  <!-- small box -->
                  <div class="small-box bg-danger">
                    <div class="inner">
                        <div class="row">
                            <div class="col-md-4">
                                <h1>
                                {{$estadistica['porResolverTotal']}}
                                </h1>
                            </div>
                            <div class="col-md-8">
                                <p style="font-size: 16px; margin-bottom:-10px;"><strong>{{$estadistica['porResolverTotalMes']}} en <?php setlocale(LC_TIME, "spanish"); echo strftime("%B");?></strong></p>
                                <p style="font-size: 16px"><strong>{{$estadistica['porResolverTotalAno']}} en <?php setlocale(LC_TIME, "spanish"); echo strftime("%Y");?></strong></p>
                            </div>
                        </div>      
                      <p>Comites por resolver totales</p>
                    </div>
                    <div class="icon">
                      <i class="fas fa-fw fa-hospital"></i>
                    </div>
                    <a href="comiteAdulto?resuelto=0" class="small-box-footer">Mas info <i class="fas fa-arrow-circle-right"></i></a>
                  </div>
                </div>
                <!-- ./col -->
              </div>

              <div class="row">
                <div id="piechartResuelto" style="width: 500px; height: 500px;"></div>
                <div id="piechartPorResolver" style="width: 500px; height: 500px;"></div>
              </div>
@stop

@section('js')

<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script>
    @if(isset(request()->especialidades))
        var user = @json(request()->all());
        var especialidades = user.especialidades;
        $("#especialidades").val(especialidades).trigger('change');
    @endif

    @if(isset(request()->medicos))
        var user = @json(request()->all());
        var medicos = user.medicos;
        $("#medicos").val(medicos).trigger('change');
    @endif

	  $(".alert-success").fadeTo(20000, 500).slideUp(500, function(){
        $(".alert-success").slideUp(1000);
    });

    $(".alert-danger").fadeTo(20000, 5000).slideUp(500, function(){
        $(".alert-danger").slideUp(1000);
    });

    $('.select2').select2();

    google.charts.load('current', {'packages':['corechart']});
    google.charts.setOnLoadCallback(drawChart);
    google.charts.setOnLoadCallback(drawChart2);

    function drawChart() {
        var data = google.visualization.arrayToDataTable(@json($torta));
        var options = {
        title: 'Comites Resueltos por Especialidad'
        };
        var chart = new google.visualization.PieChart(document.getElementById('piechartResuelto'));
        chart.draw(data, options);
    }
    function drawChart2() {
        var data = google.visualization.arrayToDataTable(@json($torta2));
        var options = {
        title: 'Comites Por Resolver por Especialidad'
        };
        var chart = new google.visualization.PieChart(document.getElementById('piechartPorResolver'));
        chart.draw(data, options);
    }    
</script>
@stop