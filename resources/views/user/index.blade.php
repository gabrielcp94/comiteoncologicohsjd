@extends('adminlte::page')

@section('title', 'Lista de Usuarios')

@section('content_header')
    @if(session()->has('message'))
        <div class="alert alert-success">
            {{ session()->get('message') }}
        </div>
    @endif
    @if(session()->has('error'))
        <div class="alert alert-danger">
            <i class="fa fa-exclamation-triangle"></i> {{ session()->get('error') }}
        </div>
    @endif
@stop

@section('content')
		{{-- <form class="form-horizontal" id="form" method="POST">
			{{ csrf_field() }} --}}
			

	<div class="card card-info">
		<div class="card-header">
		<h3 class="card-title">Lista de Usuarios</h3>
		</div>
		<!-- /.card-header -->
		<!-- form start -->
		<div class="card-body">
            <div id="example1_wrapper" class="dataTables_wrapper dt-bootstrap4">
                <div class="row">
                    <div class="col-sm-12">
                        <form class="form-horizontal">
                            <div class="form-group row">
                                <div class="col-sm-5">
                                    <input type="text" name="tx_nombre" class="form-control" id="tx_nombre" placeholder="Nombre" value="{{request()->tx_nombre}}">
                                </div>
                                <div class="col-sm-3">
                                    <input type="text" name="tx_rut" class="form-control" id="tx_rut" placeholder="Rut" value="{{request()->tx_rut}}">
                                </div>
                                <div class="col-sm-4 text-right">
                                    <button type="submit" class="btn btn-info">Filtrar</button>
                                    <a href="user/create" class="btn btn-success" type="button" title="Agregar Usuario"><i class="fa fa-user-plus" style="color:white"></i></a>
                                </div>
                            </div>
                        </form>
                        <table class="table table-striped table-hover" id="tableUsers">
                            <thead>
                                <th>ID</th>
                                <th>Nombre</th>
                                <th>Rut</th>
                                <th>Especialidad</th>
                                <th>Establecimiento</th>
                                <th>Tipo Profesional</th>
                                <th>Activo</th>
                                <th><i class="fa fa-cog"></i></th>
                            </thead>
                            <tbody>
                                @foreach ($users as $user)
                                    <tr role="row" class="odd">
                                        <td>{{$user->id}}</td>
                                        <td>{{$user->tx_nombre_minuscula}}</td>
                                        <td>{{$user->tx_rut}}</td>
                                        <td>{{$user->especialidad->tx_nombre_minuscula}}</td>
                                        <td>{{$user->establecimiento->tx_nombre_minuscula}}</td>
                                        <td>{{$user->tx_tipo_profesional}}</td>
                                        <td>{{$user->active == 'Y' ? 'Si' : 'No'}}</td>
										<td>
                                            <form action="{{ route('user.destroy',$user->id) }}" method="POST">
                                                <div class="btn-group">
                                                <a class="btn btn-warning btn-xs" href="{{ route('user.edit',$user->id) }}" title="Editar Usuario"><i class="fa fa-edit" style="color:white"></i></a>
                                                <a class="btn btn-primary btn-xs" onclick="return confirm('¿Esta seguro de reiniciar la contraseña de {{$user->tx_nombre_minuscula}}?');" href="resetPass/{{$user->id}}" title="Reiniciar Contraseña"><i class="fa fa-lock-open" style="color:white"></i></a>
                                                @csrf
                                                @method('DELETE')
                                                <button onclick="return confirm('¿Esta seguro de eliminar a {{$user->tx_nombre_minuscula}}?');" type="submit" class="btn btn-danger btn-xs" title="Eliminar Usuario"><i class="fa fa-trash" style="color:white"></i></button>
                                                </div>
                                            </form>
										</td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                        {{ $users->appends(request()->query())->links() }}
                    </div>
                </div>
            </div>
        </div>
        <!-- /.card-body -->
    </div>
@stop

@section('js')
<script>
	$(".alert-success").fadeTo(20000, 500).slideUp(500, function(){
        $(".alert-success").slideUp(1000);
    });

    $(".alert-danger").fadeTo(20000, 5000).slideUp(500, function(){
        $(".alert-danger").slideUp(1000);
    });
</script>
@stop