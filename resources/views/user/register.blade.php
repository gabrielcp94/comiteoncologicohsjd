@extends('adminlte::page')

@section('title', 'Registro Usuario')

@section('content')
		{{-- <form class="form-horizontal" id="form" method="POST">
			{{ csrf_field() }} --}}
			

	<div class="card card-info">
		<div class="card-header">
		<h3 class="card-title">{{ isset($user) ? 'Editar cuenta' : 'Crear cuenta'}}</h3>
		</div>
		<!-- /.card-header -->
		<!-- form start -->
		<form role="form" class="form-horizontal" id="form" method="POST" action="{{action('UserController@store')}}">
			{{ csrf_field() }}
			<input type="hidden" id="miUser" name="miUser" value="{{isset($miUser) ? $miUser : ''}}"/>
			<div class="card-body">
				<div class="form-group">
					<div class="row">
						<div class="col-sm-3">
							<label for="login">Rut:<span style="color:#FF0000";>*</span></label>
							<input class="form-control" id="login" name="login" value="{{isset($user) ? $user->login : ''}}" {{isset($user) ? 'readonly' : ''}} onblur="getPerson($(this).val());" required>
						</div>
						<div class="col-sm-3">
							<label for="name">Nombre:<span style="color:#FF0000";>*</span></label>
							<input class="form-control" id="name" name="name" value="{{isset($user) ? $user->name : ''}}" required>
						</div>
						<div class="col-sm-3">
							<label for="email">Email:</label>
							<input type="email" class="form-control" id="email" name="email" value="{{isset($user) ? $user->email : ''}}">
						</div>
						@if(!isset($miUser))
							<div class="col-sm-3">
								<div class="custom-control custom-checkbox">
									<input class="custom-control-input" type="checkbox" id="active" name="active" value="Y" checked>
									<label for="active" class="custom-control-label">Activo</label>
								</div>
							</div>
						@endif
					</div>
				</div>
				@if(!isset($miUser))
					<div class="form-group">
						<div class="row">
							<div class="col-sm-3">
								<label for="tx_especialidad">Especialidad:<span style="color:#FF0000";>*</span></label>
								<select class="select2 select2-hidden-accessible" id="tx_especialidad" name="tx_especialidad" data-placeholder="Seleccione especialidad" style="width: 100%;" data-select2-id="7" tabindex="-1" aria-hidden="true" required>
									<option value=""></option>
									@foreach ($especialidades as $especialidad)
										<option value={{$especialidad->id}} {{isset($user->especialidad) && $user->especialidad->id == $especialidad->id ? "selected" : ""}}>{{$especialidad->tx_nombre_minuscula}}</option>
									@endforeach
								</select>
							</div>
							<div class="col-sm-3">
								<label for="tx_establecimiento">Establecimiento:<span style="color:#FF0000";>*</span></label>
								<select class="select2 select2-hidden-accessible" id="tx_establecimiento" name="tx_establecimiento" data-placeholder="Seleccione establecimiento" style="width: 100%;" data-select2-id="7" tabindex="-1" aria-hidden="true" required>
									<option value=""></option>
									@foreach ($establecimientos as $establecimiento)
										<option value={{$establecimiento->id}} {{isset($user->establecimiento) && $user->establecimiento->id == $establecimiento->id ? "selected" : ""}}>{{$establecimiento->tx_nombre_minuscula}}</option>
									@endforeach
								</select>					
							</div>
							<div class="col-sm-3">
								<label for="tx_tipo_profesional">Tipo Profesional:<span style="color:#FF0000";>*</span></label>
								<select class="select2 select2-hidden-accessible" id="tx_tipo_profesional" name="tx_tipo_profesional" data-placeholder="Seleccione tipo profesional" style="width: 100%;" data-select2-id="7" tabindex="-1" aria-hidden="true" required>
									<option value=""></option>
									<option value="STAFF" {{isset($user->tx_tipo_profesional) && $user->tx_tipo_profesional == 'STAFF' ? "selected" : ""}}>Staff</option>
									<option value="BECADO" {{isset($user->tx_tipo_profesional) && $user->tx_tipo_profesional == 'BECADO' ? "selected" : ""}}>Becado</option>
								</select>					
							</div>
							<div class="col-sm-3">
								<label for="group_id">Grupo:<span style="color:#FF0000";>*</span></label>
								<select class="select2 select2-hidden-accessible" id="group_id" name="group_id" data-placeholder="Seleccione grupo" style="width: 100%;" data-select2-id="7" tabindex="-1" aria-hidden="true" required>
									<option value=""></option>
									@foreach ($grupos as $grupo)
										<option value={{$grupo->group_id}} {{isset($user->grupo) && $user->grupo->group_id == $grupo->group_id ? "selected" : ""}}>{{$grupo->description}}</option>
									@endforeach
								</select>					
							</div>
						</div>
					</div>
				@endif
			</div>
		  	<div class="card-footer text-right">
				<button type="submit" class="btn btn-success">Guardar</button>
		  	</div>
		</form>
	  </div>
@stop

@section('js')
<script>
	function getPerson(tx_rut){
		@if (!isset($user))
			$.getJSON("{{action('GetController@getDatosRut')}}?rut="+tx_rut,
			function(data){
				if(data.user && data.encontrado){
					var opcion = confirm("Este Usuario ya existe desea editarlo?");
					if (opcion == true) {
						location.href = data.id+"/edit";
					} else {
						$("#login").val("");
					}
				}
				else{
					if(data.encontrado == true){
						$("#login").val(data.tx_rut);
						$("#name").val(data.tx_nombre);
						$("#name").css('display','').attr('readonly', true);
					}else{
						alert("Rut no encontrado");
						$("#login").val("");
						$("#name").val("");
						$("#name").css('display','').attr('readonly', false);
					}
				}
			})
		@endif
	};

	$('.select2').select2();
</script>
@stop