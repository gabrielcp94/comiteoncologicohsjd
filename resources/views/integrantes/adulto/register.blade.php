@extends('adminlte::page')

@section('title', 'Registro Integrantes')

@section('content')
		{{-- <form class="form-horizontal" id="form" method="POST">
			{{ csrf_field() }} --}}
			

	<div class="card card-info">
		<div class="card-header">
		<h3 class="card-title">{{ isset($integrantes) ? (isset($bo_clonar) && $bo_clonar == 1 ? 'Clonar' : 'Editar') : 'Crear'}} lista integrantes Adulto</h3>
		</div>
		<!-- /.card-header -->
		<!-- form start -->
		<form role="form" class="form-horizontal" id="form" method="POST" action="{{action('IntegrantesAdultoController@store')}}">
			{{ csrf_field() }}
			<input type="hidden" id="id" name="id" value="{{isset($integrantes) ? $integrantes->id : ''}}"/>
			<input type="hidden" id="bo_clonar" name="bo_clonar" value="{{isset($bo_clonar) ? '1' : '0'}}"/>
			<div class="card-body">
				<div class="form-group">
					<div class="row">
						<div class="col-sm-3">
							<label for="fc_lista">Fecha de Comité:<span style="color:#FF0000";>*</span></label>
							<div class="input-group">
								<div class="input-group-prepend">
									<span class="input-group-text"><i class="far fa-calendar-alt"></i></span>
								</div>
							<input type="date" class="form-control" id="fc_lista" name="fc_lista" value="{{isset($integrantes) ? (isset($integrantes->fc_lista) ? $integrantes->fc_lista : $integrantes->paciente->fc_nacimiento) : ''}}" required>
							</div>						
						</div>
						@if (isset($integrantes))
							<div class="col-sm-4">
								<label for="tx_integrantes">Integrantes:<span style="color:#FF0000";>*</span></label>
								<textarea class="form-control noresize" id="tx_integrantes" name="tx_integrantes">{{isset($integrantes->tx_integrantes) ? $integrantes->tx_integrantes : ""}}</textarea>
							</div>
						@else
							<div class="col-sm-4">
								<label for="medicos">Integrantes:<span style="color:#FF0000";>*</span></label>
								<select class="select2 select2-hidden-accessible" id="medicos" name="medicos[]" multiple="" data-placeholder="Seleccione medico(s)" style="width: 100%;" data-select2-id="7" tabindex="-1" aria-hidden="true" required>
									@foreach ($medicos as $medico)
										<option value={{$medico->id}} {{isset($medicosActivos) && in_array($medico->id, $medicosActivos) ? 'selected' : ''}}>{{$medico->tx_nombre_minuscula}}</option>
									@endforeach
								</select>
							</div>
						@endif
					</div>
				</div>
			</div>
		  	<div class="card-footer text-right">
				<button type="submit" class="btn btn-success">Guardar</button>
		  	</div>
		</form>
	  </div>
@stop

@section('js')
<script>
	function getPerson(tx_rut){
		@if (!isset($user))
			$.getJSON("{{action('GetController@getDatosRut')}}?rut="+tx_rut,
			function(data){
				if(data.user && data.encontrado){
					var opcion = confirm("Este Usuario ya existe desea editarlo?");
					if (opcion == true) {
						location.href = data.id+"/edit";
					} else {
						$("#tx_rut").val("");
					}
				}
				else{
					if(data.encontrado == true){
						$("#tx_rut").val(data.tx_rut);
						$("#tx_nombre").val(data.tx_nombre);
						$("#tx_nombre").css('display','').attr('readonly', true);
					}else{
						alert("Rut no encontrado");
						$("#tx_rut").val("");
						$("#tx_nombre").val("");
						$("#tx_nombre").css('display','').attr('readonly', false);
					}
				}
			})
		@endif
	};

	$('.select2').select2();
</script>
@stop