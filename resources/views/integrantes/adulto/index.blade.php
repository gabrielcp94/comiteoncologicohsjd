@extends('adminlte::page')

@section('title', 'Lista de Integrantes')

@section('content_header')
    @if(session()->has('message'))
        <div class="alert alert-success">
            {{ session()->get('message') }}
        </div>
    @endif
    @if(session()->has('error'))
        <div class="alert alert-danger">
            <i class="fa fa-exclamation-triangle"></i> {{ session()->get('error') }}
        </div>
    @endif
@stop

@section('content')
		{{-- <form class="form-horizontal" id="form" method="POST">
			{{ csrf_field() }} --}}
			

	<div class="card card-info">
		<div class="card-header">
		<h3 class="card-title">Lista de Integrantes Adultos</h3>
		</div>
		<!-- /.card-header -->
		<!-- form start -->
		<div class="card-body">
            <div id="example1_wrapper" class="dataTables_wrapper dt-bootstrap4">
                <div class="row">
                    <div class="col-sm-12">
                        <form class="form-horizontal">
                            <div class="form-group row">
                                <div class="col-sm-4">
                                    <label for="medico">Medico:</label>
                                    <input type="text" name="medico" class="form-control" id="medico" placeholder="Medico" value="{{request()->medico}}">
                                </div>
                                <div class="col-sm-8 text-right">
                                    <a href="integrantesAdulto?" class="btn btn-danger" title="Borrar Filtro">Borrar</a>
                                    <button type="submit" class="btn btn-info">Filtrar</button>
                                    <a href="integrantesAdulto/create" type="submit" class="btn btn-success" title="Agregar Integrantes"><i class="fa fa-user-plus" style="color:white"></i></a>
                                </div>
                            </div>
                        </form>
                        <table class="table table-striped table-hover" id="tableUsers">
                            <thead>
                                <th>F. Comite</th>
                                <th>Integrantes</th>
                                <th><i class="fa fa-cog"></i></th>
                            </thead>
                            <tbody>
                                @foreach ($integrantes as $integrante)
                                    <tr role="row" class="odd">
                                        <td>{{date('d-m-Y', strtotime($integrante->fc_lista))}}
                                        <td>{{$integrante->tx_integrantes}}</td>
                                        <td>
                                            <div class="btn-group">
                                                <a class="btn btn-warning btn-xs" href="integrantesAdulto/{{$integrante->id}}/edit" title="Editar"><i class="fa fa-edit" style="color:white"></i></a>
                                                <a class="btn btn-info btn-xs" href="clonarIntegrantesAdulto/?id={{$integrante->id}}" title="Clonar"><i class="fa fa-copy" style="color:white"></i></a>
                                            </div>
										</td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                        {{ $integrantes->appends(request()->query())->links() }}
                    </div>
                </div>
            </div>
        </div>
        <!-- /.card-body -->
    </div>
@stop

@section('js')
<script>
	$(".alert-success").fadeTo(20000, 500).slideUp(500, function(){
        $(".alert-success").slideUp(1000);
    });

    $(".alert-danger").fadeTo(20000, 5000).slideUp(500, function(){
        $(".alert-danger").slideUp(1000);
    });

    $('.select2').select2();
</script>
@stop