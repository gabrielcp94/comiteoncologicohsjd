@extends('adminlte::page')

@section('title', 'Lista de Integrantes')

@section('content_header')
    @if(session()->has('message'))
        <div class="alert alert-success">
            {{ session()->get('message') }}
        </div>
    @endif
    @if(session()->has('error'))
        <div class="alert alert-danger">
            <i class="fa fa-exclamation-triangle"></i> {{ session()->get('error') }}
        </div>
    @endif
@stop

@section('content')
		{{-- <form class="form-horizontal" id="form" method="POST">
			{{ csrf_field() }} --}}
			

	<div class="card card-info">
		<div class="card-header">
		<h3 class="card-title">Lista de Integrantes Infantil</h3>
		</div>
		<!-- /.card-header -->
		<!-- form start -->
		<div class="card-body">
            <div id="example1_wrapper" class="dataTables_wrapper dt-bootstrap4">
                <div class="row">
                    <div class="col-sm-12">
                        <form class="form-horizontal">
                            <div class="form-group row">
                                <div class="col-sm-4">
                                    <label for="medico">Medico:</label>
                                    <input type="text" name="medico" class="form-control" id="medico" placeholder="Medico" value="{{request()->medico}}">
                                </div>
                                <div class="col-sm-8 text-right">
                                    <a href="integrantesInfantil?" class="btn btn-danger" title="Borrar Filtro">Borrar</a>
                                    <button type="submit" class="btn btn-info">Filtrar</button>
                                    <a href="integrantesInfantil/create" type="submit" class="btn btn-success" title="Agregar Integrantes"><i class="fa fa-user-plus" style="color:white"></i></a>
                                </div>
                            </div>
                        </form>
                        <table class="table table-striped table-hover" id="tableUsers">
                            <thead>
                                <th>F. Comite</th>
                                <th>Integrantes</th>
                                <th>Observaciones</th>
                                <th><i class="fa fa-cog"></i></th>
                            </thead>
                            <tbody>
                                @foreach ($integrantes as $integrante)
                                    <tr role="row" class="odd">
                                        <td>{{date('d-m-Y', strtotime($integrante->fc_lista))}}
                                        <td>{{$integrante->tx_integrantes}}</td>
                                        <td>{{$integrante->tx_observacion}}</td>
                                        <td>
                                            <div class="btn-group">
                                                <a class="btn btn-warning btn-xs" href="integrantesInfantil/{{$integrante->id}}/edit" title="Editar"><i class="fa fa-edit" style="color:white"></i></a>
                                                <a class="btn btn-info btn-xs" href="clonarIntegrantesInfantil/?id={{$integrante->id}}" title="Clonar"><i class="fa fa-copy" style="color:white"></i></a>
                                            </div>
										</td>
                                        {{-- <td>{{$comite->id}}</td>
                                        <td>{{isset($comite->paciente->tx_nombre_completo) ? $comite->paciente->tx_nombre_completo : $comite->tx_nombre_completo}}</td>
                                        <td>{{isset($comite->paciente->tx_rut) ? $comite->paciente->tx_rut : $comite->tx_rut}}</td>
                                        <td>{{date('d-m-Y', strtotime($comite->fc_presentacion))}}</td>
                                        @if (request()->resuelto == 1)
                                            <td>{{isset($comite->resolucion) ? date('d-m-Y', strtotime($comite->resolucion->fc_resolucion)) : ''}}</td>
                                        @endif
                                        <td>{{isset($comite->medico->tx_nombre_minuscula) ? $comite->medico->tx_nombre_minuscula : $comite->tx_medico}}</td>
                                        <td>{{isset($comite->medico->especialidad) ? $comite->medico->especialidad->tx_nombre_minuscula : $comite->tx_especialidad_minuscula}}</td>
                                        <td>{{isset($comite->resolucion) ? isset($comite->resolucion->estado) ? $comite->resolucion->estado->tx_nombre_minuscula : $comite->resolucion->tx_estado : 'Indefinido'}}</td>
                                        @if (request()->adulto == 1 && request()->resuelto == 0)
                                            <td>{{isset($comite->bo_revisar_imagen) && $comite->bo_revisar_imagen == 1 ? 'Si' : 'No'}}</td>
                                            <td>{{isset($comite->resolucion) ? $comite->resolucion->tx_cateter_qt : ''}}</td>
                                        @endif
                                        <td>
                                            <div class="btn-group">
                                                <a class="btn btn-warning btn-xs" href="paciente/1/edit?adulto={{request()->adulto}}&id={{$comite->id}}" title="Editar"><i class="fa fa-edit" style="color:white"></i></a>
                                                <a class="btn btn-danger btn-xs" href="pdfComite?resuelto={{request()->resuelto}}&adulto={{request()->adulto}}&id={{$comite->id}}" title="Ver Comite"><i class="fa fa-file-pdf" style="color:white"></i></a>
                                                @if (isset($comite->tx_archivo))
                                                    <a class="btn btn-primary btn-xs" href="storage/{{$comite->tx_archivo}}" title="Archivo"><i class="fa fa-file" style="color:white"></i></a>
                                                @endif
                                            </div>
										</td> --}}
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                        {{ $integrantes->appends(request()->query())->links() }}
                    </div>
                </div>
            </div>
        </div>
        <!-- /.card-body -->
    </div>
@stop

@section('js')
<script>
    // @if(isset(request()->medicos))
    //     var user = @json(request()->all());
    //     var medicos = user.medicos;
    //     $("#medicos").val(medicos).trigger('change');
    // @endif

	$(".alert-success").fadeTo(20000, 500).slideUp(500, function(){
        $(".alert-success").slideUp(1000);
    });

    $(".alert-danger").fadeTo(20000, 5000).slideUp(500, function(){
        $(".alert-danger").slideUp(1000);
    });

    $('.select2').select2();
</script>
@stop